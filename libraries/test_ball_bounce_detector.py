
"""
   Copyright 2022, San Jose State Univerity

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    Authors:
        - Hisaam Hashim
        - Jesse Pham
        - Nathan Luc
        - Huy Nguyen
    
    Testing Description:
        Verifies that top and side-view cameras both work as intended.
"""
import argparse
import ball_bounce_detector
import time

def test_side_view_bounces():
    #camera_devices = {"primary": "/dev/video4", "secondary": "/dev/video8"}
    camera_devices = {"primary": None, "secondary": "/dev/video8"}
    side_view_camera_video_file = "video_files/bc_fg_2.mp4"
    camera_resolution = (1920, 1080)

    ball_bounce_tracker = ball_bounce_detector.BallBounceDetector(camera_devices=camera_devices,
                                                                  camera_resolution=camera_resolution,
                                                                  video_files=[None, side_view_camera_video_file])
    
    while(True):
        valid_frame, ball_was_bounced = ball_bounce_tracker.analyze_side_view_camera()

        if not valid_frame:
            break
        else:
            if ball_was_bounced:
                print("Secondary Camera found a ball bounce!")
    ball_bounce_tracker.terminate_cameras()


def test_top_view_bounces():
    #camera_devices = {"primary": "/dev/video4", "secondary": "/dev/video8"}
    camera_devices = {"primary": "/dev/video4", "secondary": None}
    top_view_camera_video_file = "video_files/tc_fg_2.mp4"
    camera_resolution = (1920, 1080)
    ball_colors_lower_rgb = (164, 153, 150)
    ball_colors_higher_rgb = (255, 123, 0)
    ball_color = [ball_colors_lower_rgb, ball_colors_higher_rgb]

    ball_bounce_tracker = ball_bounce_detector.BallBounceDetector(camera_devices=camera_devices,
                                                                  camera_resolution=camera_resolution,
                                                                  ball_colors=ball_color,
                                                                  video_files=[top_view_camera_video_file, None],
                                                                  verbose=False, debug=True)
    
    while(True):
        valid_frame, ball_was_bounced, ball_bounce_metrics = ball_bounce_tracker.analyze_top_view_camera()

        if not valid_frame:
            break
        else:
            if ball_was_bounced:
                print("Primary Camera metrics to pass: %s" % ball_bounce_metrics)
    ball_bounce_tracker.terminate_cameras()


def test_rgb_to_hsv():
    # lower bound
    rgb_values = (164, 153, 150)
    hsv_values = ball_bounce_detector.rgb_to_hsv(rgb_values[0], rgb_values[1], rgb_values[2])
    print("RGB is: ", rgb_values)
    print("HSV is: ", hsv_values)

    # upper bound
    rgb_values = (255, 123, 0)
    hsv_values = ball_bounce_detector.rgb_to_hsv(rgb_values[0], rgb_values[1], rgb_values[2])
    print("RGB is: ", rgb_values)
    print("HSV is: ", hsv_values)

if __name__ == '__main__':
    #test_rgb_to_hsv()
    #test_side_view_bounces()
    test_top_view_bounces()