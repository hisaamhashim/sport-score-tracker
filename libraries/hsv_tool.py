#finding hsv range of target object(pen)
import cv2
import numpy as np
import time

def nothing(x):
    pass

# Initializing the webcam feed.
# cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture("./video_source/tc_fg_2.mp4")
cap.set(3,1280)
cap.set(4,720)

# Create a window named trackbars.
cv2.namedWindow("Trackbars")

# Now create 6 trackbars that will control the lower and upper range of 
# H,S and V channels. The Arguments are like this: Name of trackbar, 
# window name, range,callback function. For Hue the range is 0-179 and
# for S,V its 0-255.

# ball_lower_hsv_value = (0, 0, 0)
# ball_upper_hsv_value = (179, 255, 255)

ball_lower_hsv_value = (12, 22, 166)
ball_upper_hsv_value = (29, 255, 255)
sleep_time_ms = 0.05

cv2.createTrackbar("L - H", "Trackbars", ball_lower_hsv_value[0], 179, nothing)
cv2.createTrackbar("L - S", "Trackbars", ball_lower_hsv_value[1], 255, nothing)
cv2.createTrackbar("L - V", "Trackbars", ball_lower_hsv_value[2], 255, nothing)
cv2.createTrackbar("U - H", "Trackbars", ball_upper_hsv_value[0], 179, nothing)
cv2.createTrackbar("U - S", "Trackbars", ball_upper_hsv_value[1], 255, nothing)
cv2.createTrackbar("U - V", "Trackbars", ball_upper_hsv_value[2], 255, nothing)

pl_h = 0
pl_s = 0
pl_v = 0
pu_h = 179
pu_s = 255
pu_v = 255
 
while True:
    
    # Start reading the webcam feed frame by frame.
    ret, frame = cap.read()
    if not ret:
        break
    
    # Convert the BGR image to HSV image.
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    # Get the new values of the trackbar in real time as the user changes 
    # them
    l_h = cv2.getTrackbarPos("L - H", "Trackbars")
    l_s = cv2.getTrackbarPos("L - S", "Trackbars")
    l_v = cv2.getTrackbarPos("L - V", "Trackbars")
    u_h = cv2.getTrackbarPos("U - H", "Trackbars")
    u_s = cv2.getTrackbarPos("U - S", "Trackbars")
    u_v = cv2.getTrackbarPos("U - V", "Trackbars")

    # Print if there is a change in HSV value
    if((pl_h != l_h) | (pl_s != l_s) | (pl_v != l_v) | (pu_h != u_h) | (pu_s != u_s) | (pu_v != u_v) ):
        print("ball_lower_hsv_value = (%d, %d, %d)\nball_upper_hsv_value = (%d, %d, %d)"% (l_h , l_s , l_v, u_h, u_s , u_v))
        pl_h = l_h
        pl_s = l_s
        pl_v = l_v
        pu_h = u_h
        pu_s = u_s
        pu_v = u_v
 
    # Set the lower and upper HSV range according to the value selected
    # by the trackbar
    lower_range = np.array([l_h, l_s, l_v])
    upper_range = np.array([u_h, u_s, u_v])
    
    # Filter the image and get the binary mask, where white represents 
    # your target color
    mask = cv2.inRange(hsv, lower_range, upper_range)
 
    # You can also visualize the real part of the target color (Optional)
    res = cv2.bitwise_and(frame, frame, mask=mask)
    
    # Converting the binary mask to 3 channel image, this is just so 
    # we can stack it with the others
    mask_3 = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    
    # stack the mask, orginal frame and the filtered result
    stacked = np.hstack((mask_3,frame,res))
    
    # Show this stacked frame at 40% of the size.
    cv2.imshow('Trackbars',cv2.resize(stacked,None,fx=0.4,fy=0.4))
    
    # If the user presses ESC then exit the program
    key = cv2.waitKey(1)
    if key == 27:
        break
    
    # If the user presses `s` then print this array.
    if key == ord('s'):
        
        thearray = [[l_h,l_s,l_v],[u_h, u_s, u_v]]
        print(thearray)
        
        # Also save this array as penval.npy
        np.save('hsv_value',thearray)
        break
    time.sleep(sleep_time_ms)
# Release the camera & destroy the windows.    
cap.release()
cv2.destroyAllWindows()#