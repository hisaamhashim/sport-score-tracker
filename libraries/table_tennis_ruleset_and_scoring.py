"""
    A library containing table tennis rulesets and scoring state machines
    for popular rulesets such as singles, doubles, infinite rally, and
    training mode (hitting custom sections of the table)
"""
import copy

class table_tennis_ruleset_snapshot(object):
    def save_snapshot(self, team1_scores, team2_scores, server, action="s", bounce_count=1):
        # Whether this is called on every shot, the snapshot can only be saved
        # on a serve and not a rally and on the first bounce
        # (when the serving player is ready to serve).
        if (action == "s" and bounce_count == 1):
            # We must deep copy as the pointer is passed instead of actually
            # creating a copy of the data structure.
            self._previous_team1_scores = copy.deepcopy(team1_scores)
            self._previous_team2_scores = copy.deepcopy(team2_scores)

            self._previous_action = action
            self._previous_responder = server
            self._previous_server = server
            self._previous_bounce_count = bounce_count
    
    def restore_snapshot(self):
        # The last two values are game_finished and team_that_won, respectively.
        snapshot_bundle = [
            self._previous_team1_scores,
            self._previous_team2_scores,
            self._previous_action,
            self._previous_responder,
            self._previous_server,
            self._previous_bounce_count,
            False,
            0]
        return snapshot_bundle


def validate_ruleset_singles_inputs(action, responder, location, bounce_count):
    """ Raises a ValueError if any of the inputs are out of range.
    """
    if action not in ["s", "r"]:
        raise ValueError("Action must be either: s or r!")
    if responder not in [1, 2]:
        raise ValueError("Responder/Team must be either: 1 or 2!")
    if location not in [0, 1, 2, 3, 4, 5]:
        raise ValueError("Location must be an integer between: 0-5!")
    if bounce_count not in [1, 2]:
        raise ValueError("Ball Bounce count must either: 1 or 2!")


def validate_ruleset_doubles_inputs(action, responder, location, bounce_count):
    """ Raises a ValueError if any of the inputs are out of range.
    """
    if action not in ["s", "r"]:
        raise ValueError("Action must be either: s or r!")
    if responder not in [1, 2]:
        raise ValueError("Responder/Team must be either: 1 or 2!")
    if location not in [0, 1, 2, 3, 4, 5]:
        raise ValueError("Location must be an integer between: 0-5!")
    if bounce_count not in [1, 2]:
        raise ValueError("Ball Bounce count must either: 1 or 2!")


def validate_scoring_singles_inputs(result, new_responder, team1_reward, team2_reward, ruleset_num_sets):
    """ Raises a ValueError if any of the inputs are out of range.
    """
    if result not in ["continue", "new_serve", "let"]:
        raise ValueError("The next state machine result must be: continue, new_serve, let!")
    if new_responder not in [1, 2]:
        raise ValueError("Responder/Team must be either: 1 or 2!")
    if (team1_reward not in [0, 1]) or (team2_reward not in [0, 1]):
        raise ValueError("Team point score must be either: 0 or 1!")
    if (ruleset_num_sets % 2) == 0:
        raise ValueError("Number of Sets for the match must be odd!")


def validate_scoring_doubles_inputs(result, new_responder, team1_reward, team2_reward, ruleset_num_sets):
    """ Raises a ValueError if any of the inputs are out of range.
    """
    if result not in ["continue", "new_serve", "let"]:
        raise ValueError("The next state machine result must be: continue, new_serve, let!")
    if new_responder not in [1, 2]:
        raise ValueError("Responder/Team must be either: 1 or 2!")
    if (team1_reward not in [0, 1]) or (team2_reward not in [0, 1]):
        raise ValueError("Team point score must be either: 0 or 1!")
    if (ruleset_num_sets % 2) == 0:
        raise ValueError("Number of Sets for the match must be odd!")


def ruleset_singles(action, responder, location, bounce_count):
    """ Returns the result for the ball bounce in a game of singles
        with 2 players and then updates the score tracker state machine
        which can then be used to update the score.

        Args:
            action:
                s: Serve 1
                r: Rally
            responder:
                1: Player 1/Team 1
                2: Player 2/Team 2
            location:
                0: Out of Bounds
                1,2,3,4: Quadrants 1-4.
                5: Net
            bounce_count:
                1: First Bounce
                2: Second Bounce
        Returns:
            result:
                continue: No scoring, just wait for the next set of coordinate data to pass to this function call.
                new_serve: A score was determined, either through the server doing a fault or an illegal move, or
                           a returning player doing an illegal move.
                let: No scoring, just wait for the next set of coordinate data to pass to this function call.
            action: See Args. This is the next action for the state machine to take on the function call.
            responder: The team that will now serve or the team that is returning a ball.
            bounce_count: Either 1 or 2, represents the number of bounces made before a score is decided
                          (> 1 applies to serving only).
            team1_reward: Either 0 or 1. This is added to the score by the scoring function.
            team2_reward: Either 0 or 1. This is added to the score by the scoring function.

        Sources:
            General Server Rules: https://www.youtube.com/watch?v=s9ELscafqVs
            Basic Rules of Table Tennis: https://www.youtube.com/watch?v=9ZoEMXyP9Lc
    """
    # Make sure all the inputs are valid/within range.
    validate_ruleset_singles_inputs(action, responder, location, bounce_count)

    if action == "s":  # The current ball bounce occurs during a serve.
        # If Team 1 has a ball bounce on Q1/Q2 or Team 2 has a ball bounce on Q3/Q4
        if ((responder == 1) and (location in [1,2])) or ((responder == 2) and (location in [3,4])):
            if bounce_count == 1: # During serve, the ball did it's first bounce on the server team's side.
                result = "continue"
                action = "s"
                bounce_count = 2  # Bounce was legal, so now wait for second bounce.

                # No point was rewarded to either team because we need to wait for the second bounce
                # to determine which team gets a point or not.
                team1_reward = 0
                team2_reward = 0

            else:  # Ball bounced twice on the same side.
                result = "new_serve"
                action = "s"
                bounce_count = 1  # Reset the bounce count for a new serve.

                if responder == 1:  # If Team 1 hit tha ball, they lost the point.
                    team1_reward = 0
                    team2_reward = 1
                else:  # If Team 2 hit the ball, they lost the point.
                    team1_reward = 1
                    team2_reward = 0
    
        # If Team 1 has a ball bounce on Q3/Q4 or Team 2 has a ball bounce on Q1/Q2
        elif ((responder == 1) and (location in [3,4])) or ((responder == 2) and (location in [1,2])):
            if bounce_count == 1: # The player hit the first bounce of a service on the opposite side
                result = "new_serve"
                action = "s"
                bounce_count = 1  # Reset the bounce count for a new serve.

                if responder == 1:  # If Team 1 hit the ball, they lost the point.
                    team1_reward = 0
                    team2_reward = 1
                else:  # If Team 2 hit the ball, they lost the point.
                    team1_reward = 1
                    team2_reward = 0

            else:  # The ball bounced a second time, on the opposing player's side
                # Serve was legal, so the next ball bounce is expected to be during a rally.
                result = "continue"
                action = "r"
                bounce_count = 1  # Reset the ball bounce count (No longer needed for single's rallies)

                if responder == 1:
                    responder = 2  # The next ball will be received by Team 2
                else:
                    responder = 1  # The next ball will be received by Team 1

                # No points are rewarded as the game is still in play.
                team1_reward = 0
                team2_reward = 0
    
        elif location == 5:  # Either team hit the net/let
            # Since the ball hit the net, on the first or second bounce, the serve must be restarted
            # by the same player that did the let.
            result = "let"
            action = "s"
            bounce_count = 1  # Reset the bounce count for a redo serve.

            # No points rewarded for either team, and action is not changed, service must be redoed.
            team1_reward = 0
            team2_reward = 0
    
        else: # Ball was out of bounds.
            # No matter whether on the first or second bounce, if either player hits the ball out of
            # bounds, the player who currently served (the responder) loses the point,
            # and a new serve begins.
            result="new_serve"
            action = "s"
            bounce_count = 1  # Reset the bounce count for a new serve.

            # Reset the ball bounce count as a new serve will be in play.
            bounce_count = 1

            if responder == 1:  # If Team 1 hit the ball, they lost the point.
                team1_reward = 0
                team2_reward = 1
            else:  # If Team 2 hit the ball, they lost the point.
                team1_reward = 1
                team2_reward = 0

    elif action == "r":  # The current ball bounce occurs during a rally.
        # If Team 1 has a ball bounce on Q1/Q2/OOB or Team 2 has a ball bounce on Q3/Q4/OOB
        if ((responder == 1) and (location in [0,1,2])) or ((responder == 2) and (location in [0,3,4])):
            # The ball hit the same side as the player returning the ball in a rally,
            # so the player returning the ball loses a point, and the next serve begins.
            result = "new_serve"
            action = "s"
            #bounce_count = 1

            if responder == 1:  # If Team 1 hit the ball, they lost the point.
                team1_reward = 0
                team2_reward = 1
            else:  # If Team 2 hit the ball, they lost the point.
                team1_reward = 1
                team2_reward = 0
    
        # If Team 1 has a ball bounce on Q3/Q4 or Team 2 has a ball bounce on Q1/Q2
        elif ((responder == 1) and (location in [3,4])) or ((responder == 2) and (location in [1,2])):
            # The player returning the ball, returned the ball to the opposite player's side,
            # therefore continuing the rally.
            result = "continue"
            action = "r"
            #bounce_count = 1

            if responder == 1:
                responder = 2  # The next ball will now be received by Team 2
            else:
                responder = 1  # The next ball will now be received by Team 1

            # No points rewarded for either team as the rally is still ongoing.
            team1_reward = 0
            team2_reward = 0
        
        # The ball hit the net during a rally for either team.
        else:
            # Continue the rally and just watch for the next bounce to determine the next
            # team score.
            result = "continue"
            action = "r"
            #bounce_count = 1

            # No points rewarded for either team as the rally is still ongoing.
            team1_reward = 0
            team2_reward = 0
    else:
        raise ValueError("Action can only be: s1(Serve 1), s2 (Serve 2), and r (Rally)!")

    return result, action, responder, bounce_count, team1_reward, team2_reward


def ruleset_doubles(action, responder, location, bounce_count):
    """ Returns the result for the ball bounce in a game of doubles
        with 2 players and then updates the score tracker state machine
        which can then be used to update the score.

        As of the prototype, special clauses such as Service, Order of Play,
        Change of Service and Change of Ends are not considered as they require full
        tracking of each player's movements in play which is not possible in our hardware
        design.

        Args:
            action:
                s: Serve 1
                r: Rally
            responder:
                1: Player 1/Team 1
                2: Player 2/Team 2
            location:
                0: Out of Bounds
                1,2,3,4: Quadrants 1-4.
                5: Net
            bounce_count:
                1: First Bounce
                2: Second Bounce
        Returns:
            result:
                continue: No scoring, just wait for the next set of coordinate data to pass to this function call.
                new_serve: A score was determined, either through the server doing a fault or an illegal move, or
                           a returning player doing an illegal move.
                let: No scoring, just wait for the next set of coordinate data to pass to this function call.
            action: See Args. This is the next action for the state machine to take on the function call.
            responder: The team that will now serve or the team that is returning a ball.
            bounce_count: Either 1 or 2, represents the number of bounces made before a score is decided
                          (> 1 applies to serving only).
            team1_reward: Either 0 or 1. This is added to the score by the scoring function.
            team2_reward: Either 0 or 1. This is added to the score by the scoring function.

        Sources:
            General Server Rules: https://www.youtube.com/watch?v=s9ELscafqVs
            Basic Rules of Table Tennis: https://www.youtube.com/watch?v=9ZoEMXyP9Lc
            Double Server Rules: https://www.youtube.com/watch?v=ZWgX_k4Z1NQ
    """
    # Make sure all the inputs are valid/within range.
    validate_ruleset_doubles_inputs(action, responder, location, bounce_count)

    if action == "s":  # The current ball bounce occurs during a serve.
        # Doubles ruleset requires bouncing the first bounce on the right side of the serving player
        # followed by the second bounce landing on the right side of the receiving player.
         
        # If Team 1 has a ball bounce on Q1 or Team 2 has a ball bounce on Q4
        if ((responder == 1) and (location in [1])) or ((responder == 2) and (location in [4])):
            if bounce_count == 1: # During serve, the ball did it's first bounce on the server team's side.
                result = "continue"
                action = "s"
                bounce_count = 2  # Bounce was legal, so now wait for second bounce.

                # No point was rewarded to either team because we need to wait for the second bounce
                # to determine which team gets a point or not.
                team1_reward = 0
                team2_reward = 0

            else:  # Ball bounced twice on the same side.
                result = "new_serve"
                action = "s"
                bounce_count = 1  # Reset the bounce count for a new serve.

                if responder == 1:  # If Team 1 hit tha ball, they lost the point.
                    team1_reward = 0
                    team2_reward = 1
                else:  # If Team 2 hit the ball, they lost the point.
                    team1_reward = 1
                    team2_reward = 0
    
        # If Team 1 has a ball bounce on Q4 or Team 2 has a ball bounce on Q1
        elif ((responder == 1) and (location in [4])) or ((responder == 2) and (location in [1])):
            if bounce_count == 1: # The player hit the first bounce of a service on the opposite side
                result = "new_serve"
                action = "s"
                bounce_count = 1  # Reset the bounce count for a new serve.

                if responder == 1:  # If Team 1 hit the ball, they lost the point.
                    team1_reward = 0
                    team2_reward = 1
                else:  # If Team 2 hit the ball, they lost the point.
                    team1_reward = 1
                    team2_reward = 0

            else:  # The ball bounced a second time, on the opposing player's side
                # Serve was legal, so the next ball bounce is expected to be during a rally.
                result = "continue"
                action = "r"
                bounce_count = 1  # Reset the ball bounce count (No longer needed for single's rallies)

                if responder == 1:
                    responder = 2  # The next ball will be received by Team 2
                else:
                    responder = 1  # The next ball will be received by Team 1

                # No points are rewarded as the game is still in play.
                team1_reward = 0
                team2_reward = 0
    
        elif location == 5:  # Either team hit the net/let
            # Since the ball hit the net, on the first or second bounce, the serve must be restarted
            # by the same player that did the let.
            result = "let"
            action = "s"
            bounce_count = 1  # Reset the bounce count for a redo serve.

            # No points rewarded for either team, and action is not changed, service must be redoed.
            team1_reward = 0
            team2_reward = 0
    
        else: # Ball was out of bounds, or bounced on the left-side of either team.
            # No matter whether on the first or second bounce, if either player hits the ball out of
            # bounds, the player who currently served (the responder) loses the point,
            # and a new serve begins.
            result="new_serve"
            action = "s"
            bounce_count = 1  # Reset the bounce count for a new serve.

            # Reset the ball bounce count as a new serve will be in play.
            bounce_count = 1

            if responder == 1:  # If Team 1 hit the ball, they lost the point.
                team1_reward = 0
                team2_reward = 1
            else:  # If Team 2 hit the ball, they lost the point.
                team1_reward = 1
                team2_reward = 0

    # For doubles, aside from the order of the player 
    elif action == "r":  # The current ball bounce occurs during a rally.
        # If Team 1 has a ball bounce on Q1/Q2/OOB or Team 2 has a ball bounce on Q3/Q4/OOB
        if ((responder == 1) and (location in [0,1,2])) or ((responder == 2) and (location in [0,3,4])):
            # The ball hit the same side as the player returning the ball in a rally,
            # so the player returning the ball loses a point, and the next serve begins.
            result = "new_serve"
            action = "s"
            #bounce_count = 1

            if responder == 1:  # If Team 1 hit the ball, they lost the point.
                team1_reward = 0
                team2_reward = 1
            else:  # If Team 2 hit the ball, they lost the point.
                team1_reward = 1
                team2_reward = 0
    
        # If Team 1 has a ball bounce on Q3/Q4 or Team 2 has a ball bounce on Q1/Q2
        elif ((responder == 1) and (location in [3,4])) or ((responder == 2) and (location in [1,2])):
            # The player returning the ball, returned the ball to the opposite player's side,
            # therefore continuing the rally.
            result = "continue"
            action = "r"
            #bounce_count = 1

            if responder == 1:
                responder = 2  # The next ball will now be received by Team 2
            else:
                responder = 1  # The next ball will now be received by Team 1

            # No points rewarded for either team as the rally is still ongoing.
            team1_reward = 0
            team2_reward = 0
        
        # The ball hit the net during a rally for either team.
        else:
            # Continue the rally and just watch for the next bounce to determine the next
            # team score.
            result = "continue"
            action = "r"
            #bounce_count = 1

            # No points rewarded for either team as the rally is still ongoing.
            team1_reward = 0
            team2_reward = 0
    else:
        raise ValueError("Action can only be: s1(Serve 1), s2 (Serve 2), and r (Rally)!")

    return result, action, responder, bounce_count, team1_reward, team2_reward


def scoring_singles(result, previous_server, team1_reward, team2_reward, team1_score, team2_score, snapshot, ruleset_num_sets=3):
    """ Updates the score given in team1_score and team2_score which can then be read by the GUI.
    
        Args:
            result:
                new_serve: The next player will be chosen for the serve depending on the points for the set
                continue: Typically, no modification to the next player.
                let: Redo requires that the same player will serve, with no point change.
            server: This previous server.
                1 or 2: For Team 1 or Team 2.
            team1_reward: The point to add to Team 1's score returned from the ruleset function.
            team2_reward: The point to add to Team 2's score returned from the ruleset function.
            team1_score: Contains the updated sets and points score for the game, Team 1
            team2_score: Contains the updated sets and points score for the game, Team 2
            snapshot: The snapshot object to store the previous results before scoring gets applied.
            ruleset_num_sets: The number of sets for any player to reach before the game is over.
        
        Returns:
            new_server: An integer representing which team (1 or 2) is currently the server (if action='new_serve').
                        On all other actions, this variable doesn't mean anything, and is just passed through to
                        the ruleset funtion for the next state in the ruleset state machine.
            team1_score: Contains the updated sets and points score for the game, Team 1
            team2_score: Contains the updated sets and points score for the game, Team 2
            game_finished:
                True: Game is finished. Don't call this function anymore as doing so may give wrong scores.
                False: Game is in play
            team_that_won:  An integer representing which team won (1 or 2).

        Sources:
                General Server Rules: https://www.youtube.com/watch?v=s9ELscafqVs
                Basic Rules of Table Tennis: https://www.youtube.com/watch?v=9ZoEMXyP9Lc
    """
    # Make sure all the inputs are valid/within range.
    validate_scoring_singles_inputs(result, previous_server, team1_reward, team2_reward, ruleset_num_sets)

    # Save the snapshot just before the score gets modified.
    snapshot.save_snapshot(team1_score, team2_score, previous_server)

    # Only one team can get a point, not both.
    if team1_reward > 0:
        team1_score["points"] += 1
    elif team2_reward > 0:
        team2_score["points"] += 1

    difference_in_points_between_teams = abs(team1_score["points"] - team2_score["points"])

    # Only on a two point difference, can a set be awarded.
    if ( (team1_score["points"] >= 11) and (team1_score["points"] > team2_score["points"]) ) and (difference_in_points_between_teams >= 2):
        team1_score["sets"] += 1
        team1_score["points"] = 0
        team2_score["points"] = 0
    elif ( (team2_score["points"] >= 11) and (team2_score["points"] > team1_score["points"]) ) and (difference_in_points_between_teams >= 2):
        team2_score["sets"] += 1
        team1_score["points"] = 0
        team2_score["points"] = 0
    
    # Used to determine who the next server is.
    total_points_between_teams = team1_score["points"] + team2_score["points"]

    new_server = previous_server
    if result == "new_serve":
        # Upon every 2 points (if current set hasn't reached 10:10 yet) switch the server team.
        if ((total_points_between_teams < 20) and ((total_points_between_teams % 2) == 0)):
            if previous_server == 1:
                new_server = 2
            else:
                new_server = 1
            
        # If the total score is >= 20 (10:10 has been reached), keep switching the team for each serve.
        elif (total_points_between_teams >= 20):
            if previous_server == 1:
                new_server = 2
            else:
                new_server = 1

    # Now determine if the game is still in play or the game is over.
    game_finished = False
    team_that_won = 0
    total_sets_played = team1_score["sets"] + team2_score["sets"]
    if total_sets_played == ruleset_num_sets:
        game_finished = True
        if team1_score["sets"] > team2_score["sets"]:
            team_that_won = 1
        else:
            team_that_won = 2

    return new_server, team1_score, team2_score, game_finished, team_that_won


def scoring_doubles(result, previous_server, team1_reward, team2_reward, team1_score, team2_score, snapshot, ruleset_num_sets=3):
    """ Updates the score given in team1_score and team2_score which can then be read by the GUI.
        Currently, doubles and singles scoring functions are exactly the same. The differences in scoring,
        is in the clauses of Service, Order of Play, Change of Service and Change of Ends. Since
        no hardware tracking for players are possible, this implementation will be ignored on both
        the ruleset function and scoring functions for doubles.
    
        Args:
            result:
                new_serve: The next player will be chosen for the serve depending on the points for the set
                continue: Typically, no modification to the next player.
                let: Redo requires that the same player will serve, with no point change.
            server: This previous server.
                1 or 2: For Team 1 or Team 2.
            team1_reward: The point to add to Team 1's score returned from the ruleset function.
            team2_reward: The point to add to Team 2's score returned from the ruleset function.
            team1_score: Contains the updated sets and points score for the game, Team 1
            team2_score: Contains the updated sets and points score for the game, Team 2
            snapshot: The snapshot object to store the previous results before scoring gets applied.
            ruleset_num_sets: The number of sets for any player to reach before the game is over.
        
        Returns:
            new_server: An integer representing which team (1 or 2) is currently the server (if action='new_serve').
                        On all other actions, this variable doesn't mean anything, and is just passed through to
                        the ruleset funtion for the next state in the ruleset state machine.
            team1_score: Contains the updated sets and points score for the game, Team 1
            team2_score: Contains the updated sets and points score for the game, Team 2
            game_finished:
                True: Game is finished. Don't call this function anymore as doing so may give wrong scores.
                False: Game is in play
            team_that_won:  An integer representing which team won (1 or 2).

        Sources:
                General Server Rules: https://www.youtube.com/watch?v=s9ELscafqVs
                Basic Rules of Table Tennis: https://www.youtube.com/watch?v=9ZoEMXyP9Lc
    """
    # Make sure all the inputs are valid/within range.
    validate_scoring_doubles_inputs(result, previous_server, team1_reward, team2_reward, ruleset_num_sets)

    # Save the snapshot just before the score gets modified.
    snapshot.save_snapshot(team1_score, team2_score, previous_server)

    # Only one team can get a point, not both.
    if team1_reward > 0:
        team1_score["points"] += 1
    elif team2_reward > 0:
        team2_score["points"] += 1

    difference_in_points_between_teams = abs(team1_score["points"] - team2_score["points"])

    # Only on a two point difference, can a set be awarded.
    if ( (team1_score["points"] >= 11) and (team1_score["points"] > team2_score["points"]) ) and (difference_in_points_between_teams >= 2):
        team1_score["sets"] += 1
        team1_score["points"] = 0
        team2_score["points"] = 0
    elif ( (team2_score["points"] >= 11) and (team2_score["points"] > team1_score["points"]) ) and (difference_in_points_between_teams >= 2):
        team2_score["sets"] += 1
        team1_score["points"] = 0
        team2_score["points"] = 0
    
    # Used to determine who the next server is.
    total_points_between_teams = team1_score["points"] + team2_score["points"]

    new_server = previous_server
    if result == "new_serve":
        # Upon every 2 points (if current set hasn't reached 10:10 yet) switch the server team.
        if ((total_points_between_teams < 20) and ((total_points_between_teams % 2) == 0)):
            if previous_server == 1:
                new_server = 2
            else:
                new_server = 1
            
        # If the total score is >= 20 (10:10 has been reached), keep switching the team for each serve.
        elif (total_points_between_teams >= 20):
            if previous_server == 1:
                new_server = 2
            else:
                new_server = 1

    # Now determine if the game is still in play or the game is over.
    game_finished = False
    team_that_won = 0
    total_sets_played = team1_score["sets"] + team2_score["sets"]
    if total_sets_played == ruleset_num_sets:
        game_finished = True
        if team1_score["sets"] > team2_score["sets"]:
            team_that_won = 1
        else:
            team_that_won = 2

    return new_server, team1_score, team2_score, game_finished, team_that_won