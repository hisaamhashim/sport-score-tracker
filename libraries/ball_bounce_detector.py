"""
   Copyright 2022, San Jose State Univerity

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    Authors:
        - Hisaam Hashim
        - Jesse Pham
        - Nathan Luc
        - Huy Nguyen
    
    Description:
        This library analyzes either a series of camera feeds or an actual video recording
        and then overlays grids and shapes on objects such as a ball and table in order to
        monitor ball bounces and their respective coordinates on the table. CV2 is used
        for the analysis.
"""
import argparse
from collections import deque
import cv2
from enum import Enum
import imutils
from imutils.video import VideoStream
import math
import numpy as np
import time

COORDINATE_MAPPER_LIMITS = {"xmin": -55, "xmax": 55, "ymin": -100, "ymax": 100}

##############################
# Defaults RGB for Ball Bounce
# Lower_RGB = (164, 153, 150)
# Lower_HSV = (12, 22, 164)
#
# Higher_RGB = (255, 123, 0)
# Higher_HSV = (29, 255, 255)
##############################
def rgb_to_hsv(r, g, b):
 
    # R, G, B values are divided by 255
    # to change the range from 0..255 to 0..1:
    r, g, b = r / 255.0, g / 255.0, b / 255.0
 
    # h, s, v = hue, saturation, value
    cmax = max(r, g, b)    # maximum of r, g, b
    cmin = min(r, g, b)    # minimum of r, g, b
    diff = cmax-cmin       # diff of cmax and cmin.
 
    # if cmax and cmax are equal then h = 0
    if cmax == cmin:
        h = 0
     
    # if cmax equal r then compute h
    elif cmax == r:
        h = (60 * ((g - b) / diff) + 360) % 360
 
    # if cmax equal g then compute h
    elif cmax == g:
        h = (60 * ((b - r) / diff) + 120) % 360
 
    # if cmax equal b then compute h
    elif cmax == b:
        h = (60 * ((r - g) / diff) + 240) % 360
 
    # if cmax equal zero
    if cmax == 0:
        s = 0
    else:
        s = (diff / cmax) * 100
 
    # compute v
    v = cmax * 100

    # Scale for 0->255 for S and V instead of %
    hsv_values = (math.ceil(h), math.ceil((s * 255) / 100), math.ceil((v * 255) / 100))

    return hsv_values


def _find_x_linear(y, m, b):
    return (y - b)/m

# TODO: The coordinate mappine is not accurate to the table location!
def _transform_coord(x, y, x1, y1, y2, info_table, mapper_limits=COORDINATE_MAPPER_LIMITS):
    #Transform to origin
    (x,y) = (x - x1, y - y1)

    #Convert using table
    if x!=0:
        x = (x + info_table[y][2]) * ( (mapper_limits["ymax"] * 2) / info_table[y][1] )
    if y!=0:
        y = (y) * ( (mapper_limits["ymax"] * 2) / (y2 - y1))
    return (x, y)


# TODO: Add functionality to add percentages from init to customize boundaries from YAML file.
def _get_overlay_grid_parameters(video_width, video_height=None):
    if video_height is None:
        video_height = math.ceil(video_width * 0.5625)

    middle_table_horizontal_value = video_width // 2
    top_of_net_location_y = math.floor(video_height * 0.65903307888040712468193384223919)
    bottom_of_net_location_y = math.floor(video_height * 0.8117048346055979643765903307888)
    pixel_distance_from_center_offset_top = math.floor(video_width * 0.315)
    pixel_distance_from_center_offset_bottom = math.floor(video_width * 0.475)

    y_offset = math.floor(video_width * 0.011)
    (x1, y1) = (middle_table_horizontal_value - pixel_distance_from_center_offset_top, top_of_net_location_y - y_offset)
    (x2, y2) = (middle_table_horizontal_value - pixel_distance_from_center_offset_bottom, bottom_of_net_location_y + y_offset)
    (x3, y3) = (middle_table_horizontal_value + pixel_distance_from_center_offset_top, top_of_net_location_y - y_offset)
    (x4, y4) = (middle_table_horizontal_value + pixel_distance_from_center_offset_bottom, bottom_of_net_location_y + y_offset)

    # Transfer to origin coordinates.
    (x1o, y1o) = (0,0)
    (x2o, y2o) = (x2 - x1, y2 - y1)
    (x3o, y3o) = (x3 - x1, y3 - y1)
    (x4o, y4o) = (x4 - x1, y4 - y1)  

    # Find the functions that go through 1, 2, 3 and 4
    m1 = (y2o - y1o)/(x2o - x1o)
    b1 = y2o - m1*x2o 

    m2 = (y4o - y3o)/(x4o - x3o)
    b2 = y4o - m2*x4o

    info_table = [ [0]*3 for i in range(y1o, y2o + 1)]

    for y in range(y1o, y2o + 1):
        info_table[y][0] = y

        #length from left to right
        info_table[y][1] = _find_x_linear(y, m1, b1) - _find_x_linear(y, m2, b2)

        #length from left to y
        info_table[y][2] = 0 - _find_x_linear(y, m1, b1)

    return (video_height,
            x1,
            y1,
            y2,
            info_table,
            middle_table_horizontal_value,
            top_of_net_location_y,
            bottom_of_net_location_y,
            pixel_distance_from_center_offset_top,
            pixel_distance_from_center_offset_bottom)


def _find_mapper_quadrant(x, y, mapper_limits=COORDINATE_MAPPER_LIMITS):
    quadrant = 0

    # TODO: Move to the _transform_coord function.
    # Seems like this only works on singles.
    x = abs(x)
    y = abs(y)
    #print("[BEFORE] x: %s, y: %s" % (x, y))

    x = x - mapper_limits["ymax"]
    y = y - mapper_limits["xmax"]
    #print("[AFTER] x: %s, y: %s" % (x, y))

    temp_x = x
    temp_y = y
    x = temp_y
    y = temp_x
    #print("[FLIPPED] x: %s, y: %s" % (x, y))

    # NOTE: No OUT of bounds will be detected as algorithm only counts bounce on table.
    if x < 0 and y > 0:
        quadrant = 1
    elif x > 0 and y > 0:
        quadrant = 2
    elif x < 0 and y < 0:
        quadrant = 3
    elif x > 0 and y < 0:
        quadrant = 4

    # Finally, set the x and y values to be in the range of the mapper.
    if x < mapper_limits["xmin"]:
        x = mapper_limits["xmin"] - 1
    elif x > mapper_limits["xmax"]:
        x = mapper_limits["xmax"] - 1
    if y < mapper_limits["ymin"]:
        y = mapper_limits["ymin"] - 1
    elif y > mapper_limits["ymax"]:
        y = mapper_limits["ymax"] - 1

    #print("Bounced on Q%s" % quadrant)
    return quadrant, (x, y)


class BallBounceDetector(object):
    """
        Uses CV2 to identify the table and ball and 
        monitor ball bounces. A video can be analyzed
        or a live video feed. Up to two cameras can
        be analyzed simultaneously at 30, 60 or 120 fps.
    """
    _debug = False
    _verbose = False
    _save_image= "ball_bounce_proof.png"  # artifacts/current_session/ball_bounce_proof.png
    _incoming_ball_bounce_metrics = {}
    _camera_devices = {"primary": "/dev/video4", "secondary": "/dev/video8"}
    _camera_resolution = (1920, 1080)
    _supported_video_formats = [".mp4", ".avi"]
    _video_files = [None, None]
    _cameras_are_ready = [False, False]
    _ball_color_hsv_higher_bound = (29, 255, 255) # Highest lighting condition value for ball color
    _ball_color_hsv_lower_bound = (12, 22, 164) # Lowest lighting condition value for ball color
    _colors = {"blue": (0, 0, 255), "yellow": (255, 255, 0), "red": (255, 0, 0), "green": (0, 255, 0), "pink": (255, 0, 255)}
    _supported_directions = ["up", "down"]
    _wait_for_service_after_double_bounce_time_s = 5  # If 5 seconds have elapsed since the last double bounce, count it.

    # These two are controlled by the feedback from the Score Tracker Coordinate 2D Mapper API.
    _score_tracker_action = None
    _top_view_rally_last_bounce_time = 0
    _out_of_bounds_trigger_time_during_rally_s = 15

    _top_view_video_stream = None
    _top_view_frame_counter = 0
    _top_view_last_frame = None
    _top_view_ball_location_points = deque(maxlen=64)
    _top_view_video_width = 1920
    _top_view_video_height = 1080
    _top_view_number_of_bounces = 0
    _top_view_can_bounce = False
    _top_view_no_bounce_counter = 0
    _top_view_ball_bounce_last_location = (0, 0)
    _top_view_ball_current_y_direction = "up"
    _top_view_overlay_x1 = 0
    _top_view_overlay_y1 = 0
    _top_view_overlay_y2 = 0
    _top_view_overlay_info_table = None
    _top_view_height_of_start_ball = 0
    _top_view_table_coordinates = None
    _top_view_ball_bounce_was_detected = False
    _top_view_last_quadrant_bounced = 0
    _top_view_same_side_bounces = 0
    _top_view_last_bounce_time = 0

    _side_view_video_stream = None
    _side_view_frame_counter = 0
    _side_view_ball_location_points = deque(maxlen=64)
    _side_view_video_width = 700
    _side_view_location_top_of_net = 275
    _side_view_location_bottom_of_net = 320
    _side_view_pixel_distance_from_center_offset = 338
    _side_view_ball_current_y_direction = "up"
    _side_view_passed_table = False
    _side_view_can_bounce = False
    _side_view_number_of_bounces = 0
    _side_view_ball_bounce_was_detected = False


    def __init__(self, camera_devices, camera_resolution=(1920, 1080), ball_colors=None, save_image=None, video_files=None, oob_inverval=15, verbose=False, debug=False):
        if verbose:
            self._verbose = verbose
        if debug:
            self._debug = debug

        if save_image is not None:
            self._save_image = save_image
            print("Ball Bounce proof will now be stored at: %s" % self._save_image)
    
        self._camera_devices = camera_devices
        self._camera_resolution = camera_resolution
        print("Cameras selected: %s" % self._camera_devices)

        # Initialize the ball bounce metrics.
        self._incoming_ball_bounce_metrics = {"ready": False,
                                              "x": 0, "y": 0,
                                              "timeout_exceeded": False,
                                              "bounce_proof": self._save_image}

        # Link the video files if present.
        if video_files is not None:
            if isinstance(video_files, str):
                self._video_files[0] = video_files
            elif isinstance(video_files, list):
                self._video_files = video_files
                print("Video files selected: %s" % self._video_files)
        
        # Destroy all prexisting frames:
        self._top_view_video_stream = None
        self._side_view_video_stream = None

        # Modify the overview grid colors from RGB to BGR.
        self._colors = {"blue": (0, 0, 255), "yellow": (255, 255, 0), "red": (255, 0, 0), "green": (0, 255, 0), "pink": (255, 0, 255)}
        self._colors["red"] = tuple(reversed(self._colors["red"]))
        self._colors["green"] = tuple(reversed(self._colors["green"]))
        self._colors["blue"] = tuple(reversed(self._colors["blue"]))
        self._colors["pink"] = tuple(reversed(self._colors["pink"]))
        self._colors["yellow"] = tuple(reversed(self._colors["yellow"]))
        print("Overlay boundary colors are changed from RGB to BGR.")

        # Take the RGB colors and convert to HSV space.
        if ball_colors is not None:
            # Convert the RGB colors to HSV
            self._ball_color_hsv_lower_bound = rgb_to_hsv(ball_colors[0][0], ball_colors[0][1], ball_colors[0][2])
            self._ball_color_hsv_higher_bound = rgb_to_hsv(ball_colors[1][0], ball_colors[1][1], ball_colors[1][2])
            print("New Ball Lower Bound HSV: ", self._ball_color_hsv_lower_bound)
            print("New Ball Higher Bound HSV: ", self._ball_color_hsv_higher_bound)
            print("Ball Colors converted from RGB space to HSV space")
        else:
            print("Default Ball Lower Bound HSV: ", self._ball_color_hsv_lower_bound)
            print("Default Ball Higher Bound HSV: ", self._ball_color_hsv_higher_bound)      

        # Specify the OOB Interval (When OOB is triggered after no bounce occurs in a rally for some time).
        self._out_of_bounds_trigger_time_during_rally_s = oob_inverval

        # Initialize the cameras with CV2 VideoCapture
        if self._camera_devices["primary"] is not None or (self._video_files is not None and self._video_files[0] is not None):
            self._configure_top_view_camera_feed()
        elif self._camera_devices["secondary"] is not None or (self._video_files is not None and self._video_files[1] is not None):
            self._configure_side_view_camera_feed()
        else:
            raise ValueError("Could not initialize the library since neither a camera feed nor video file was passed to the library.")

    ############################################
    # Functions with the Coordinate 2D Mapper
    ############################################
    def set_action(self, action):
        supported_actions = ["rally", "server", None]

        if action in supported_actions:
            self._score_tracker_action = action
            if self._verbose:
                print("Game Action set to: %s" % self._score_tracker_action)

    ############################################
    # Functions shared between primary(top)
    # and secondary(side-view) cameras.
    ############################################
    # TODO: Maybe we can merge into a different function?
    def _reset_defaults(self, reset_side_view=False):
        if reset_side_view:
            self._side_view_ball_current_y_direction = "up"
            self._side_view_passed_table = False
            self._side_view_can_bounce = False
            self._side_view_number_of_bounces = 0
            self._side_view_frame_counter = 0
        else:
            self._top_view_ball_bounce_last_location = (0, 0)
            self._top_view_ball_current_y_direction = "up"
            self._top_view_can_bounce = False
            self._top_view_number_of_bounces = 0
            self._top_view_no_bounce_counter = 0
            self._top_view_last_frame = False

    def _get_ball_y_value(self, ball_index):
        return ball_index[1]
    
    def _get_ball_x_value(self, ball_index):
        return ball_index[0]

    def _reset_ball(self):
        self._top_view_can_bounce = False
        self._top_view_number_of_bounces = 0
        self._top_view_no_bounce_counter = 0

    # TODO: This function is shared with both primary(top) and secondary(side-view) cameras.
    def terminate_cameras(self):
        # PRIMARY CAMERA
        if self._top_view_video_stream is not None:
            if self._video_files[0] is None:
                self._top_view_video_stream.stop()
            else:
                self._top_view_video_stream.release()
            self._top_view_video_stream = None

        # Secondary camera is optional, so only do the release if it is present.
        if self._side_view_video_stream is not None:
            if self._video_files[1] is None:
                self._side_view_video_stream.stop()
            else:
                self._side_view_video_stream.release()
            self._side_view_video_stream = None

        # Stop all windows.
        cv2.destroyAllWindows()

    ############################################
    # PRIMARY (TOP-VIEW) CAMERA FUNCTIONS
    ############################################
    # TODO: Add functionality to add percentages from init to customize boundaries from YAML file.
    def _configure_overlay_top_view(self, scale_video_width=700):
        self._top_view_video_width = scale_video_width
        
        overlay_parameters = _get_overlay_grid_parameters(video_width=scale_video_width, video_height=None)
        self._top_view_video_height = overlay_parameters[0]
        self._top_view_overlay_x1 = overlay_parameters[1]
        self._top_view_overlay_y1 = overlay_parameters[2]
        self._top_view_overlay_y2 = overlay_parameters[3]
        self._top_view_overlay_info_table = overlay_parameters[4]

        middle_table_horizontal_value = overlay_parameters[5]

        self._top_view_top_of_net_location_y = overlay_parameters[6]
        self._top_view_bottom_of_net_location_y = overlay_parameters[7]

        pixel_distance_from_center_offset_top = overlay_parameters[8]
        pixel_distance_from_center_offset_bottom = overlay_parameters[9]

        middle_of_table_offset_x = math.floor(scale_video_width * 0.375)
        table_bottom_left_point_x = middle_table_horizontal_value - pixel_distance_from_center_offset_bottom
        table_bottom_right_point_x = middle_table_horizontal_value + pixel_distance_from_center_offset_bottom
        table_top_left_point_x = middle_table_horizontal_value - pixel_distance_from_center_offset_top
        table_top_right_point_x = middle_table_horizontal_value + pixel_distance_from_center_offset_top
        table_mid_left_x = middle_table_horizontal_value - middle_of_table_offset_x
        table_mid_right_x = middle_table_horizontal_value + middle_of_table_offset_x
        table_mid_height = ((self._top_view_top_of_net_location_y + self._top_view_bottom_of_net_location_y) // 2) - 9

        self._top_view_height_of_start_ball = math.floor(self._top_view_video_height* 0.254)

        # TODO: Use a dictionary instead to make it more readable.
        self._top_view_table_coordinates = [
            table_top_left_point_x,
            table_top_right_point_x,
            self._top_view_top_of_net_location_y,
            table_mid_left_x,
            table_mid_right_x,
            table_mid_height,
            table_bottom_left_point_x,
            table_bottom_right_point_x,
            self._top_view_bottom_of_net_location_y,
            middle_table_horizontal_value,
            0,
            0]

    def _configure_top_view_camera_feed(self):
        # Only allow supported formats to be played.
        if self._video_files[0] is not None and self._video_files[0][-4:] in self._supported_video_formats:
            # Play a video file.
            self._top_view_video_stream = cv2.VideoCapture(self._video_files[0])
    
            self._cameras_are_ready[0] = True
            if self._verbose:
                print("Primary Video file camera playback selected: %s." % self._video_files[0])
            #time.sleep(2)
        else:
            # Connect to a live camera.
            camera_device_id = int(self._camera_devices["primary"][10:])
            print("Primary Camera Device: %s(%s)" % (self._camera_devices["primary"], camera_device_id))
    
            self._top_view_video_stream = VideoStream(
                src=camera_device_id,
                resolution=self._camera_resolution).start()

            # Wait a few seconds for the feed to link.
            if self._verbose:
                print("Primary Live Camera feed is getting started...")
            time.sleep(2.0)
            self._cameras_are_ready[0] = True
        
        # Configure all the dependency variables for other functions.
        # TODO: Scaling to 700 from 1920 breaks some bounces!
        self._configure_overlay_top_view(scale_video_width=self._camera_resolution[0])
        self._reset_defaults(reset_side_view=False)

    def _ball_bounce_top_view(self, table_l, table_r, net_top, net_bot, no_bounce_wait_limit=60):
        y_distance_offset = math.floor(self._top_view_video_width * 0.011)

        # Parameters that will be changed.
        ball_bounce_coordinates = None
        ball_has_bounced = False
        x_distance_offset = 0
        
        # Based on the number of bounces, modify the values.
        if self._top_view_number_of_bounces == 0:
            x_distance_offset = math.floor(self._top_view_video_width * 0.020)
        if self._top_view_number_of_bounces > 0:
            self._top_view_no_bounce_counter += 1
        if self._top_view_no_bounce_counter >= no_bounce_wait_limit:
            self._reset_ball()
            if self._verbose:
                print("Ball did not bounce in time")
            
            # False, None -> Indicates ball didn't bounced, hence no coordinates.
            return ball_has_bounced, ball_bounce_coordinates

        if self._top_view_ball_current_y_direction == "down":
            if self._top_view_can_bounce == True:
                # Upward motion for the ball.
                if self._get_ball_y_value(self._top_view_ball_location_points[0]) < self._get_ball_y_value(self._top_view_ball_location_points[1]):
                    self._top_view_number_of_bounces += 1

                    # Calculate the Coordinate 2D Mapper coordinates based on the (X,Y) pixel coordinates of the ball.
                    ball_bounce_coordinates = _transform_coord(
                        x=self._top_view_ball_location_points[1][0],
                        y=self._top_view_ball_location_points[1][1],
                        x1=self._top_view_overlay_x1,
                        y1=self._top_view_overlay_y1,
                        y2=self._top_view_overlay_y2,
                        info_table=self._top_view_overlay_info_table)
                    
                    if self._verbose:
                        print("Bounced %s at %s." % (self._top_view_number_of_bounces, ball_bounce_coordinates))
                            
                    self._top_view_ball_bounce_last_location = self._top_view_ball_location_points[0]
                    self._top_view_ball_current_y_direction = "up"
                    self._top_view_can_bounce = False
                    self._top_view_no_bounce_counter = 0
                    ball_has_bounced = True
    
                # Downward motion for the ball.
                elif self._get_ball_y_value(self._top_view_ball_location_points[0]) > (net_bot):
                    self._reset_ball()

                    # False, None -> Indicates ball didn't bounced, hence no coordinates.
                    return ball_has_bounced, ball_bounce_coordinates
    
            else:
                # Ball is still in motion, hence can_bounce = True
                if (self._get_ball_x_value(self._top_view_ball_location_points[0]) >= table_l + x_distance_offset) and (self._get_ball_x_value(self._top_view_ball_location_points[0]) <= table_r - x_distance_offset):	# within x boundaries
                    if (self._get_ball_y_value(self._top_view_ball_location_points[0]) >= net_top - y_distance_offset) and (self._get_ball_y_value(self._top_view_ball_location_points[0]) <= net_bot):	# within y boundaries
                        self._top_view_can_bounce = True
        else:
            if self._get_ball_y_value(self._top_view_ball_location_points[0]) > self._get_ball_y_value(self._top_view_ball_location_points[1]) + y_distance_offset:
                self._top_view_ball_current_y_direction = "down"

        # True, (X,Y) -> Will indicate that the ball bounced successfully
        return ball_has_bounced, ball_bounce_coordinates

    def analyze_top_view_camera(self, loop_video=False):
        self._top_view_ball_bounce_was_detected = False

        if self._cameras_are_ready[0]:
            # (return_code, frame) is returned from read() call.
            if self._video_files[0] is None:
                self._top_view_frame = self._top_view_video_stream.read()
            else:
                _, self._top_view_frame = self._top_view_video_stream.read()
            self._top_view_frame_counter += 1

            # If the stream is a video file, choose a different frame to read.
            if self._video_files[0] is not None and self._video_files[0][-4:] in self._supported_video_formats:
                # Restart the video from the beginning if the last frame is already read.
                if loop_video and self._top_view_frame_counter == self._top_view_video_stream.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT):
                    if self._verbose:
                        print("Looping video.")
                    self._top_view_frame_counter = 0
                    self._top_view_video_stream.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0)

            # If the frame is empty, stop the cameras.
            if self._top_view_frame is None:
                if self._verbose:
                    print("No frame!")
                self._cameras_are_ready[0] = False

            # resize the frame, blur it, and convert it to the HSV
            # color space
            self._top_view_frame = imutils.resize(self._top_view_frame, height=self._top_view_video_height, width=self._top_view_video_width)
            blur_frame = cv2.GaussianBlur(self._top_view_frame, (5, 5), 0)
            hsv_frame = cv2.cvtColor(blur_frame, cv2.COLOR_BGR2HSV)
            
            # construct a mask for the defined color, then perform
            # a series of dilations and erosions to remove any small
            # blobs left in the mask
            mask = cv2.inRange(hsv_frame, self._ball_color_hsv_lower_bound, self._ball_color_hsv_higher_bound) # lower, higher
            mask = cv2.dilate(mask, None, iterations=2)

            # For some reason csv.line uses RGB colors as BGR, so you need to reverse the RGB tuple/list.
            table_line_colors = [
                self._colors["green"],
                self._colors["blue"],
                self._colors["pink"],
                self._colors["red"]]

            # Draw the overlay lines for the table.
            cv2.line(
                self._top_view_frame,
                (self._top_view_table_coordinates[9], self._top_view_table_coordinates[2]),
                (self._top_view_table_coordinates[9], self._top_view_table_coordinates[8]),
                table_line_colors[3], 1)
            cv2.line(
                self._top_view_frame,
                (self._top_view_table_coordinates[0], self._top_view_table_coordinates[2]),
                (self._top_view_table_coordinates[1], self._top_view_table_coordinates[2]),
                table_line_colors[0], 1)
            cv2.line(
                self._top_view_frame,
                (self._top_view_table_coordinates[3], self._top_view_table_coordinates[5]),
                (self._top_view_table_coordinates[4], self._top_view_table_coordinates[5]),
                table_line_colors[1], 1)
            cv2.line(
                self._top_view_frame,
                (self._top_view_table_coordinates[6], self._top_view_table_coordinates[8]),
                (self._top_view_table_coordinates[7], self._top_view_table_coordinates[8]),
                table_line_colors[2], 1)
            cv2.line(
                self._top_view_frame,
                (self._top_view_table_coordinates[6], self._top_view_table_coordinates[8]),
                (self._top_view_table_coordinates[0], self._top_view_table_coordinates[2]),
                self._colors["red"], 1)
            cv2.line(
                self._top_view_frame,
                (self._top_view_table_coordinates[7], self._top_view_table_coordinates[8]),
                (self._top_view_table_coordinates[1], self._top_view_table_coordinates[2]),
                self._colors["red"], 1)
            cv2.line(
                self._top_view_frame,
                (0, self._top_view_height_of_start_ball),
                (self._top_view_video_width, self._top_view_height_of_start_ball),
                self._colors["red"], 1)

            # Encircle the ball in the image.
            contours_found_in_mask = cv2.findContours(
                mask.copy(),
                cv2.RETR_EXTERNAL,
                cv2.CHAIN_APPROX_SIMPLE)
            contours_found_in_mask = imutils.grab_contours(contours_found_in_mask)
            center = None

            if len(contours_found_in_mask) > 0:
                largest_contour_found = max(contours_found_in_mask, key=cv2.contourArea)
                ((circle_x, circle_y), circle_radius) = cv2.minEnclosingCircle(largest_contour_found)
                countour_moments = cv2.moments(largest_contour_found)
                center = (int(countour_moments["m10"] / countour_moments["m00"]), int(countour_moments["m01"] / countour_moments["m00"]))

                if circle_radius > 5:
                    cv2.circle(
                        self._top_view_frame,
                        (int(circle_x), int(circle_y)),
                        int(circle_radius),
                        self._colors["yellow"],
                        2)
                    self._top_view_ball_location_points.appendleft(center)

            # Analyze the pixels of the ball within the table_coordinates while the ball is in motion and determine if a ball bounce has occurred.
            if len(self._top_view_ball_location_points) >= 2 and self._top_view_ball_location_points[0] is not None and self._top_view_ball_location_points[1] is not None:
                self._top_view_ball_bounce_was_detected, ball_bounce_coordinates = self._ball_bounce_top_view(self._top_view_table_coordinates[6], self._top_view_table_coordinates[7], self._top_view_table_coordinates[2], self._top_view_table_coordinates[8])
                
                # Save the image for when the ball has bounced.
                if self._top_view_ball_bounce_was_detected:
                    # The bounce proof should be the last frame that just before the bounce was detected.
                    mapper_quadrant, ball_bounce_coordinates = _find_mapper_quadrant(ball_bounce_coordinates[0], ball_bounce_coordinates[1])

                    # Save the time when the last bounce was made.
                    if self._score_tracker_action == "rally":
                        self._top_view_rally_last_bounce_time = time.time()

                    ############################################
                    # DOUBLE BOUNCE LOGIC
                    ############################################
                    if self._debug:
                        print("Previous: %s, Current: %s" % (self._top_view_last_quadrant_bounced, mapper_quadrant))

                    # If the last bounce before and current bounce occurs on the same side, 
                    if (self._top_view_last_quadrant_bounced in [3, 4] and mapper_quadrant in [3, 4]) or (self._top_view_last_quadrant_bounced in [1, 2] and mapper_quadrant in [1, 2]):
                        self._top_view_same_side_bounces += 1

                        # Measure the last time the double bounce occurred.
                        current_time = time.time()
                        time_since_last_double_bounce = current_time - self._top_view_last_bounce_time

                        # If the double bounce occurred to quickly, do not count the ball bounce.
                        #if self._debug:
                        #    print("Time since last double bounce: %s seconds." % time_since_last_double_bounce)

                        if self._top_view_same_side_bounces > 2:
                            if time_since_last_double_bounce <= self._wait_for_service_after_double_bounce_time_s:
                                #print("More bounces after double bounce! Ignoring (Double bounces: %s)" % self._top_view_same_side_bounces)
            
                                # Set the ball bounce flag to false.
                                self._top_view_ball_bounce_was_detected = False

                            else:
                                # If the time elapsed (double bounce -> wait -> serve on double bounce side), count the bounce.
                                self._top_view_same_side_bounces = 0
                                #if self._debug:
                                #    print("New Service on the same side, Ignoring the double bounce.")

                        # Save the time again for measuring when a double bounce occurs.
                        self._top_view_last_bounce_time = current_time

                    else:
                        # If the ball bounce on a different side of the table, save the time.
                        #print("Bounce occurred on the opposite side of the table!")
                        self._top_view_last_bounce_time = time.time()  # Save a new time.

                    # Store the last quadrant that was bounced to check for double bounces later.
                    self._top_view_last_quadrant_bounced = mapper_quadrant
                
                else:
                    ##################################################
                    # OUT-OF-BOUNDS CONDITION
                    ##################################################
                    if self._score_tracker_action == "rally":
                        current_time = time.time()
                        time_since_last_bounce = current_time - self._top_view_rally_last_bounce_time

                        # If the current ball last bounced in a rally and this last bounce didn't bounce
                        # for say 15 seconds, then automatically trigger the OOB condition to be returned.
                        if time_since_last_bounce >= self._out_of_bounds_trigger_time_during_rally_s:
                            self._incoming_ball_bounce_metrics["ready"] = True
                            self._incoming_ball_bounce_metrics["x"] = 0
                            self._incoming_ball_bounce_metrics["y"] = 105

                            # Set action back to None unless rally is somehow still going on.
                            self._score_tracker_action = None

                            # Save the proof to the file.
                            cv2.imwrite(self._save_image, self._top_view_last_frame)

                            if self._verbose:
                                print("OOB has occurred because a new bounce during a rally didn't occur after %s seconds." % self._out_of_bounds_trigger_time_during_rally_s)

                # This may have changed again, so if it hasn't, save the proof to file.
                if self._top_view_ball_bounce_was_detected:
                    cv2.imwrite(self._save_image, self._top_view_last_frame)
                    #if self._debug:
                    #    input("Press ENTER to continue.")
                    if self._verbose:
                        print("Ball Bounce proof was saved.")

            # Wait some time before moving onto the next frame.
            time.sleep(0.005)
            self._top_view_last_frame = self._top_view_frame

            # TODO: Comment this out once you verify this works.
            if self._debug:
                cv2.imshow("Top View Feed", self._top_view_frame)
                key = cv2.waitKey(1) & 0xFF

            # If the ball has bounced, return the coordinates.
            if self._top_view_ball_bounce_was_detected and ball_bounce_coordinates is not None:
                if self._verbose:
                    print("Ball Bounce was detected!")
                # Update the metrics.
                self._incoming_ball_bounce_metrics["ready"] = True
                self._incoming_ball_bounce_metrics["x"] = ball_bounce_coordinates[0]
                self._incoming_ball_bounce_metrics["y"] = ball_bounce_coordinates[1]

            else:
                if self._verbose:
                    print("No ball bounce.")

                # Update the metrics.
                self._incoming_ball_bounce_metrics["ready"] = False
                self._incoming_ball_bounce_metrics["x"] = 0
                self._incoming_ball_bounce_metrics["y"] = 0        

        else:
            if self._verbose:
                print("Top-view camera stream is not ready!")
        
        return self._cameras_are_ready[0], self._top_view_ball_bounce_was_detected, self._incoming_ball_bounce_metrics

    ############################################
    # SECONDARY (SIDE-VIEW) CAMERA FUNCTIONS
    ############################################
    def _configure_overlay_side_view(self, live_feed=False):
        # TODO: Do the parameters below need to get calibrated on every new run?
        if live_feed:
            self._side_view_location_top_of_net = 347
            self._side_view_location_bottom_of_net = 387
        else:
            self._side_view_location_top_of_net = 275
            self._side_view_location_bottom_of_net = 320

        # Configure the other parameters.
        self._side_view_middle_table_horizontal_value = self._side_view_video_width // 2  # TODO: Is this equivalent to int(x)?
        self._side_view_location_left_of_table = self._side_view_middle_table_horizontal_value - self._side_view_pixel_distance_from_center_offset
        self._side_view_location_right_of_table = self._side_view_middle_table_horizontal_value + self._side_view_pixel_distance_from_center_offset

    def _configure_side_view_camera_feed(self):
        # Only allow supported formats to be played.
        if self._video_files[1] is not None and self._video_files[1][-4:] in self._supported_video_formats:
            # Play a video file.
            self._side_view_video_stream = cv2.VideoCapture(self._video_files[1])
    
            self._cameras_are_ready[1] = True
            self._configure_overlay_side_view(live_feed=False)

            if self._verbose:
                print("Secondary Video file camera playback selected: %s." % self._video_files[1])
            #time.sleep(2)
        else:
            # Connect to a live camera.
            camera_device_id = int(self._camera_devices["secondary"][10:])
            print("Secondary Camera Device: %s(%s)" % (self._camera_devices["secondary"], camera_device_id))
    
            self._side_view_video_stream = VideoStream(
                src=camera_device_id,
                resolution=self._camera_resolution).start()

            # Wait a few seconds for the feed to link.
            if self._verbose:
                print("Secondary Live Camera feed is getting started...")
            time.sleep(2.0)
    
            self._cameras_are_ready[1] = True
            self._configure_overlay_side_view(live_feed=True)
        
        # Reset the defaults.
        self._reset_defaults(reset_side_view=False)

    def _ball_bounce_side_view(self, table_l, table_r, net_top, net_bot):
        x_distance_offset = 0
        y_distance_offset = 5

        ball_bounce_detected = False
        ball_bounce_coordinates = (0, 0)

        if self._side_view_number_of_bounces == 0:
            x_distance_offset = 35

        if self._side_view_ball_current_y_direction == "down":
            if (self._side_view_can_bounce == True) and (self._side_view_passed_table == False):
                # If an upward motion occurred
                if self._get_ball_y_value(self._side_view_ball_location_points[0]) < self._get_ball_y_value(self._side_view_ball_location_points[1]): 	
                    self._side_view_number_of_bounces += 1
                    if self._verbose:
                        print("Ball was Bounced to: %s:%s" % (
                            self._side_view_number_of_bounces,
                            self._side_view_ball_location_points[1]))

                    # Update the tuple below to indicate bounce occurred as well as WHERE it occurred.
                    ball_bounce_detected = True
                    ball_bounce_coordinates = self._side_view_ball_location_points[1]
                    
                    self._side_view_can_bounce = False
                    # Change the Y-direction of the ball to UP.
                    self._side_view_ball_current_y_direction = "up"

                elif self._get_ball_y_value(self._side_view_ball_location_points[0]) > (net_bot + y_distance_offset):
                    # TODO: Needs to be reset when starting next rally
                    self._side_view_passed_table = True

            else:
                if (self._get_ball_x_value(self._side_view_ball_location_points[0]) >= table_l + x_distance_offset) and (self._get_ball_x_value(self._side_view_ball_location_points[0]) <= table_r - x_distance_offset):	# within x boundaries
                    if (self._get_ball_y_value(self._side_view_ball_location_points[0]) >= net_top + y_distance_offset) and (self._get_ball_y_value(self._side_view_ball_location_points[0]) <= net_bot + y_distance_offset):	# within y boundaries
                        self._side_view_can_bounce = True
        else:
            if self._get_ball_y_value(self._side_view_ball_location_points[0]) > self._get_ball_y_value(self._side_view_ball_location_points[1]) + y_distance_offset:
                self._side_view_ball_current_y_direction = "down"
        
        # Return if the bounce occurred or not.
        return ball_bounce_detected, ball_bounce_coordinates

    # TODO: Once the side view camera has detected the bounce, pull the coordinates of the ball from the primary camera?
    def analyze_side_view_camera(self, loop_video=False):
        ###########################################################################################
        # Side-view camera's job is to only detect the bounce and then immediately read the frame
        # from the primary camera.
        ###########################################################################################
        self._side_view_ball_bounce_was_detected = False

        if self._cameras_are_ready[1]:
            # (return_code, frame) is returned from read() call.
            _, self._side_view_frame = self._side_view_video_stream.read()
            self._side_view_frame_counter += 1

            # If the stream is a video file, choose a different frame to read.
            if self._video_files is not None and self._video_files[1][-4:] in self._supported_video_formats:
                # Restart the video from the beginning if the last frame is already read.
                if loop_video and self._side_view_frame_counter == self._side_view_video_stream.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT):
                    if self._verbose:
                        print("Looping video.")
                    self._side_view_frame_counter = 0
                    self._side_view_video_stream.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0)

            # If the frame is empty, stop the cameras.
            if self._side_view_frame is None:
                if self._verbose:
                    print("No frame!")
                self._cameras_are_ready[1] = False

            # Reset the variable
            bounce_was_detected = None

            # resize the frame, blur it, and convert it to the HSV
            # color space
            self._side_view_frame = imutils.resize(self._side_view_frame, width=self._side_view_video_width)
            blur_frame = cv2.GaussianBlur(self._side_view_frame, (5, 5), 0)
            hsv_frame = cv2.cvtColor(blur_frame, cv2.COLOR_BGR2HSV)
            
            # construct a mask for the defined color, then perform
            # a series of dilations and erosions to remove any small
            # blobs left in the mask
            mask = cv2.inRange(hsv_frame, self._ball_color_hsv_lower_bound, self._ball_color_hsv_higher_bound) # lower, higher
            mask = cv2.dilate(mask, None, iterations=2)
            
            cv2.line(
                self._side_view_frame,
                (self._side_view_middle_table_horizontal_value, self._side_view_location_top_of_net),
                (self._side_view_middle_table_horizontal_value, self._side_view_location_bottom_of_net),
                tuple(reversed(self._colors["red"])),
                1)
            cv2.line(
                self._side_view_frame,
                (self._side_view_location_left_of_table, self._side_view_location_bottom_of_net),
                (self._side_view_location_right_of_table, self._side_view_location_bottom_of_net),
                tuple(reversed(self._colors["red"])),
                1)

            contours_found_in_mask = cv2.findContours(
                mask.copy(),
                cv2.RETR_EXTERNAL,
                cv2.CHAIN_APPROX_SIMPLE)
            contours_found_in_mask = imutils.grab_contours(contours_found_in_mask)
            center = None

            # Draw the circle for the ball on the frame.
            if len(contours_found_in_mask) > 0:
                largest_contour_found = max(contours_found_in_mask, key=cv2.contourArea)
                ((circle_x, circle_y), circle_radius) = cv2.minEnclosingCircle(largest_contour_found)
                countour_moments = cv2.moments(largest_contour_found)
                center = (
                    int(countour_moments["m10"] / countour_moments["m00"]),
                    int(countour_moments["m01"] / countour_moments["m00"]))

                if circle_radius > 5:
                    cv2.circle(
                        self._side_view_frame,
                        (int(circle_x), int(circle_y)),
                        int(circle_radius),
                        tuple(reversed(self._colors["yellow"])),
                        2)
                    self._side_view_ball_location_points.appendleft(center)

            if len(self._side_view_ball_location_points) >= 2 and self._side_view_ball_location_points[0] is not None and self._side_view_ball_location_points[1] is not None:
                bounce_was_detected, _ = self._ball_bounce_side_view(
                    self._side_view_location_left_of_table,
                    self._side_view_location_right_of_table,
                    self._side_view_location_top_of_net,
                    self._side_view_location_bottom_of_net)
            
            # Wait for 5 ms for processing?
            time.sleep(0.005)

            # Show the frame and then save the frame to a file.
            if self._debug:
                cv2.imshow("Side View Feed", self._side_view_frame)
                key = cv2.waitKey(1) & 0xFF

            if bounce_was_detected:
                if self._verbose:
                    print("Side View camera Ball Bounce was detected!")
            else:
                if self._verbose:
                    print("No Side View Ball Bounce was detected on this frame!")

        else:
            if self._verbose:
                print("Side-view camera stream is not ready!")
        
        return self._cameras_are_ready[1], bounce_was_detected
