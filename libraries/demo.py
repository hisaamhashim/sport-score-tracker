"""
    A helper library for the GUI for programmming different
    demos based on selection.
"""
try:
    import coordinate_2d_mapper
except Exception as e:
    import libraries.coordinate_2d_mapper as coordinate_2d_mapper

###########################################
# Global variables shared with the GUI.
###########################################
DEMO_IMAGES_FOLDER = "artifacts/demo/"
DEMO_SELECTED = "demo1"

class gui_demos(object):
    """ Main class which demos can be selected from.
    """
    _callback_counter = 0
    _verbose = True
    _table_location = 0
    _demo_name = "demo1"
    _supported_demos = ["demo1"]
    _game_mapper = None
    _team1_info = None
    _team2_info = None
    _current_server = None
    _sets = 1
    _game_mode = "singles"
    _game_is_finished = False
    _team_that_won = False
    _ball_bounce_proof = False
    _ball_bounce_coordinates = False
    _ball_bounce_metrics = None
    
    def __init__(self, demo_name="demo1", starting_server=2, sets=1, game_mode="singles", team1_info=None, team2_info=None, verbose=True):
        self.mapper_initialized = False

        # Choose the correct demo.
        if demo_name in self._supported_demos:
            self._demo_name = demo_name
        else:
            self._demo_name = "demo1"

        # Control verbosity.
        if verbose:
            self._verbose = True
        else:
            self._verbose = False
        
        # reset the counter.
        self._callback_counter = 0

        # Configure the team scores.
        if team1_info is not None:
            self._team1_info = team1_info
        else:
            self._team1_info = {"name": "Player 1",
                                "sets": 0,
                                "points": 5}
        if team2_info is not None:
            self._team2_info = team2_info
        else:
            self._team2_info = {"name": "Player 2",
                                "sets": 0,
                                "points": 9}
        
        # Configure the sets and server, if passed by the function.
        self._current_server = starting_server
        self._sets = sets
        self._game_mode = game_mode
    
    def get_mapper(self):
        if self.mapper_initialized:
            return self._game_mapper
        else:
            raise ValueError("Demo Coordinated apper is not initialized!")

    def set_mapper(self, game_mapper):
        if self.mapper_initialized:
            self._game_mapper = game_mapper
        else:
            raise ValueError("Demo Coordinated apper is not initialized!")

    def modify_counter(self, increment_or_decrement):
        if self.mapper_initialized:
            if increment_or_decrement:
                self._callback_counter += 1
            else:
                self._callback_counter -= 1

    def playback_demo(self):
        # Only supports playback of Demo1 at this time.
        if self._demo_name == "demo1":
            return self.playback_demo1()
    
    def playback_demo1(self):
        """
            Demo 1: 1 set Singles game,
                    Player 2 has 2 points until winning.
        """
        # When the callback counter is 0 (initialized),
        # start a new game.
        if self._callback_counter == 0:
            # Force a new game.
            self._game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
                starting_server_team=self._current_server,
                num_of_sets=self._sets,
                ruleset_type=self._game_mode,
                team1_info=self._team1_info,
                team2_info=self._team2_info,
                verbose=self._verbose)

            # Assume that the teams are ready to serve but will not start
            # serving until the next call.            
            self.mapper_initialized = True
            self._ball_bounce_coordinates = (100, 100)
            self._ball_bounce_ready = False
            self._ball_bounce_proof = None

        elif self._callback_counter == 1:
            # Player 2 at 9 points is serving and hits valid shot in Q1.
            self._ball_bounce_coordinates = (-50, 50)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_Q1.png"

        elif self._callback_counter == 2:
            # Player 2 at 9 points is rallying and lands a valid shot in Q4
            self._ball_bounce_coordinates = (50, -50)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_Q4.png"
            
        elif self._callback_counter == 3:
            # Player 1 at 5 points is rallying and hits an out at OUT #1.
            # Player 2 should now get a point and is serving again with 10 points.
            self._ball_bounce_coordinates = (0, 110)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_OUT1.png"
            
        elif self._callback_counter == 4:
            # Player 2 at 10 points is serving again with valid shot in Q2.
            self._ball_bounce_coordinates = (50, 50)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_Q2.png"
            
        elif self._callback_counter == 5:
            # Player 2 at 10 points is rallying and hits the net near Q1.
            # Player 1 now as 6 points and is the new server.
            self._ball_bounce_coordinates = (-25, 5)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_OUT1_Net.png"

        elif self._callback_counter == 6:
            # Player 1 at 6 points is serving and lands a valid shot in Q3.
            self._ball_bounce_coordinates = (-50, 50)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_Q3.png"

        elif self._callback_counter == 7:
            # Player 1 at 6 points is rallying and hits an out in OUT1.
            # Player 2 now has 11 points and wins the game.
            self._ball_bounce_coordinates = (0, 110)
            self._ball_bounce_ready = True
            self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])
            self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_OUT1.png"

        else:
            self._game_is_finished, self._team_that_won = self._game_mapper.get_game_state()
            # Check if the game was finished. If the game wasn't finished keep hitting OUT2
            # on every shot until it happens.
            if self._game_is_finished:
                # The callback counter will be set to zero before the next callback.
                self._callback_counter = -1
            else:
                if self._ball_bounce_coordinates[0] == 0 and self._ball_bounce_coordinates[1] == 110:
                    self._ball_bounce_coordinates = (0, -110)
                    self._ball_bounce_ready = True
                    self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_OUT2.png"
                    print("Game was not finished due to undoing score. Force ball bounce at OUT2.")
                else:
                    self._ball_bounce_coordinates = (0, 110)
                    self._ball_bounce_ready = True
                    self._ball_bounce_proof = DEMO_IMAGES_FOLDER + "demo1_OUT1.png"
                    print("Game was not finished due to undoing score. Force ball bounce at OUT1.")
                self._game_mapper.play_game(self._ball_bounce_coordinates[0], self._ball_bounce_coordinates[1])

        # Increment the callback counter.
        self._callback_counter += 1
        print("New demo counter: %s" % self._callback_counter)

        # Get a status of the scores and return back to the calling function in GUI.
        self._team1_scores, self._team2_scores = self._game_mapper.get_scores()
        self._game_is_finished, self._team_that_won = self._game_mapper.get_game_state()
        self._current_server = self._game_mapper.get_server()

        # Create the ball bounce metrics.
        self._ball_bounce_metrics = {"ready": self._ball_bounce_ready,
                                     "x": self._ball_bounce_coordinates[0],
                                     "y": self._ball_bounce_coordinates[1],
                                     "timeout_exceeded": False,
                                     "bounce_proof": self._ball_bounce_proof}

        # (Team1 Score, Team2 Score, Current Server, Proof of Bounce)
        return self._team1_scores, self._team2_scores, self._current_server, self._ball_bounce_metrics, self._game_is_finished