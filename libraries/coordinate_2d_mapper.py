"""
    A simple mapping library which takes a set of 2D coordinates
    retrieved from a capture image slice that gets mapped to a
    coordinate grid which is then used to generate game scores,
    depending on the game being played.

    As of 2022, this mapper is only designed for scoring table
    tennis games based on the olympics rule set. These rules
    are currently hardset but may be written later to be
    customized in a more modular fashion.

    This library is intended to be called by a parent class
    or some other function which handles the game state machine.
    
    However, for unit-test purposes, we can provide a input .txt
    file with preset coordinates in order to verify mapper
    state machine is working correctly.
"""
try:
    import table_tennis_ruleset_and_scoring as game_ruleset_and_scoring
except Exception as e:
    import libraries.table_tennis_ruleset_and_scoring as game_ruleset_and_scoring

import random


def randomly_choose_starting_server_team():
    """ Returns the player number between 1 and 2 using
        a psudeo random generator.
    """
    return random.randint(1,2)


class table_tennis_coordinate_mapper(object):
    """ Keeps track of previous and current coordinates
        and passes it onto the ruleset function to
        determine how points are to be rewarded.
        
        The latest scores can be retrieved from
        this function as well.
    """
    _verbose = True
    _current_coordinate_data = {'x': 0, 'y': 0}
    _max_valid_x_coordinate = 55
    _max_valid_y_coordinate = 100
    _num_of_sets = 3  # Assume a 3 set match.
    _ruleset_type = "singles"  # 'singles' or 'doubles'
    _team1_scores = {"name": "Team 1", "sets": 0, "points": 0}
    _team2_scores = {"name": "Team 2", "sets": 0, "points": 0}
    _game_finished = False
    _team_that_won = 0

    _action = "s"  # On initialization, assume any player is ready to serve.
    _bounce_count = 1  # The current coordinates represents the number of bounces currently made before scoring.
    _location = 0  # An integer value which represents which location the ball was at.
    
    # On initialization, this represents which player is serving (action = serve/let)
    # otherwise, it represents which player is responsible for returning the
    # ball (action = continue).
    _responder = 1
    _server = 1

    # TODO: These variables are reserved exclusively for doubles.
    # For the general prototype, Service, Order of Play, Change of Service and Change of Ends clauses will be ignored.
    # If time, proper doubles will be implemented, presuming reliable player tracking is available on the hardware. 
    _player = 1

    def __init__(self, starting_server_team, num_of_sets=3, ruleset_type="singles",
                 team1_info=None, team2_info=None,
                 x_max=55, y_max=100,
                 verbose=True):
        # team1_info and team2_info are dictionaries whose data
        # structures consist of the following: name, sets and points.
        # This is required in case we need to resume the game from a
        # save state.
        if team1_info is not None:
            self._team1_scores["name"] = team1_info["name"]
            self._team1_scores["sets"] = team1_info["sets"]
            self._team1_scores["points"] = team1_info["points"]
        if team2_info is not None:
            self._team2_scores["name"] = team2_info["name"]
            self._team2_scores["sets"] = team2_info["sets"]
            self._team2_scores["points"] = team2_info["points"]

        if ( ((num_of_sets % 2) == 1) and (num_of_sets > 0) ):
            self._num_of_sets = num_of_sets
        else:
            raise ValueError("Cannot have an even number of sets or sets less than 0!")

        if starting_server_team not in [1, 2]:
            raise ValueError("The team that will serve first in the game must be either: 1 or 2!")
        else:
            self._responder = starting_server_team
            self._server = starting_server_team

        # Singles or Doubles
        if ruleset_type in ["singles", "doubles"]:
            self._ruleset_type = ruleset_type
        else:
            raise ValueError("Only supports ruleset for: singles, doubles!")

        if ((x_max <= 0) or (y_max <= 0)):
            raise ValueError("X max and Y max must be greater than the minimum threshold of 0!")
        else:
            self._max_valid_x_coordinate = x_max
            self._max_valid_y_coordinate = y_max

        # Save a copy of the game state variables from the initialized parameters.
        self._current_snapshot = game_ruleset_and_scoring.table_tennis_ruleset_snapshot()
        self._current_snapshot.save_snapshot(self._team1_scores, self._team2_scores, self._server)

        # Set the verbosity to either true or false.
        self._verbose = verbose

        # Print some information for the user if verbosity is True.
        if self._verbose:
            print("<---------------------------------------------------->")
            print("[2D Mapper-INIT] Starting Server Team: %d, %s[%d-%d] vs %s[%d-%d], Ruleset: %s, Num Sets: %d" % (
                self._responder,
                self._team1_scores["name"], self._team1_scores["sets"], self._team1_scores["points"],
                self._team2_scores["name"], self._team2_scores["sets"], self._team2_scores["points"],
                self._ruleset_type, self._num_of_sets))
            print("<---------------------------------------------------->")

    def __ball_in_Q1(self, coordinate_data):
        """ Returns true if the ball is in Q1(-,+) """
        x = coordinate_data["x"]
        y = coordinate_data["y"]
        x_min = -1 * self._max_valid_x_coordinate
        x_center = 0
        y_max = self._max_valid_y_coordinate
        y_center = 0
        if (((x < x_center) and (x >= x_min)) and ((y > y_center) and (y <= y_max))):
            return True
        else:
            return False

    def __ball_in_Q2(self, coordinate_data):
        """ Returns true if the ball is in Q2(+,+) """
        x = coordinate_data["x"]
        y = coordinate_data["y"]
        x_max = self._max_valid_x_coordinate
        x_center = 0
        y_max = self._max_valid_y_coordinate
        y_center = 0
        if (((x > x_center) and (x <= x_max)) and ((y > y_center) and (y <= y_max))):
            return True
        else:
            return False

    def __ball_in_Q3(self, coordinate_data):
        """ Returns true if the ball is in Q3(-,-) """
        x = coordinate_data["x"]
        y = coordinate_data["y"]
        x_min = -1 * self._max_valid_x_coordinate
        x_center = 0
        y_min = -1 * self._max_valid_y_coordinate
        y_center = 0
        if (((x < x_center) and (x >= x_min)) and ((y < y_center) and (y >= y_min))):
            return True
        else:
            return False

    def __ball_in_Q4(self, coordinate_data):
        """ Returns true if the ball is in Q4(+,-) """
        x = coordinate_data["x"]
        y = coordinate_data["y"]
        x_max = self._max_valid_x_coordinate
        x_center = 0
        y_min = -1 * self._max_valid_y_coordinate
        y_center = 0
        if (((x > x_center) and (x <= x_max)) and ((y < y_center) and (y >= y_min))):
            return True
        else:
            return False

    def __ball_is_out_of_bounds(self, coordinate_data):
        """ Returns true if the ball is in Q4(+,-) """
        x = coordinate_data["x"]
        y = coordinate_data["y"]
        x_max = self._max_valid_x_coordinate
        x_min = -1 * self._max_valid_x_coordinate
        y_max = self._max_valid_y_coordinate
        y_min = -1 * self._max_valid_y_coordinate
        if ((x > x_max) or (x < x_min) or (y > y_max) or (y < y_min)):
            return True
        else:
            return False

    def __ball_hit_net(self, coordinate_data):
        """ Returns true if the ball is at at the y=0 and within the boundary of the table."""
        x = coordinate_data["x"]
        y = coordinate_data["y"]
        x_max = self._max_valid_x_coordinate
        x_min = -1 * self._max_valid_x_coordinate
        y_center = 0
        if ((y == y_center) and ((x >= x_min) or (x <= x_max))):
            return True
        else:
            return False

    def get_coordinate_location(self, x, y):
        # Previously a private function. Made it public so that it can
        # be tested by unit tests. This shouldn't really be used
        # by the GUI.
        coordinate_data = {"x": x, "y": y}
        
        location = 0
        if self.__ball_is_out_of_bounds(coordinate_data):
            location = 0
        elif self.__ball_in_Q1(coordinate_data):
            location = 1
        elif self.__ball_in_Q2(coordinate_data):
            location = 2
        elif self.__ball_in_Q3(coordinate_data):
            location = 3
        elif self.__ball_in_Q4(coordinate_data):
            location = 4
        elif self.__ball_hit_net(coordinate_data):
            location = 5
        else:
            # Currently a placeholder for custom areas
            # on the table. Same as Out of Bounds (OOB)
            location = 0
        return location

    # TODO: Remove this function and instead let play_game() find the location.
    def set_ball_location(self, value):
        if value in [0, 1, 2, 3, 4, 5]:
            self._location = value
            if self._verbose:
                print("[2D Mapper] New Location = %d" % self._location)
        else:
            raise ValueError("The location can only be set to an integer between 0-5!")

    def start_new_game(self, starting_server_team, num_of_sets=3, ruleset_type="singles"):
        self._responder = starting_server_team
        self._server = starting_server_team
        self._team1_scores["sets"] = 0
        self._team1_scores["points"] = 0
        self._team2_scores["sets"] = 0
        self._team2_scores["points"] = 0
        self._current_coordinate_data["x"] = 0
        self._current_coordinate_data["y"] = 0
        self._location = 0
        self._action = "s"  # Restart from new serve.
        self._bounce_count = 1
        self._game_finished = False
        self._team_that_won = 0
        self._num_of_sets = num_of_sets
        self._ruleset_type = ruleset_type

        # Clear the snapshot for a new game.
        self._current_snapshot.save_snapshot(self._team1_scores, self._team2_scores, self._server)

    def play_game(self, x=None, y=None):
        """ Performs the ruleset and scoring update
            State machines for the score tracker.
        """
        if self._ruleset_type == "singles":
            if (not self._game_finished):
                # TODO: Remove the if statement once basic unit-tests are done.
                if ((x is not None) and (y is not None)):
                    if ((self._current_coordinate_data["x"] == x) and (self._current_coordinate_data["y"] == y)):
                        # NOTE: May not be correct in case double bounce around same area has bad accuracy.
                        print("[2D Mapper] Coordinates are the same, nothing new was done!")
                        return False
                    self._current_coordinate_data["x"] = x
                    self._current_coordinate_data["y"] = y
                    self._location = self.get_coordinate_location(x, y)
                    if self._verbose:
                        print("[2D Mapper] Location = %d" % self._location)

                # Obtain the next game action for the next incoming ball bounce coordinates,
                # as well as any scores that were rewarded to a player.
                result, self._action, self._responder, self._bounce_count, team1_reward, team2_reward = game_ruleset_and_scoring.ruleset_singles(
                    self._action, self._responder,
                    self._location, self._bounce_count)
                if self._verbose:
                    print("[2D Mapper-NextState] Result=%s, Action=%s, Responder=%d, Bounce_Count=%d, Point_Team1=%d, Point_Team2=%d" % (
                        result, self._action, self._responder, self._bounce_count, team1_reward, team2_reward))
                
                if result == "new_serve":
                    # Get an update on the scores for the match and check if the game is finished or not.
                    self._server, self._team1_scores, self._team2_scores, self._game_finished, self._team_that_won = game_ruleset_and_scoring.scoring_singles(
                        result, self._server, team1_reward, team2_reward,
                        self._team1_scores, self._team2_scores, self._current_snapshot, self._num_of_sets)
                    self._responder = self._server  # After scoring, start a new serve with correct server.

                    if self._verbose:
                        print("[2D Mapper-Scores] Server=%d, Team1=[%d-%d], Team2=[%d-%d], Game_Finished=%s, Team_Won=%s" % (
                            self._server, self._team1_scores["sets"], self._team1_scores["points"],
                            self._team2_scores["sets"], self._team2_scores["points"],
                            self._game_finished, self._team_that_won))
                        print("<----------------->")
                return True
            else:
                return False

        elif self._ruleset_type == "doubles":
            if (not self._game_finished):
                # TODO: Remove the if statement once basic unit-tests are done.
                if ((x is not None) and (y is not None)):
                    if ((self._current_coordinate_data["x"] == x) and (self._current_coordinate_data["y"] == y)):
                        # NOTE: May not be correct in case double bounce around same area has bad accuracy.
                        print("[2D Mapper] Coordinates are the same, nothing new was done!")
                        return False
                    self._current_coordinate_data["x"] = x
                    self._current_coordinate_data["y"] = y
                    self._location = self.get_coordinate_location(x, y)
                    if self._verbose:
                        print("[2D Mapper] Location = %d", self._location)

                # Obtain the next game action for the next incoming ball bounce coordinates,
                # as well as any scores that were rewarded to a player.
                result, self._action, self._responder, self._bounce_count, team1_reward, team2_reward = game_ruleset_and_scoring.ruleset_doubles(
                    self._action, self._responder,
                    self._location, self._bounce_count)
                if self._verbose:
                    print("[2D Mapper-NextState] Result=%s, Action=%s, Responder=%d, Bounce_Count=%d, Point_Team1=%d, Point_Team2=%d" % (
                        result, self._action, self._responder, self._bounce_count, team1_reward, team2_reward))
                
                if result == "new_serve":
                    # Get an update on the scores for the match and check if the game is finished or not.
                    self._server, self._team1_scores, self._team2_scores, self._game_finished, self._team_that_won = game_ruleset_and_scoring.scoring_doubles(
                        result, self._server, team1_reward, team2_reward,
                        self._team1_scores, self._team2_scores, self._current_snapshot, self._num_of_sets)
                    self._responder = self._server  # After scoring, start a new serve with correct server.

                    if self._verbose:
                        print("[2D Mapper-Scores] Server=%d, Team1=[%d-%d], Team2=[%d-%d], Game_Finished=%s, Team_Won=%s" % (
                            self._server, self._team1_scores["sets"], self._team1_scores["points"],
                            self._team2_scores["sets"], self._team2_scores["points"],
                            self._game_finished, self._team_that_won))
                        print("<----------------->")
                return True
            else:
                return False
        else:
            raise ValueError("Not implemented!")

    def modify_score(self, reward_team1=False, reward_team2=False):
        # Restore the snapshot from the previous serve, effectively undoing the last point that was rewarded.
        # Only when either no team should get rewarded or only one of the teams gets rewarded, should the
        # snapshot get restored.
        if ((not reward_team1) or (not reward_team2)):
            previous_snapshot = self._current_snapshot.restore_snapshot()

            self._team1_scores = previous_snapshot[0]
            self._team2_scores = previous_snapshot[1]
            self._action = previous_snapshot[2]  # action will ALWAYS be 's' or server
            self._responder = previous_snapshot[3]
            self._server = previous_snapshot[4]
            self._bounce_count = previous_snapshot[5]
            self._game_finished = previous_snapshot[6]
            self._team_that_won = previous_snapshot[7]

            # Calling this function when a game was already won, automatically changes the game state from
            # finished to not finished.
            self._game_finished = False
            self._team_that_won = 0

            # We will only be simulating out of bounds scenario here, so this should be
            # compatible with both singles and doubles rulesets.
            if (reward_team1 and not reward_team2):  # Change the last point to reward Team 1.
                if (self._server == 1):  # Team 1 was the last server.
                    # Simulate a scenario where Team 2 is supposed to return a ball on their side during a rally.
                    self._responder = 2
                    self._action = "r"
                    self.set_ball_location(0)  # Team 2 hit out of bounds, so Team 1 gets the point.
                    self.play_game()
                else:  # Team 2 was the last server.
                    self._responder = 2
                    self.set_ball_location(0) # Team 2 hit their serve out of bounds, so Team 1 gets the point.
                    self.play_game()
            elif (reward_team2 and not reward_team1):  # Change the last point to reward Team 2.
                if (self._server == 2):  # Team 2 was the last server.
                    # Simulate a scenario where Team 1 is supposed to return a ball on their side during a rally.
                    self._responder = 1
                    self._action = "r"
                    self.set_ball_location(0)  # Team 1 hit out of bounds, so Team 2 gets the point.
                    self.play_game()
                else:  # Team 1 was the last server.
                    self._responder = 1
                    self.set_ball_location(0) # Team 1 hit their serve out of bounds, so Team 2 gets the point.
                    self.play_game()

    def get_scores(self):
        return self._team1_scores, self._team2_scores

    def get_game_state(self):
        return self._game_finished, self._team_that_won

    #####################################
    # GUI ONLY FUNCTIONS
    #####################################
    def get_server(self):
        return self._server

    def get_action(self):
        if self._action == "r":
            return "rally"
        elif self._action == "s":
            return "server"
        else:
            # Should never get here.
            raise ValueError("Action is not initialized!")
    