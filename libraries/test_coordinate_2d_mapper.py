"""
    Unit tests for coordinate_2d_mapper.py
"""
try:
    import coordinate_2d_mapper
except Exception as e:
    import libraries.coordinate_2d_mapper as coordinate_2d_mapper

import unittest
#import yaml

class Test2DMapperFunctionality(unittest.TestCase):
    def test__coordinate_2d_mapper_functionality_01_initialization(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(starting_server_team=starting_team, num_of_sets=1, verbose=False)

        # Verify that the game is initialized.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 earned no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 earned no points.")

        # Cleanup.
        game_mapper.start_new_game(starting_team, num_of_sets=1)
    
    def test__coordinate_2d_mapper_functionality_02_coordinate_mapping__Q0_OOB(self):
        starting_team = 1
        x_max=55
        y_max=100
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1,
            x_max=x_max, y_max=y_max, verbose=False)
        
        # Out of Bounds (OOB) are any points that are out of range of x_max or y_max.
        location = game_mapper.get_coordinate_location(-56, 100)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(-56, 0)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(-56, -100)
        self.assertEqual(location, 0, "Should be 0 (OOB).")

        location = game_mapper.get_coordinate_location(56, 100)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(56, 0)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(56, -100)
        self.assertEqual(location, 0, "Should be 0 (OOB).")

        location = game_mapper.get_coordinate_location(-55, 101)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(0, 101)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(-55, 101)
        self.assertEqual(location, 0, "Should be 0 (OOB).")

        location = game_mapper.get_coordinate_location(-55, -101)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(0, -101)
        self.assertEqual(location, 0, "Should be 0 (OOB).")
        location = game_mapper.get_coordinate_location(-55, -101)
        self.assertEqual(location, 0, "Should be 0 (OOB).")

    def test__coordinate_2d_mapper_functionality_02_coordinate_mapping__Q1(self):
        starting_team = 1
        x_max=55
        y_max=100
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1,
            x_max=x_max, y_max=y_max, verbose=False)
        
        # Q1 quadrant consists of -x_max and +y_max
        location = game_mapper.get_coordinate_location(-55, 100)  # top-left
        self.assertEqual(location, 1, "Should be 1 (Q1).")
        location = game_mapper.get_coordinate_location(-1, 100)  # top-right
        self.assertEqual(location, 1, "Should be 1 (Q1).")
        location = game_mapper.get_coordinate_location(-25, 50)  # center
        self.assertEqual(location, 1, "Should be 1 (Q1).")
        location = game_mapper.get_coordinate_location(-55, 1)  # bottom-left
        self.assertEqual(location, 1, "Should be 1 (Q1).")
        location = game_mapper.get_coordinate_location(-1, 1)  # bottom-right
        self.assertEqual(location, 1, "Should be 1 (Q1).")

    def test__coordinate_2d_mapper_functionality_02_coordinate_mapping__Q2(self):
        starting_team = 1
        x_max=55
        y_max=100
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1,
            x_max=x_max, y_max=y_max, verbose=False)
        
        # Q2 quadrant consists of +x_max and +y_max
        location = game_mapper.get_coordinate_location(1, 100)  # top-left
        self.assertEqual(location, 2, "Should be 2 (Q2).")
        location = game_mapper.get_coordinate_location(55, 100)  # top-right
        self.assertEqual(location, 2, "Should be 2 (Q2).")
        location = game_mapper.get_coordinate_location(25, 50)  # center
        self.assertEqual(location, 2, "Should be 2 (Q2).")
        location = game_mapper.get_coordinate_location(1, 1)  # bottom-left
        self.assertEqual(location, 2, "Should be 2 (Q2).")
        location = game_mapper.get_coordinate_location(55, 1)  # bottom-right
        self.assertEqual(location, 2, "Should be 2 (Q2).")

    def test__coordinate_2d_mapper_functionality_02_coordinate_mapping__Q3(self):
        starting_team = 1
        x_max=55
        y_max=100
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1,
            x_max=x_max, y_max=y_max, verbose=False)
        
        # Q3 quadrant consists of -x_max and -y_max
        location = game_mapper.get_coordinate_location(-55, -1)  # top-left
        self.assertEqual(location, 3, "Should be 3 (Q3).")
        location = game_mapper.get_coordinate_location(-1, -1)  # top-right
        self.assertEqual(location, 3, "Should be 3 (Q3).")
        location = game_mapper.get_coordinate_location(-25, -50)  # center
        self.assertEqual(location, 3, "Should be 3 (Q3).")
        location = game_mapper.get_coordinate_location(-55, -100)  # bottom-left
        self.assertEqual(location, 3, "Should be 3 (Q3).")
        location = game_mapper.get_coordinate_location(-1, -100)  # bottom-right
        self.assertEqual(location, 3, "Should be 3 (Q3).")

    def test__coordinate_2d_mapper_functionality_02_coordinate_mapping__Q4(self):
        starting_team = 1
        x_max=55
        y_max=100
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1,
            x_max=x_max, y_max=y_max, verbose=False)
        
        # Q4 quadrant consists of +x_max and -y_max
        location = game_mapper.get_coordinate_location(1, -1)  # top-left
        self.assertEqual(location, 4, "Should be 4 (Q4).")
        location = game_mapper.get_coordinate_location(55, -1)  # top-right
        self.assertEqual(location, 4, "Should be 4 (Q4).")
        location = game_mapper.get_coordinate_location(25, -50)  # center
        self.assertEqual(location, 4, "Should be 4 (Q4).")
        location = game_mapper.get_coordinate_location(1, -100)  # bottom-left
        self.assertEqual(location, 4, "Should be 4 (Q4).")
        location = game_mapper.get_coordinate_location(55, -100)  # bottom-right
        self.assertEqual(location, 4, "Should be 4 (Q4).")

    def test__coordinate_2d_mapper_functionality_02_coordinate_mapping__Q5_net(self):
        starting_team = 1
        x_max=55
        y_max=100
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1,
            x_max=x_max, y_max=y_max, verbose=False)
        
        # Q4 quadrant consists of +x_max and -y_max
        location = game_mapper.get_coordinate_location(-55, 0)  # farthest-left
        self.assertEqual(location, 5, "Should be 5 (Q5).")
        location = game_mapper.get_coordinate_location(0, 0)  # center
        self.assertEqual(location, 5, "Should be 5 (Q5).")
        location = game_mapper.get_coordinate_location(55, 0)  # farthest right
        self.assertEqual(location, 5, "Should be 5 (Q5).")

    def test__coordinate_2d_mapper_functionality_03_basic_rules__serving_with_coordinates(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 1 successfully serves from Q1 -> Q3
        game_mapper.play_game(-30, 50)
        game_mapper.play_game(-45, -85)

        # Team 2 misses the ball because they hit OOB just above Q4,
        # meaning that Team 1 wins the point.
        game_mapper.play_game(50, 110)

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Team 1 successfully serves from Q2 -> Q4
        game_mapper.play_game(30, 35)
        game_mapper.play_game(5, -70)

        # Team 2 returns the ball back to Team 1 to Q1
        game_mapper.play_game(35, 90)

        # Team 1 double bounces to Q2, making Team 2 to win the point.
        game_mapper.play_game(-3, 10)

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_04_basic_rules__serving_with_location(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 1 successfully serves from Q1 -> Q3
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()

        # Team 2 misses the ball because they hit OOB just above Q4,
        # meaning that Team 1 wins the point.
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Team 1 successfully serves from Q2 -> Q4
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 returns the ball back to Team 1 to Q1
        game_mapper.set_ball_location(1)
        game_mapper.play_game()

        # Team 1 double bounces to Q2, making Team 2 to win the point.
        game_mapper.set_ball_location(2)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_04_basic_rules__serving_double_bounce(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 1 is the server and double bounces on their side. Team 2 gets the point.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # Restart the game but with Team 2 serving.
        game_mapper.start_new_game(2, num_of_sets=1)

        # Team 2 is the server and double bounces on their side. Team 1 gets the point.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_04_basic_rules__rallies_ten_successful(self):
        starting_team = 2
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 2 launches a successful serve from Q3 -> Q1
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()

        # Team 1 succesfully rallies the shot back to Team 2.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 and Team 1 successfully rally back and forth 10 times.
        ball_locations = [2, 4]
        for i in range(10):
            # Every even shot is hit by Team 2.
            if ((i % 2) == 0):
                game_mapper.set_ball_location(ball_locations[0])
            else:
                game_mapper.set_ball_location(ball_locations[1])
            game_mapper.play_game()

        # The last shot is in Team 2 hands, and they hit out, therefore Team 1 wins the point
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_04_basic_rules__let_three_repeat_serves(self):
        starting_team = 2
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 2 hits the ball at the net three times in a row.
        # The first shot is a valid first bounce, but second bounce is the net.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(5)
        game_mapper.play_game()

        # The next two shots are all at the net, requiring redo of serve.
        game_mapper.set_ball_location(5)
        game_mapper.play_game()
        game_mapper.set_ball_location(5)
        game_mapper.play_game()

        # Team 2 now hits OOB awarding Team 1 the point.
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_04_basic_rules__serving_no_tiebreaker_two_serves_per_team(self):
        starting_team = 1
        reloaded_game_team1_scores = {"name": "Team 1", "sets": 0, "points": 7}
        reloaded_game_team2_scores = {"name": "Team 2", "sets": 0, "points": 7}
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles", 
            team1_info=reloaded_game_team1_scores,
            team2_info=reloaded_game_team2_scores,
            verbose=False)

        # Team 1 launches first serve to Team 2. Team 2 hits OOB.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 8, "Team 1 has 8 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 7, "Team 2 has 7 points.")

        # Team 1 launches second serve to Team 2. Team 2 hits OOB.
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 9, "Team 1 has 9 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 7, "Team 2 has 7 points.")

        # Team 1 served twice, and neither player has reached a tiebreaker.
        # Therefore Team 2 serves.
        # Team 2 launches first serve to Team 1. Team 1 hits OOB.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 9, "Team 1 has 9 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 8, "Team 2 has 8 points.")

        # Team 2 launches second serve to Team 1. Team 1 hits OOB.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 9, "Team 1 has 9 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 9, "Team 2 has 9 points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_04_basic_rules__serving_tiebreaker_one_serve_per_team(self):
        starting_team = 1
        reloaded_game_team1_scores = {"name": "Team 1", "sets": 0, "points": 10}
        reloaded_game_team2_scores = {"name": "Team 2", "sets": 0, "points": 10}
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles", 
            team1_info=reloaded_game_team1_scores,
            team2_info=reloaded_game_team2_scores,
            verbose=False)

        # Team 1 launches first serve to Team 2. Team 2 hits OOB.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 11, "Team 1 has 11 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 10, "Team 2 has 10 points.")

        # Because this is a tiebreaker, Team 2 is now the server.
        # Team 2 successfully serves to Team 1 and Team 1 gets OOB.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 11, "Team 1 has 11 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 11, "Team 2 has 11 points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_05_start_new_game__during_play(self):
        starting_team = 2
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 2 misses their serve and double bounces. Team 1 gets the point.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Team 2 serves their 2nd serve to Team 1 and Team 1 misses the shot.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game(0)

        # Verify that Team 2 gets the next point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # Start a new game.
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Verify that the points were all reset.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

    def test__coordinate_2d_mapper_functionality_06_game_finished__saved_game(self):
        starting_team = 1
        reloaded_game_team1_scores = {"name": "Team 1", "sets": 0, "points": 10}
        reloaded_game_team2_scores = {"name": "Team 2", "sets": 0, "points": 9}
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles", 
            team1_info=reloaded_game_team1_scores,
            team2_info=reloaded_game_team2_scores,
            verbose=False)

        # Make sure Team 1 serves a successful serve
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 hits out, making Team 1 get a set and win the game.
        game_mapper.set_ball_location(0)
        game_mapper.play_game(0)

        # Verify that Team 1 won the one set game.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, True, "The game was finished.")
        self.assertEqual(team_that_won, 1, "Team 1 should have won.")
        self.assertEqual(team1_scores["sets"], 1, "Team 1 should have 1 set.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Cleanup.
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_functionality_07_transmitter_score_change__last_serve_current_set(self):
        starting_team = 1
        reloaded_game_team1_scores = {"name": "Team 1", "sets": 1, "points": 8}
        reloaded_game_team2_scores = {"name": "Team 2", "sets": 1, "points": 7}
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=3, ruleset_type="singles", 
            team1_info=reloaded_game_team1_scores,
            team2_info=reloaded_game_team2_scores,
            verbose=False)

        # Make sure Team 1 serves a successful serve, or so we think.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 intentionally misses the ball, giving Team 1 the point
        # only because Team 1 actually hit the second bounce off the table
        # and the mapper incorrectly mapped it as Q4 instead of OOB.
        game_mapper.set_ball_location(0)
        game_mapper.play_game(0)

        # Verify that Team 1 has gotten the point, with a 2 point lead to Team 2.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 1, "Team 1 should have 1 set.")
        self.assertEqual(team1_scores["points"], 9, "Team 1 should have 9 points.")
        self.assertEqual(team2_scores["sets"], 1, "Team 2 should have 1 set.")
        self.assertEqual(team2_scores["points"], 7, "Team 2 should have 7 points.")

        # Now assume that Team 2 uses the remote to reset the points back such
        # that Team 1 has to redo the serve.
        game_mapper.modify_score(False, False)

        # Verify that Team 1's point was restored to that which was before.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 1, "Team 1 should have 1 set.")
        self.assertEqual(team1_scores["points"], 8, "Team 1 should have 8 points")
        self.assertEqual(team2_scores["sets"], 1, "Team 2 should have 1 set.")
        self.assertEqual(team2_scores["points"], 7, "Team 2 should have 7 points.")

        # Cleanup.
        game_mapper.start_new_game(starting_team, num_of_sets=3)

    def test__coordinate_2d_mapper_functionality_08_transmitter_score_change__last_serve_previous_set(self):
        starting_team = 1
        reloaded_game_team1_scores = {"name": "Team 1", "sets": 0, "points": 10}
        reloaded_game_team2_scores = {"name": "Team 2", "sets": 1, "points": 6}
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=3, ruleset_type="singles", 
            team1_info=reloaded_game_team1_scores,
            team2_info=reloaded_game_team2_scores,
            verbose=False)

        # Make sure Team 1 serves a successful serve, or so we think.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 intentionally misses the ball, giving Team 1 the point
        # only because Team 1 actually hit the second bounce off the table
        # and the mapper incorrectly mapped it as Q4 instead of OOB.
        game_mapper.set_ball_location(0)
        game_mapper.play_game(0)

        # Verify that Team 1 has gotten the point, with a 2 point lead to Team 2.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 1, "Team 1 should have 1 set.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 should have 0 points.")
        self.assertEqual(team2_scores["sets"], 1, "Team 2 should have 1 set.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 should have 0 points.")

        # Now assume that Team 2 uses the remote to reset the points back such
        # that Team 1 has to redo the serve.
        game_mapper.modify_score(False, False)

        # Verify that Team 1's point was restored to that which was before.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have 0 sets.")
        self.assertEqual(team1_scores["points"], 10, "Team 1 should have 10 points")
        self.assertEqual(team2_scores["sets"], 1, "Team 2 should have 1 set.")
        self.assertEqual(team2_scores["points"], 6, "Team 2 should have 6 points.")

        # Cleanup.
        game_mapper.start_new_game(starting_team, num_of_sets=3)

    def test__coordinate_2d_mapper_functionality_09_transmitter_score_change__continue_finished_game(self):
        starting_team = 1
        reloaded_game_team1_scores = {"name": "Team 1", "sets": 0, "points": 10}
        reloaded_game_team2_scores = {"name": "Team 2", "sets": 0, "points": 6}
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles", 
            team1_info=reloaded_game_team1_scores,
            team2_info=reloaded_game_team2_scores,
            verbose=False)

        # Make sure Team 1 serves a successful serve, or so we think.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 intentionally misses the ball, giving Team 1 the point
        # only because Team 1 actually hit the second bounce off the table
        # and the mapper incorrectly mapped it as Q4 instead of OOB.
        game_mapper.set_ball_location(0)
        game_mapper.play_game(0)

        # Verify that Team 1 won the game.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, True, "The game finished")
        self.assertEqual(team_that_won, 1, "Team 1 wins the game")
        self.assertEqual(team1_scores["sets"], 1, "Team 1 should have 1 set.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 should have 0 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 should have 0 points.")

        # Now assume that Team 2 uses the remote to reset the points back such
        # that Team 1 has to redo the serve.
        game_mapper.modify_score(False, False)

        # Verify that the game is restored.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have 1 set.")
        self.assertEqual(team1_scores["points"], 10, "Team 1 should have 10 points")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 6, "Team 2 should have 6 points.")

        # Cleanup.
        game_mapper.start_new_game(starting_team, num_of_sets=3)

    def test__coordinate_2d_mapper_functionality_09_transmitter_score_change__give_opposing_team_the_point(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles", 
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Make sure Team 1 serves a successful serve.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 rallies the ball back successfully to Team.
        game_mapper.set_ball_location(2)
        game_mapper.play_game(0)

        # Team 1 rallies the ball back to to Team 1.
        game_mapper.set_ball_location(3)
        game_mapper.play_game(0)

        # Team 2 doesn't hit the ball intentionally and the ball goes OOB. Team 1 gets the point.
        game_mapper.set_ball_location(0)
        game_mapper.play_game(0)

        # Verify Team 1 got the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is not finished yet.")
        self.assertEqual(team_that_won, 0, "No team wins the game")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Now assume that Team 2 uses the remote to not only contest the point,
        # But judges agree that Team 1 actually hit OOB, so Team 2 deserves
        # the point instead.
        game_mapper.modify_score(False, True)

        # Verify that Team 2 got the point instead.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is not finished yet.")
        self.assertEqual(team_that_won, 0, "No team wins the game")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        ################## Restart the game, Team 2 is now the server.
        game_mapper.start_new_game(2, num_of_sets=1)

        # Assume Team 2 apparently makes a fast serve and Team 1 couldn't hit the ball.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 got the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is not finished yet.")
        self.assertEqual(team_that_won, 0, "No team wins the game")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # The judges see that Team 2 violated a rule in table tennis where you must show the ball
        # completely through the serve, but Team 2 hit the ball partially during the serve.
        # Due to this, Team 2's serve was illegal and Team 1 must be rewarded the point instead.
        game_mapper.modify_score(True, False)

        # Verify Team 1 got the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is not finished yet.")
        self.assertEqual(team_that_won, 0, "No team wins the game")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Cleanup.
        game_mapper.start_new_game(starting_team, num_of_sets=1)


class Test2DMapperSingles(unittest.TestCase):
    def test__coordinate_2d_mapper_singles_01_gameplay__serving_rules(self):
        # In Singles, the team that is serving can serve any 4 quadrants (Q1-Q4) from their side,
        # to any 4 quadrants on the opponent's side.
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="singles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 1 serves their serve from Q1 -> Q3, and Team 2 hits OOB.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Team 1 serves their serve from Q1 -> Q4, and Team 2 hits OOB.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 2, "Team 1 has 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        ######################## Restart the game.
        game_mapper.start_new_game(starting_team, num_of_sets=1)

        # Team 1 serves their serve from Q2 -> Q3, and Team 2 hits OOB.
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Team 1 serves their serve from Q2 -> Q4, and Team 2 hits OOB.
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 2, "Team 1 has 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        ######################## Restart the game, Team 2 is the server now.
        game_mapper.start_new_game(2, num_of_sets=1)

        # Team 2 serves their serve from Q3 -> Q1, and Team 1 hits OOB.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # Team 2 serves their serve from Q3 -> Q2, and Team 1 hits OOB.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 2, "Team 2 has 2 points.")

        ######################## Restart the game, Team 2 is still the server.
        game_mapper.start_new_game(2, num_of_sets=1)

        # Team 2 serves their serve from Q4 -> Q1, and Team 1 hits OOB.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # Team 2 serves their serve from Q4 -> Q2, and Team 1 hits OOB.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 2, "Team 2 has 2 points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1)

    def test__coordinate_2d_mapper_singles_02_gameplay__one_set_match_no_tiebreaker(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(starting_server_team=starting_team, num_of_sets=1, verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)
        
        # Verify that the game is initialized.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 earned no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 earned no points.")

        # Team 1 (Serve #1) Serves the ball correctly, Team 2 receiver.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
    
        # Team 2 rally's the ball back successfully, Team 1 receiver.
        game_mapper.set_ball_location(2)
        game_mapper.play_game()

        # Team 1 rally's the ball back successfully, Team 2 receiver.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()

        # Team 2 misses the ball as ball goes out of bounds. Team 1 wins point (1-0)
        game_mapper.set_ball_location(0)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 earnt 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 earned no points.")

        # Team 1 serves again (Serve #2) and scores an out. Team 2 wins a point (1-1)
        game_mapper.set_ball_location(0)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 earnt 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 earned no points.")

        # Server is now Team 2 (Serve #1), and serves correctly, Team 1 receiver.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()

        # Team 1 returns the ball correctly, and Team 2 double bounces. Team 1 wins a point (2-1)
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 2, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 earned no points.")

        # Team 2 (Serve #2) is double bounces on second bounce. Team 1 wins a point (3-1)
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 3, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 earned no points.")
    
        # Team 1 (Serve #1) makes a successful serve, but Team 2 hits their side.
        # Team 1 wins a point. (4-1)
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)  # Q4 -> Q3 by Team 2 is illegal.
        game_mapper.play_game()

        # Team 1 (Serve #2) makes a successful serve,
        # Team 2 hits back successfully, Team 1 hits back successfully,
        # Team 2 hits out of bounds.
        # Team 1 wins a point. (5-1)
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Team 2 (Serve #1) makes a successful serve,
        # Team 1 hits out of bounds. Team 2 wins a point (5-2)
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()   
        
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 5, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 2, "Team 2 earned no points.")

        # Team 2 (Serve #2) makes a successful serve,
        # Team 1 hits out of bounds. Team 2 wins a point (5-3)
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()   
        
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 5, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 3, "Team 2 earned no points.")

        # Team 1 gets bored and decides to go super saiyan, Team 1 finishes
        # the game with a landslide victory.
        game_mapper.set_ball_location(1)  # Team 1 (Server #1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)  # (6-3)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 6, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 3, "Team 2 earned no points.")
        game_mapper.set_ball_location(2)  # Team 1 (Server #2)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)  # (7-3)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 7, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 3, "Team 2 earned no points.")
        game_mapper.set_ball_location(0)  # Team 2 (Server #1)
        game_mapper.play_game()  # (8-3)
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 8, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 3, "Team 2 earned no points.")
        game_mapper.set_ball_location(4)  # Team 2 (Server #2)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)  # (9-3)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 9, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 3, "Team 2 earned no points.")
        game_mapper.set_ball_location(2)  # Team 1 (Server #1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)  # (10-3)
        game_mapper.play_game()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 10, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 3, "Team 2 earned no points.")
        game_mapper.set_ball_location(1)  # Team 1 (Server #2)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)  # (11-3)
        game_mapper.play_game()

        # Game won by Team 1.
        team1_scores, team2_scores = game_mapper.get_scores()
        game_is_finished, team_that_won = game_mapper.get_game_state()
        self.assertEqual(team1_scores["sets"], 1, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 earnt 2 points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 earned no points.")
        self.assertEqual(game_is_finished, True, "The Game should have finished!")
        self.assertEqual(team_that_won, 1, "Team 1 should have won!!")

    def test__coordinate_2d_mapper_singles_03_gameplay__one_set_match_tiebreaker_win(self):
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(starting_server_team=starting_team, num_of_sets=1, verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1)
        
        # Verify that the actual game started from scratch.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game is still in play.")
        self.assertEqual(team_that_won, 0, "No team has won yet.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should no sets won.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 earned no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 earned no points.")

        # Obtain a Tie Score of 10:10
        for _ in range(21):
            game_mapper.set_ball_location(0)
            game_mapper.play_game()
        
        # Team 2 wins the next 2 points.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()
        game_is_finished, team_that_won = game_mapper.get_game_state()
        self.assertEqual(game_is_finished, True, "The Game should have finished!")
        self.assertEqual(team_that_won, 2, "Team 2 should have won!!")


class Test2DMapperDoubles(unittest.TestCase):
    def test__coordinate_2d_mapper_doubles_01_gameplay__serving_rules(self):
        # In Doubles, the team that is serving must serve from the right-side to the
        # opponents right-side. So Team 1 must serve from Q1 to Q4, while Team 2
        # must serve from Q4 to Q1. All other serves are a loss of points.
        starting_team = 1
        game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=starting_team, num_of_sets=1, ruleset_type="doubles",
            verbose=False)
        game_mapper.start_new_game(starting_team, num_of_sets=1, ruleset_type="doubles")

        # VALID SERVE: Team 1 serves from Q1->Q4. Team 2 returns OOB.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # ILLEGAL SERVE: Team 1 serves from Q1->Q3. Team 2 gets the point.
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(3)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        ######################## Restart the game.
        game_mapper.start_new_game(starting_team, num_of_sets=1, ruleset_type="doubles")

        # ILLEGAL SERVE: Team 1 serves the first bounce from Q2. Team 2 gets the point.
        game_mapper.set_ball_location(2)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        ######################## Restart the game, Team 2 is now the server.
        game_mapper.start_new_game(2, num_of_sets=1, ruleset_type="doubles")

        # VALID SERVE: Team 2 serves from Q4->Q1. Team 1 returns OOB.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(1)
        game_mapper.play_game()
        game_mapper.set_ball_location(0)
        game_mapper.play_game()

        # Verify that Team 2 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 0, "Team 1 has no points.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        # ILLEGAL SERVE: Team 2 serves from Q4->Q2. Team 1 gets the point.
        game_mapper.set_ball_location(4)
        game_mapper.play_game()
        game_mapper.set_ball_location(2)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 1, "Team 2 has 1 point.")

        ######################## Restart the game, Team 2 is again serving.
        game_mapper.start_new_game(2, num_of_sets=1, ruleset_type="doubles")

        # ILLEGAL SERVE: Team 2 serves the first bounce from Q3. Team 1 gets the point.
        game_mapper.set_ball_location(3)
        game_mapper.play_game()

        # Verify that Team 1 gets the point.
        game_is_finished, team_that_won = game_mapper.get_game_state()
        team1_scores, team2_scores = game_mapper.get_scores()
        self.assertEqual(game_is_finished, False, "The game was not finished.")
        self.assertEqual(team_that_won, 0, "No team has won.")
        self.assertEqual(team1_scores["sets"], 0, "Team 1 should have no sets.")
        self.assertEqual(team1_scores["points"], 1, "Team 1 has 1 point.")
        self.assertEqual(team2_scores["sets"], 0, "Team 2 should have no sets won.")
        self.assertEqual(team2_scores["points"], 0, "Team 2 has no points.")

        # Cleanup
        game_mapper.start_new_game(starting_team, num_of_sets=1, ruleset_type="doubles")


if __name__ == '__main__':
    unittest.main()