"""
	This script performs an update by running a series of commands in shell.
"""
import os
import subprocess
import yaml
from yaml.loader import SafeLoader

#################################################
#   Config YAML file
#################################################
YAML_FILE_DIRECTORY="common"
config_yaml_file = "%s/gui_config.yaml" % YAML_FILE_DIRECTORY
GUI_CONFIG_PARAMS = []
with open(config_yaml_file) as f:
    GUI_CONFIG_PARAMS = yaml.load(f, Loader=SafeLoader)

#################################################
#   Temporary directory
#################################################
ILLEGAL_DIRECTORIES = [
	"", "/", "/usr/bin/local", "/usr/bin", "/bin", "/dev", "/etc", "/export", "/home", "/mnt",
	"/media", "/net", "/opt", "/proc", "/root", "/run", "/shared", "/srv", "/sys", "/tmp",
	"/tmpfs", "/usr", "/var"]
HOME_DIRECTORY = os.path.expanduser("~")
UPDATER_TEMPORARY_DIRECTORY = "%s/Downloads/score_tracker_latest" % HOME_DIRECTORY
SCORE_TRACKER_ROOT_DIRECTORY="sport-score-tracker"
UPDATER_SCRIPT_DIRECTORY = "/".join(os.path.realpath(__file__).split("/")[:-1])

##################################################
# Platform finder
##################################################
PLATFORM_TYPE = "pc"
try:
    import RPi.GPIO as GPIO
    PLATFORM_TYPE = "pi"
    print("Raspberry Pi is running the updater.")
except Exception as e:
    PLATFORM_TYPE = "pc"
    print("Local Machine is the updater.")
LAUNCHER_DIRECTORY = None


def create_temporary_directory():
	if os.path.exists(UPDATER_TEMPORARY_DIRECTORY):
		if UPDATER_TEMPORARY_DIRECTORY in ILLEGAL_DIRECTORIES:
			raise ValueError("Cannot use illegal directories!")
		else:
			# Remove the old directory.
			os.system("sudo rm -r %s" % UPDATER_TEMPORARY_DIRECTORY)
			#os.system("rm -r %s" % UPDATER_TEMPORARY_DIRECTORY)
			print("Directory: %s was removed." % UPDATER_TEMPORARY_DIRECTORY)

	# Create the new directory.
	os.system("mkdir %s" % UPDATER_TEMPORARY_DIRECTORY)
	print("Directory: %s was created." % UPDATER_TEMPORARY_DIRECTORY)
		
def pull_from_git():
	if "git" in GUI_CONFIG_PARAMS["about"]:
		create_temporary_directory()

		# If a branch is specified, pull from the branch,
		if "git_branch" in GUI_CONFIG_PARAMS["about"]:
			print("Pulling from branch: %s" % GUI_CONFIG_PARAMS["about"]["git_branch"])
			command = "git clone -b %s %s" % (
				GUI_CONFIG_PARAMS["about"]["git_branch"],
				GUI_CONFIG_PARAMS["about"]["git"])
		else:
			# Clone directly from HEAD.
			print("Pulling from head: %s" % GUI_CONFIG_PARAMS["about"]["git"])
			command = "git clone %s" % (GUI_CONFIG_PARAMS["about"]["git"])
		# Call in the same shell.
		subprocess.call(command, cwd=UPDATER_TEMPORARY_DIRECTORY, shell=True)
	else:
		raise ValueError("No Git provided. Cannot run updater!")

def close_existing_app():
	# Assume that the application is running for the current directory.
	print("Closing any existing apps...")
	os.system("%s/stop_score_tracker.sh" % UPDATER_SCRIPT_DIRECTORY)

def modify_yaml_for_platform():
	root_yaml_file_directory = "%s/%s/%s" % (
		UPDATER_TEMPORARY_DIRECTORY,
		SCORE_TRACKER_ROOT_DIRECTORY,
		YAML_FILE_DIRECTORY)

	# If the platform is Raspberry Pi, change the YAML file to run in full screen.
	# NOTE: We assume that both gui_config_pc.yaml and gui_config_rpi.yaml already
	# exist.
	if PLATFORM_TYPE == "pc":
		old_file = "%s/gui_config_pc.yaml" % root_yaml_file_directory
		new_file = "%s/gui_config.yaml" % root_yaml_file_directory
		print("YAML file updated for your Local Machine.")
	else:
		old_file = "%s/gui_config_rpi.yaml" % root_yaml_file_directory
		new_file = "%s/gui_config.yaml" % root_yaml_file_directory
		print("YAML file updated for your Raspberry Pi.")
	os.system("cp %s %s" % (old_file, new_file))
	print("YAML file copied!")

def generate_starter_script():
	global LAUNCHER_DIRECTORY

	# This function will update the starter script which the Raspberry Pi will boot from.
	starter_script = "%s/run_score_tracker.sh" % (UPDATER_TEMPORARY_DIRECTORY + "/" + SCORE_TRACKER_ROOT_DIRECTORY)
	with open(starter_script, "w") as f:
		if PLATFORM_TYPE == "pc":
			LAUNCHER_DIRECTORY = "%s/Downloads/latest_gui" % HOME_DIRECTORY
		else:
			LAUNCHER_DIRECTORY = "/score_tracker"
		f.write("cd %s; sudo nice -10 python3 ./score_tracker_gui.py" % LAUNCHER_DIRECTORY)
		print("Finished writing starter script: %s." % starter_script)

def update_launcher_directory():
	# Remove the launcher directory if is exists. Clean up contents.
	if os.path.exists(LAUNCHER_DIRECTORY):
		if LAUNCHER_DIRECTORY not in ILLEGAL_DIRECTORIES:
			os.system("sudo rm -r %s/*" % LAUNCHER_DIRECTORY)
			print("Cleaned Launcher directory: %s." % LAUNCHER_DIRECTORY)
		else:
			raise ValueError("Cannot work with illegal directories!")
	else:
		os.system("mkdir %s" % LAUNCHER_DIRECTORY)
		print("Created Launcher directory: %s." % LAUNCHER_DIRECTORY)

	# copy git contents to the Launcher directory.
	os.system("cp -r %s/* %s" % (
		UPDATER_TEMPORARY_DIRECTORY + "/" + SCORE_TRACKER_ROOT_DIRECTORY,
		LAUNCHER_DIRECTORY))
	print("Launcher directory: %s is updated." % (UPDATER_TEMPORARY_DIRECTORY + "/" + SCORE_TRACKER_ROOT_DIRECTORY))

def launch_application():
	print("Launching Score Tracker.")
	command = "%s/run_score_tracker.sh" % LAUNCHER_DIRECTORY
	#subprocess.Popen(command.strip(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	subprocess.call(command, shell=True)

def run_updater():
	pull_from_git()
	modify_yaml_for_platform()
	close_existing_app()
	generate_starter_script()
	update_launcher_directory()
	launch_application()


if __name__ == '__main__':
	run_updater()
