# NOTE: Please copy all contents of the GUI into /score_tracker folder.
# Make sure that folder has read+write persmissions for every user.

# Required packages to run kivy libraries

# Get latest updates
sudo apt-get update

# Update kivy libraries
sudo python3 -m pip install kivy kivymd imutils

# Running OpenCV
sudo python3 -m pip install opencv-python
sudo python3 -m pip install -U numpy
sudo python3 -m pip install pyyaml
sudo apt-get -y install libcblas-dev
sudo apt-get -y libhdf5-dev
sudo apt-get -y libhdf5-serial-dev
sudo apt-get -y libatlas-base-dev
sudo apt-get -y libjasper-dev
sudo apt-get -y libqtgui4
sudo apt-get -y libqt4-test
sudo apt-get -y libilmbase-dev
sudo apt-get -y libopenexr-dev
sudo apt-get -y libgstreamer1.0-dev
sudo apt-get -y libavcodec-dev
sudo apt-get -y libavformat-dev
sudo apt-get -y libswscale-dev
sudo apt-get -y libwebp-dev

# Running Kivy with SDL2
sudo apt-get -y install libsdl2-dev
sudo apt-get -y install libsdl2-image-dev

# Install the MS Fonts package.
sudo apt-get -y install ttf-mscorefonts-installer

# Create the bootable script.
sudo touch /etc/xdg/autostart/score_tracker_gui.desktop
sudo echo "[Desktop Entry]" > /etc/xdg/autostart/score_tracker_gui.desktop
sudo echo "Name=Score_Tracker_GUI" >> /etc/xdg/autostart/score_tracker_gui.desktop
sudo echo "Exec=/bin/sh /score_tracker/run_score_tracker.sh"
