"""
   Copyright 2022, San Jose State Univerity

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

    Authors:
        - Hisaam Hashim
        - Jesse Pham
        - Nathan Luc
        - Huy Nguyen
    
    Dependencies:
        On the Raspberry Pi, please run the script 'score_tracker_dependencies.sh'
        prior to running this tool.

    Description:
        Score Tracker GUI currently only for Table Tennis, using the Ball Bounce
        Detector library, Coordinate 2D Mapper library, 1-2 cameras and a 4-button
        remote to monitor a table tennis game and save scores.
"""

import copy
import cv2
import datetime
import libraries.demo as demo
import json
import kivy
from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config
from kivy.graphics.context_instructions import Color
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics.vertex_instructions import Ellipse
from kivy.logger import Logger
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.vkeyboard import VKeyboard
from kivy.uix.widget import Widget
from kivy.core.window import Window
import libraries.ball_bounce_detector as bounce_detector
import libraries.coordinate_2d_mapper as coordinate_2d_mapper
import os
import random
import subprocess
import time
import webbrowser
import yaml
from yaml.loader import SafeLoader


#################################################
#   Config Settings
#################################################
#Config.set('kivy', 'keyboard_mode', 'docked')

#################################################
# Config YAML file
#################################################
config_yaml_file = 'common/gui_config.yaml'
GUI_CONFIG_PARAMS = []
with open(config_yaml_file) as f:
    GUI_CONFIG_PARAMS = yaml.load(f, Loader=SafeLoader)
    print(GUI_CONFIG_PARAMS)

#################################################
# Saved Sessions JSON File
#################################################
autosave_file = "common/autosave.json"
sessions_file = "common/saved_sessions.json"
TIMESTAMP_FORMAT="%Y-%m-%d %H:%M"

#################################################
# Screen Resolution
#################################################
DISPLAY_SIZE_X = GUI_CONFIG_PARAMS["core"]["display"]["x"]
DISPLAY_SIZE_Y = GUI_CONFIG_PARAMS["core"]["display"]["y"]
Window.size = (DISPLAY_SIZE_X, DISPLAY_SIZE_Y) # 1024 x 600
Window.minimum_width, Window.minimum_height = Window.size
print(Window.size)

# Enable Full Screen Mode.
# NOTE: This will change your Display Screen Resolution.
if GUI_CONFIG_PARAMS["core"]["fullscreen"]:
    Window.fullscreen = True

#################################################
# Ball Bounce Algorithm variables
#################################################
ball_bounce_proof_save_file = "artifacts/current_session/ball_bounce_proof.png"
CAMERA_FEED_VIDEO_FILES = [None, None]  # primary, secondary
BB_FILTER_IMAGES_DIRECTORY="artifacts/menus/bb_filter"
BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND = [164, 153, 150]
BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND = [255, 123, 0]
BALL_BOUNCE_DETECTION_TABLE_COLOR = [64, 144, 245]
BALL_BOUNCE_DETECTION_PARAM_IDS = None
BALL_BOUNCE_DETECTION_DEBUG_CANVAS = None
DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND = None
DEBUG_COLOR_INDICATOR_TABLE = None
DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND = None

#################################################
# GUI Shared Screen variables
#################################################
shared_menu_input_device_function = None
shared_running_session_scheduler_interval = float(1/30)
gui_initialize_menus_functions = [None, None, None]
shared_running_session = None
shared_running_session_game_mapper = None
running_session_scheduler_functions = [None, None, None, None]

#################################################
# Camera Settings
#################################################
CAMERA_RESOLUTION = (1920, 1080)
CAMERA_IMAGE_LOCATION = "artifacts/menus/camera/"
CONNECTED_CAMERAS = {'primary': None, 'secondary': None}
ATTACHED_CAMERAS = {'primary': None, 'secondary': None}
CAMERAS_READY = {'primary': False, 'secondary': False}
CAMERA_STREAMING = {'primary': False, 'secondary': False}

# This function uses subprocess to return the list of connected cameras in the system.
def find_camera_device_ids():
    search_command = "ls /dev/video*"

    list_of_connected_cameras = subprocess.run(search_command, shell=True, capture_output=True)
    connected_cameras = list_of_connected_cameras.stdout.decode()[:-1].split("\n")
    
    # TODO: For debug, remove once verified.
    Logger.info("MAIN - find_camera_device_ids: Cameras Found: %s" % connected_cameras)

    return connected_cameras

# Extracts the camera port numbers from /dev/video[x] device ids, and attaches it to CV2.
def connect_to_cameras(connected_cameras):
    # Attempt to attach the ids to CV2.
    # Example: {'primary': /dev/video1, 'secondary': /dev/video3} -> primary=1, secondary=3
    global ATTACHED_CAMERAS
    global CAMERAS_READY
    Logger.info("MAIN - find_camera_device_ids: Connecting camera: %s" % connected_cameras)
    Logger.info("MAIN - find_camera_device_ids: Camera Ready (Before): %s" % CAMERAS_READY)

    try:
        primary_port = None
        secondary_port = None
        if connected_cameras['primary'] is not None:
            primary_port = int(connected_cameras['primary'][10:].strip())
        if connected_cameras['secondary'] is not None:
            secondary_port = int(connected_cameras['secondary'][10:].strip())

        global ATTACHED_CAMERAS
        if primary_port is not None and not CAMERAS_READY['primary']:
            # Setup CV2 VideoCapture object on the selected port.
            # This will allow us to read each frame with the read() command.s
            ATTACHED_CAMERAS['primary'] = cv2.VideoCapture(primary_port)

            # Change the resolution to 800z600
            ATTACHED_CAMERAS['primary'].set(3,800)
            ATTACHED_CAMERAS['primary'].set(4,600)
            Logger.info("MAIN - find_camera_device_ids: Changed primary camera resolution to 800x600.")

            CAMERAS_READY['primary'] = True
            Logger.info("MAIN - find_camera_device_ids: Attached primary camera to CV2.")
        else:
            if primary_port is None:
                Logger.info("MAIN - find_camera_device_ids: Primary camera is not attached!")

        if secondary_port is not None and not CAMERAS_READY['secondary']:
            ATTACHED_CAMERAS['secondary'] = cv2.VideoCapture(secondary_port)
            ATTACHED_CAMERAS['secondary'].set(3,800)
            ATTACHED_CAMERAS['secondary'].set(4,600)
            Logger.info("MAIN - find_camera_device_ids: Changed secondary camera resolution to 800x600.")

            CAMERAS_READY['secondary'] = True
            Logger.info("MAIN - find_camera_device_ids: Attached secondary camera to CV2.")
        else:
            if secondary_port is None:
                Logger.info("MAIN - find_camera_device_ids: Secondary camera is not attached!")

    except Exception as e:
        Logger.debug("MAIN - find_camera_device_ids: Could not connect to either one or more camera ports! Try again.")
    
    Logger.info("MAIN - find_camera_device_ids: Camera Ready (After): %s" % CAMERAS_READY)


#################################################
#   Input Devices
#################################################
REMOTE_TYPE = "keyboard"
try:
    import RPi.GPIO as GPIO
    REMOTE_TYPE = "four_button_remote"
    Logger.info("GUI Init: Raspberry Pi is running GUI.")
except Exception as e:
    REMOTE_TYPE = "keyboard"
    Logger.info("GUI Init: Local Machine is running GUI.")

DEBUG_REMOTE_SCHEDULER_FUNCTION = None
KEYFOB_IMAGES_DIRECTORY="artifacts/menus/keyfob"
KEYFOB_LAST_BUTTON_PRESSED = 0
GUI_ENABLE_KEYFOB_ACTION = False
GUI_KEYBOARD_BUTTON_PRESS = False
GUI_KEYFOB_REMOTE_KEY_PRESS = {"A": False, "B": False, "C": False, "D": False}
GUI_KEYBOARD_PRESS_COUNTER = 0
KEYFOB_ACTIONS = ["PAUSE_OR_RESUME", "RESTORE_LAST", "TEAM1_INCR", "TEAM2_INC"]
#################################################################################################
#   The following functions are only used if run on the Raspberry Pi with the Keyfob connected
#   to the following pins:
#       D0: GPIO_22
#       D1: GPIO_27
#       D2: GPIO_24
#       D3: GPIO_23
#################################################################################################
def SetupGPIOPins():
    '''
        Configures 4 pins on the Rasberry Pi as input pins.
        D3 - D0 correspond to Keyfob Buttons A - D respectively.
    '''
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def KeyfobStatus():
    '''
        Returns a dictionary containing button press states.
            True = Button Pressed
            False = Button Not Pressed
    '''
    button_a = GPIO.input(23)
    button_b = GPIO.input(24)
    button_c = GPIO.input(27)
    button_d = GPIO.input(22)

    KEYFOB_REMOTE_KEY_PRESS = {"A": False, "B": False, "C": False, "D": False}

    # TODO: Not sure if pressing multiple buttons together changes outputs
    # If it doesn't replace "elif" with "if". Although, last BUTTON pressed state
    # Needs to be updated in that case. The implementation below only works
    # For one button press at a time.
    global KEYFOB_LAST_BUTTON_PRESSED
    if button_a and KEYFOB_LAST_BUTTON_PRESSED != 1:
        KEYFOB_REMOTE_KEY_PRESS['A'] = True
        KEYFOB_LAST_BUTTON_PRESSED = 1
    elif button_b and KEYFOB_LAST_BUTTON_PRESSED != 2:
        KEYFOB_REMOTE_KEY_PRESS['B'] = True
        KEYFOB_LAST_BUTTON_PRESSED = 2
    elif button_c and KEYFOB_LAST_BUTTON_PRESSED != 3:
        KEYFOB_REMOTE_KEY_PRESS['C'] = True
        KEYFOB_LAST_BUTTON_PRESSED = 3
    elif button_d and KEYFOB_LAST_BUTTON_PRESSED != 4:
        KEYFOB_REMOTE_KEY_PRESS['D'] = True
        KEYFOB_LAST_BUTTON_PRESSED = 4
    elif not button_a and not button_b and not button_c and not button_d:
        KEYFOB_LAST_BUTTON_PRESSED = 0

    return KEYFOB_REMOTE_KEY_PRESS


#################################################
#   Saved History
#################################################
def ClearHistory(clear_autosave=False):
    if os.path.exists(sessions_file):
        os.system("rm %s" % sessions_file)
        Logger.info("GUI Functions - ClearHistory: Deleted Saved Sessions file: %s." % sessions_file)
    if clear_autosave:
        if os.path.exists(autosave_file):
            os.system("rm %s" % autosave_file)
            Logger.info("GUI Functions - ClearHistory: Deleted Autosave Sessions file: %s." % autosave_file)

def GenerateHistorySamples():
    session_data1 = {"date": "2022-11-21 11:45",
                     "game_type": "Normal",
                     "game_mode": "Singles",
                     "sets": 3,
                     "title": "Session 1: Hisaam vs Huy 2022",
                     "team1": "Huy",
                     "team2": "Hisaam",
                     "team1_sets": 1, 
                     "team1_points": 6,
                     "team2_sets": 1, 
                     "team2_points": 4,
                     "server": 1}
    session_data2 = {"date": "2022-10-15 17:00",
                     "game_type": "Normal",
                     "game_mode": "Singles",
                     "sets": 3,
                     "title": "Session 2: Jessie vs Nathan 2022",
                     "team1": "Jessie",
                     "team2": "Nathan",
                     "team1_sets": 1, 
                     "team1_points": 9,
                     "team2_sets": 1, 
                     "team2_points": 8,
                     "server": 2}
    session_data3 = {"date": "2022-10-15 13:12",
                     "game_type": "Normal",
                     "game_mode": "Singles",
                     "sets": 3,
                     "title": "Session 3: Preet vs Huy 2022",
                     "team1": "Preet",
                     "team2": "Huy",
                     "team1_sets": 2, 
                     "team1_points": 10,
                     "team2_sets": 0, 
                     "team2_points": 3,
                     "server": 1}
        
    # Combine each session data into a list.
    all_session_data = []
    all_session_data.append(session_data1)
    all_session_data.append(session_data2)
    all_session_data.append(session_data3)

    # Check if a sessions file already exists.
    # If it does, read back all entries, append
    # to the list and then write back.
    new_sessions_data = []
    if os.path.exists(sessions_file):
        with open(sessions_file, "r") as json_readfile:
            Logger.debug("GUI Functions - GenerateHistorySamples: Previous sessions data, found. Combining.")

            # Read the previous data
            old_sessions_data = json.load(json_readfile)
            if len(old_sessions_data) > 6:
                old_sessions_data.pop(0)
                old_sessions_data.pop(0)
                old_sessions_data.pop(0)
            # Append to data.
            old_sessions_data.extend(all_session_data)
            new_sessions_data = old_sessions_data
            
    else:
        Logger.debug("GUI Functions - GenerateHistorySamples: No sessions data found, saving new data...")
        new_sessions_data = all_session_data
        
    # Finally write session data to the JSON file.
    with open(sessions_file, "w") as json_writefile:
        new_sessions_data_json = json.dumps(new_sessions_data, indent=14)
        json_writefile.write(new_sessions_data_json)
        Logger.debug("GUI Functions - GenerateHistorySamples: Saved sample history sessions to: %s" % sessions_file)

#################################################
#   Main Menu
#################################################
class MainMenu(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(MainMenu, self).__init__(**kwargs)

        # Update the version # based on the YAML file.
        self.ids.main_menu_version_number.text = "Version: " + GUI_CONFIG_PARAMS["version"]

        # Enable Input devices.
        global shared_menu_input_device_function
        shared_menu_input_device_function = self.enable_input_devices
        shared_menu_input_device_function()

    ######################################################
    # Keyboard Binding shared with all screens.
    ######################################################
    def enable_input_devices(self):
        global GUI_ENABLE_KEYFOB_ACTION
        global GUI_KEYBOARD_BUTTON_PRESS
        global GUI_KEYFOB_REMOTE_KEY_PRESS
        global GUI_KEYBOARD_PRESS_COUNTER
        GUI_ENABLE_KEYFOB_ACTION = False
        GUI_KEYBOARD_BUTTON_PRESS = False
        GUI_KEYFOB_REMOTE_KEY_PRESS = {"A": False, "B": False, "C": False, "D": False}

        if REMOTE_TYPE == "four_button_remote":
            # Configure input pins to read out Keyfob Button presses.
            Logger.info("MainMenu - enable_input_devices: RF 4-button remote is the input device.")
            SetupGPIOPins()
        else:
            # Simulate the keyfob button presses with the keyboard
            self._gui_keyboard = Window.request_keyboard(self._gui_keyboard_closed, self)
            self._gui_keyboard.bind(on_key_down=self._gui_keyboard_pressed_down)
            GUI_KEYBOARD_PRESS_COUNTER = 0
            Logger.info("MainMenu - enable_input_devices: Local keyboard is the input device.")

    #####################################################################################
    # The following two functions are when run on the local machine.
    # Keyboard presses of A. B, C, and D simulate keyfob presses.
    #####################################################################################
    def _gui_keyboard_closed(self):
        # This function is for cleanup after the program closes.
        self._gui_keyboard.unbind(on_key_down=self._gui_keyboard_pressed_down)
        self._gui_keyboard = None

    def _gui_keyboard_pressed_down(self, keyboard, keycode, text, modifiers):
        global GUI_KEYFOB_REMOTE_KEY_PRESS
        global GUI_KEYBOARD_BUTTON_PRESS
        # This function tracks keyboard presses.
        # 1. Check key press on keyboard for A, B, C, D
        # 2. Update the BLUETOOTH_REMOTE_KEY_PRESS global variable.

        # By default set all key presses to False.
        GUI_KEYFOB_REMOTE_KEY_PRESS['A'] = False
        GUI_KEYFOB_REMOTE_KEY_PRESS['B'] = False
        GUI_KEYFOB_REMOTE_KEY_PRESS['C'] = False
        GUI_KEYFOB_REMOTE_KEY_PRESS['D'] = False

        # Set the key button to true only if the corresponding button was pressed
        if keycode[1] == 'a' or  keycode[1] == "A":
            GUI_KEYFOB_REMOTE_KEY_PRESS['A'] = True
            GUI_KEYBOARD_BUTTON_PRESS = True
        elif keycode[1] == 'b' or  keycode[1] == "B":
            GUI_KEYFOB_REMOTE_KEY_PRESS['B'] = True
            GUI_KEYBOARD_BUTTON_PRESS = True
        elif keycode[1] == 'c' or  keycode[1] == "C":
            GUI_KEYFOB_REMOTE_KEY_PRESS['C'] = True
            GUI_KEYBOARD_BUTTON_PRESS= True
        elif keycode[1] == 'd' or  keycode[1] == "D":
            GUI_KEYFOB_REMOTE_KEY_PRESS['D'] = True
            GUI_KEYBOARD_BUTTON_PRESS = True

        return True
    
    # Clicking this button takes you to the Start Game Setting Screen.
    def main_menu_start_game_button(self):
        # Switch to the New Sessions Settings Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "start_new_session_settings_menu"

    # Clicking this button takes you to the Restore Session Menu.
    def main_menu_restore_session_button(self):
        # Automatically refresh the history.
        global gui_initialize_menus_functions
        Clock.schedule_once(gui_initialize_menus_functions[1])

        # Switch to the Restore Sessions Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "restore_sessions_menu"

    # Clicking this button takes you to the Debug Menu.
    def main_menu_debug_menu_button(self):
        # Switch to the About Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "debug_menu"

    # Clicking this button takes you to the About Menu.
    def main_menu_about_menu_button(self):
        # Switch to the About Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "about_menu"


#################################################
#   Start New Session Settings Menu
#################################################
class NewSessionSettingsPopup(Popup):
    def __init__(self, functions, **kwargs):
        super(NewSessionSettingsPopup, self).__init__(**kwargs)
        self.functions = functions


class NewSessionSettings(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(NewSessionSettings, self).__init__(**kwargs)

        self.virtual_keyboard_enabled = False
        self.virtual_keyboard_box_selected = 0

        # Update the views with defaults from YAML.
        self.load_new_session_settings_from_yaml()

    #####################################################################################
    # Virtual Keyboard functions for Title TextInput
    #####################################################################################
    def new_session_settings_title_virtual_keyboard_enable(self, *args):
        if not self.virtual_keyboard_enabled or self.virtual_keyboard_box_selected in [2, 3]:
            # Remove previous keyboard if it already exists.
            if self.virtual_keyboard_box_selected == 2:
                self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team1)
                Logger.debug("NewSessionSettings - new_session_settings_title_virtual_keyboard_enable: Removed Team1 Vkeyboard")
            elif self.virtual_keyboard_box_selected == 3:
                self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team2)
                Logger.debug("NewSessionSettings - new_session_settings_title_virtual_keyboard_enable: Removed Team2 Vkeyboard")
           
            # Initialize a new virtual keyboard for modifying the title label.
            self.virtual_keyboard_title = VKeyboard(docked=False,
                                                    on_key_up=self.new_session_settings_title_virtual_keyboard_keypress)

            # TODO: Change the size of the virtual keyboard.
            self.ids.new_session_settings_vkeyboard_placement.add_widget(self.virtual_keyboard_title)

            # Set these flags to know whehn keyboard is enabled as well as which box was selected.
            self.virtual_keyboard_enabled = True
            self.virtual_keyboard_box_selected = 1

            # TODO: Remove these debug messages.
            Logger.info("NewSessionSettings - new_session_settings_title_virtual_keyboard_enable: Enabling Virtual Keyboard for Title.")

    # When typing on the keyboard, automatically update the title label.
    # Also, when ENTER is pressed, close the keyboard.
    def new_session_settings_title_virtual_keyboard_keypress(self, keyboard, keycode, *args):
        if isinstance(keycode, tuple):
            keycode = keycode[1]

        # Obtain the label for the title.
        title_textinput = self.ids.new_session_settings_title_input
        title_text = title_textinput.text

        # When the user presses enter, the virtual keyboard should now close.
        if keycode == "enter":
            # Set the flags to indicate that the virtual keyboard is now closed:
            self.virtual_keyboard_enabled = False
            self.virtual_keyboard_box_selected = 0

            # Remove the virtual keyboard widget.
            self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_title)
            print("Removed Title Vkeyboard")
        else:
            # Back space removes the last character.
            if keycode == "backspace":
                title_text = title_text[:-1]
                keycode = ""
            
            if keycode == "spacebar":
                keycode = " "

            # Update the label.
            title_textinput.text = f'{title_text}{keycode}'

    #####################################################################################
    # Virtual Keyboard functions for Team 1 TextInput
    #####################################################################################
    def new_session_settings_team1_virtual_keyboard_enable(self, *args):
        if not self.virtual_keyboard_enabled or self.virtual_keyboard_box_selected in [1, 3]:
            # Remove previous keyboard if it already exists.
            if self.virtual_keyboard_box_selected == 1:
                self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_title)
                Logger.debug("NewSessionSettings - new_session_settings_team1_virtual_keyboard_enable: Removed Title Vkeyboard")
            elif self.virtual_keyboard_box_selected == 3:
                self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team2)
                Logger.debug("NewSessionSettings - new_session_settings_team1_virtual_keyboard_enable: Removed Team2 Vkeyboard")
            
            # Initialize a new virtual keyboard for modifying the title label.
            self.virtual_keyboard_team1 = VKeyboard(docked=False,
                                                    on_key_up=self.new_session_settings_team1_virtual_keyboard_keypress)

            # TODO: Change the size of the virtual keyboard.
            self.ids.new_session_settings_vkeyboard_placement.add_widget(self.virtual_keyboard_team1)

            # Set these flags to know whehn keyboard is enabled as well as which box was selected.
            self.virtual_keyboard_enabled = True
            self.virtual_keyboard_box_selected = 2

            # TODO: Remove these debug messages.
            Logger.debug("NewSessionSettings - new_session_settings_team1_virtual_keyboard_enable: Enabling Virtual Keyboard for Team1.")

    def new_session_settings_team1_virtual_keyboard_keypress(self, keyboard, keycode, *args):
        if isinstance(keycode, tuple):
            keycode = keycode[1]

        # Obtain the label for the title.
        team1_textinput = self.ids.new_session_settings_team1_input
        team1_text = team1_textinput.text

        # When the user presses enter, the virtual keyboard should now close.
        if keycode == "enter":
            # Set the flags to indicate that the virtual keyboard is now closed:
            self.virtual_keyboard_enabled = False
            self.virtual_keyboard_box_selected = 0

            # Remove the virtual keyboard widget.
            self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team1)
            print("Removed Team1 Vkeyboard")
        else:
            # Back space removes the last character.
            if keycode == "backspace":
                team1_text = team1_text[:-1]
                keycode = ""
            
            if keycode == "spacebar":
                keycode = " "

            # Update the label.
            team1_textinput.text = f'{team1_text}{keycode}'

    #####################################################################################
    # Virtual Keyboard functions for Team 2 TextInput
    #####################################################################################
    def new_session_settings_team2_virtual_keyboard_enable(self, *args):
        if not self.virtual_keyboard_enabled or self.virtual_keyboard_box_selected in [1, 2]:
            # Remove previous keyboard if it already exists.
            if self.virtual_keyboard_box_selected == 1:
                self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_title)
                Logger.debug("NewSessionSettings - new_session_settings_team2_virtual_keyboard_enable: Removed Title Vkeyboard")
            elif self.virtual_keyboard_box_selected == 2:
                self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team1)
                Logger.debug("NewSessionSettings - new_session_settings_team2_virtual_keyboard_enable: Removed Team1 Vkeyboard")
            
            # Initialize a new virtual keyboard for modifying the title label.
            self.virtual_keyboard_team2 = VKeyboard(docked=False,
                                                    on_key_up=self.new_session_settings_team2_virtual_keyboard_keypress)

            # TODO: Change the size of the virtual keyboard.
            self.ids.new_session_settings_vkeyboard_placement.add_widget(self.virtual_keyboard_team2)

            # Set these flags to know whehn keyboard is enabled as well as which box was selected.
            self.virtual_keyboard_enabled = True
            self.virtual_keyboard_box_selected = 3

            # TODO: Remove these debug messages.
            Logger.debug("NewSessionSettings - new_session_settings_team2_virtual_keyboard_enable: Enabling Virtual Keyboard for Team2.")

    def new_session_settings_team2_virtual_keyboard_keypress(self, keyboard, keycode, *args):
        if isinstance(keycode, tuple):
            keycode = keycode[1]

        # Obtain the label for the title.
        team2_textinput = self.ids.new_session_settings_team2_input
        team2_text = team2_textinput.text

        # When the user presses enter, the virtual keyboard should now close.
        if keycode == "enter":
            # Set the flags to indicate that the virtual keyboard is now closed:
            self.virtual_keyboard_enabled = False
            self.virtual_keyboard_box_selected = 0

            # Remove the virtual keyboard widget.
            self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team2)
            print("Removed Team2 Vkeyboard")
        else:
            # Back space removes the last character.
            if keycode == "backspace":
                team2_text = team2_text[:-1]
                keycode = ""
            
            if keycode == "spacebar":
                keycode = " "

            # Update the label.
            team2_textinput.text = f'{team2_text}{keycode}'

    #####################################################################################
    # General button press actions.
    #####################################################################################
    # Clicking this button takes you back to the main menu.
    def new_session_settings_goback_button(self):
        # Switch to the Main Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "main_menu"

    # Drop down selection for "Game Type"
    def new_sessions_settings_game_type_selected(self, value):
        print("Game Type selected: %s" % value)

    # Drop down selection for "Game Mode"
    def new_sessions_settings_game_mode_selected(self, value):
        print("Game Mode selected: %s" % value)

    # Drop down selection for "Sets"
    def new_sessions_settings_set_selected(self, value):
        print("# Sets are set to %s." % value)

    #####################################################################################
    # Popup actions.
    #####################################################################################
    def confirm_settings_button(self, *args):
        # Configure the label for the popup.
        new_session_settings = "Game Type: %s\nGame Mode: %s\nTitle: %s\nTeam1 Name: %s\nTeam2 Name: %s\n# Sets: %s" % (
            self.ids.new_sessions_settings_game_type.text,
            self.ids.new_sessions_settings_game_mode.text,
            self.ids.new_session_settings_title_input.text,
            self.ids.new_session_settings_team1_input.text,
            self.ids.new_session_settings_team2_input.text,
            self.ids.new_sessions_settings_set_dropdown.text
        )

        # Setup the bundle used for passing onwards to the Running session.
        self.new_session_settings = {"game_type": self.ids.new_sessions_settings_game_type.text,
                                     "game_mode": self.ids.new_sessions_settings_game_mode.text,
                                     "sets": int(self.ids.new_sessions_settings_set_dropdown.text),
                                     "title": self.ids.new_session_settings_title_input.text,
                                     "team1_name": self.ids.new_session_settings_team1_input.text,
                                     "team2_name": self.ids.new_session_settings_team2_input.text,
                                     "team1_score": {"sets": 0, "points": 0},
                                     "team2_score": {"sets": 0, "points": 0},
                                     "server": 1}

        # Configure the popup and then show it.
        self.confirm_settings_popup = NewSessionSettingsPopup(self)
        self.confirm_settings_popup.ids.confirm_settings_popup_label.text = new_session_settings
        self.confirm_settings_popup.open()

        # Close all virtual keyboards.
        if self.virtual_keyboard_box_selected == 1:
            self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_title)
            Logger.debug("NewSessionSettings - start_new_session_in_running_session_menu: Removed Title Vkeyboard")
        elif self.virtual_keyboard_box_selected == 2:
            self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team1)
            Logger.debug("NewSessionSettings - start_new_session_in_running_session_menu: Removed Team1 Vkeyboard")
        elif self.virtual_keyboard_box_selected == 3:
            self.ids.new_session_settings_vkeyboard_placement.remove_widget(self.virtual_keyboard_team2)
            Logger.debug("NewSessionSettings - start_new_session_in_running_session_menu: Removed Team2 Vkeyboard")

        # Update the YAML file.
        self.update_new_session_settings_in_yaml()

        # Re-enable the menu for the keyboard binding.
        shared_menu_input_device_function()

    def start_new_session_in_running_session_menu(self, *args):
        # Dismiss the popup window.
        self.confirm_settings_popup.dismiss()

        ##################################################
        # COIN FLIP IMAGE
        ##################################################
        # If selected team = 1, display heads image, otherwise display tails image.
        global shared_new_sessions_coin_flip_image
        self.team_randomly_selected = coordinate_2d_mapper.randomly_choose_starting_server_team()
        Logger.info("NewSessionSettings - start_new_session_in_running_session_menu: Team %s is now the starting team!" % self.team_randomly_selected)

        # Team #1 is the starting team.
        if self.team_randomly_selected == 1:
            self.ids.new_session_team_selection.source="artifacts/menus/new_session/quarter_heads_player1.png"
            self.ids.new_session_team_selection.opacity = 1
        else:
            self.ids.new_session_team_selection.source="artifacts/menus/new_session/quarter_tails_player2.png"
            self.ids.new_session_team_selection.opacity = 1

        # Update the shared data bundle with new session settings.
        self.new_session_settings["server"] = self.team_randomly_selected
        global shared_running_session
        shared_running_session = None
        shared_running_session = self.new_session_settings
        Logger.info("NewSessionSettings - start_new_session_in_running_session_menu: Parameters to pass to running session: %s" % shared_running_session)

        # Transition in 3 seconds.
        Clock.schedule_once(self.updated_variables_and_start_running_session, 3)

    def updated_variables_and_start_running_session(self, *args):
        # Hide the coin flip image.
        self.ids.new_session_team_selection.opacity = 0
        Logger.debug("NewSessionSettings - updated_variables_and_start_running_session: Coin Flip image is removed.")

        #############################################
        # Configure the scheduler for the demo
        #############################################
        global running_session_scheduler_functions
        global shared_running_session_scheduler_interval
        if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
            shared_running_session_scheduler_interval = 5
            Clock.schedule_once(running_session_scheduler_functions[1])
            Logger.debug("NewSessionSettings - updated_variables_and_start_running_session: Running Debug Init scheduler.")
        else:
            shared_running_session_scheduler_interval = float(1/30)
        Logger.debug("NewSessionSettings - updated_variables_and_start_running_session: Scheduler interval is now set to %s seconds." % shared_running_session_scheduler_interval)

        # Run the schedulers for the running session.
        Clock.schedule_interval(running_session_scheduler_functions[0], 1/60)
        Clock.schedule_interval(running_session_scheduler_functions[1], shared_running_session_scheduler_interval)
        print("NewSessionSettings - updated_variables_and_start_running_session: Running Scheduler for Score Tracker with camera feed.")

        # Transition to the Running Session Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "running_session_menu"
        print("NewSessionSettings - updated_variables_and_start_running_session: Transitioning to the Running Session Menu.")

    #####################################################################################
    # Updating YAML file with entries saved in the New Sessions
    #####################################################################################
    def update_new_session_settings_in_yaml(self):
        with open(config_yaml_file, "w") as yaml_f:
            # Modify the ball bounce entries in the YAML file.
            GUI_CONFIG_PARAMS["score_tracker"]["default_mode"] = self.ids.new_sessions_settings_game_mode.text
            GUI_CONFIG_PARAMS["score_tracker"]["default_sets"] = int(self.ids.new_sessions_settings_set_dropdown.text)
            GUI_CONFIG_PARAMS["score_tracker"]["default_team1"] = str(self.ids.new_session_settings_team1_input.text).strip()
            GUI_CONFIG_PARAMS["score_tracker"]["default_team2"] = str(self.ids.new_session_settings_team2_input.text).strip()

            # Store the contents back into the YAML file and sort it.
            yaml.dump(GUI_CONFIG_PARAMS, yaml_f, sort_keys=True)
            Logger.debug("NewSessionSettings - update_new_session_settings_in_yaml: Updated the config yaml file with latest new session settings.")

    def load_new_session_settings_from_yaml(self):
            self.ids.new_sessions_settings_game_mode.text = GUI_CONFIG_PARAMS["score_tracker"]["default_mode"]
            self.ids.new_sessions_settings_set_dropdown.text = str(GUI_CONFIG_PARAMS["score_tracker"]["default_sets"])
            self.ids.new_session_settings_team1_input.text = GUI_CONFIG_PARAMS["score_tracker"]["default_team1"]
            self.ids.new_session_settings_team2_input.text = GUI_CONFIG_PARAMS["score_tracker"]["default_team2"]
            self.ids.new_session_settings_title_input.text = "%s vs %s" % (
                self.ids.new_session_settings_team1_input.text, self.ids.new_session_settings_team2_input.text)
            Logger.debug("NewSessionSettings - update_new_session_settings_in_yaml: Updated New Session settings with YAML defaults.")

#################################################
#   New Running Session Menu
#################################################
class RunningSessionSavePopop(Popup):
    def __init__(self, functions, **kwargs):
        super(RunningSessionSavePopop, self).__init__(**kwargs)
        self.functions = functions


class RunningSessionUndoPopop(Popup):
    def __init__(self, functions, **kwargs):
        super(RunningSessionUndoPopop, self).__init__(**kwargs)
        self.functions = functions


class RunningSessionGameOverPopop(Popup):
    def __init__(self, functions, **kwargs):
        super(RunningSessionGameOverPopop, self).__init__(**kwargs)
        self.functions = functions


class RunningSession(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(RunningSession, self).__init__(**kwargs)
        
        # Reset all flags.
        self.undo_flag = False
        self.paused_flag = False
        self.popup_is_running = False
        self.score_was_changed = False  # NOTE: Currently unused.

        ######################################################################################################
        # TODO: The functions below can be added to their own init function which can be called separately.
        #
        # Initialize the ball bounce metrics (updated by Clock with Ball Bounce Detection Algorithm)
        self.incoming_ball_bounce_metrics = {"ready": False,
                                             "x": 50,
                                             "y": 75,
                                             "timeout_exceeded": False,
                                             "bounce_proof": "artifacts/current_session/ball_bounce_proof.png"}

        # TODO: This should be passed from the "Settings" menu or when loading a saved session.
        self.current_session_data = {"game_type": "Practice",
                                     "game_mode": "Singles",
                                     "sets": 5,
                                     "title": "Placeholder",
                                     "team1_name": "Chad",
                                     "team2_name": "Karen",
                                     "team1_score": {"sets": 0, "points": 0},
                                     "team2_score": {"sets": 0, "points": 0},
                                     "server": 1}

        global shared_running_session
        shared_running_session = self.current_session_data

        # Initialize the time for the last time the ball bounce was displayed.
        self.ball_bounce_proof_last_displayed_time = time.time()

        ################################################
        # Run these updater functions on boot.
        ################################################
        self.update_session_ball_bounce_metrics(self.incoming_ball_bounce_metrics)
        self.update_session_info(self.current_session_data)
        ######################################################################################################

        # Conifugre the input device monitor function.
        global running_session_scheduler_functions
        running_session_scheduler_functions[0] = self.check_keyfob_remote_presses

        ###############################################################################
        # Configure the scheduler for when demo is selected, or not.
        ###############################################################################

        if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
            global shared_running_session_scheduler_interval
            shared_running_session_scheduler_interval = 5
            self.demo_handler = None
            running_session_scheduler_functions[1] = self.issue_demo_playback
            Logger.info("RunningSession: Demo scheduler is configured.")
        else:
            self.bounce_detector_initialized = False
            self.ball_bounce_tracker = None
            self.enable_live_cameras = False

            if "video_demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
                self.enable_live_cameras = False
                global CAMERA_FEED_VIDEO_FILES
                CAMERA_FEED_VIDEO_FILES[0] = "artifacts/demo/primary_top_view_demo1.mp4"

                # Rescale the camera image.
                self.ids.running_session_ball_bounce_proof.size_hint = (0.58, 0.58)
                self.ids.running_session_ball_bounce_proof.pos_hint = {"x": 0.025, "top": 0.89}

                Logger.info("RunningSession: Ball Bounce Detector will now use video feed #1(%s) for detector + scoring demo." % CAMERA_FEED_VIDEO_FILES[0])
                running_session_scheduler_functions[1] = self.ball_bounce_detector_play_prerecorded_video
            else: 
                self.enable_live_cameras = True
                Logger.info("RunningSession: Ball Bounce Detector is currently unavailable. The scheduler does nothing..")
                running_session_scheduler_functions[1] = self.ball_bounce_detector_monitor_cameras
        
    #####################################################################################
    # Recording functions. Demo and actual Ball Bounce Detector.
    #####################################################################################
    def issue_demo_playback(self, *args):
        global shared_running_session
        global shared_running_session_game_mapper

        # TODO: Replace with demo_handler_init() function call?
        if self.demo_handler is None: 
            # Remove the mapper.
            shared_running_session_game_mapper  = None

            # Configure the API with the deep copied parameters.
            self.transferred_session_params = copy.deepcopy(shared_running_session)
            transfered_team1_info = {
                "name": self.transferred_session_params["team1_name"],
                "sets": self.transferred_session_params["team1_score"]["sets"],
                "points": self.transferred_session_params["team1_score"]["points"]}
            transfered_team2_info = {
                "name": self.transferred_session_params["team2_name"],
                "sets": self.transferred_session_params["team2_score"]["sets"],
                "points": self.transferred_session_params["team2_score"]["points"]}
            
            self.demo_handler = demo.gui_demos(
                demo_name=GUI_CONFIG_PARAMS["demo"],
                starting_server=self.transferred_session_params["server"],
                sets=self.transferred_session_params["sets"],
                game_mode=self.transferred_session_params["game_mode"].lower(),
                team1_info=transfered_team1_info,
                team2_info=transfered_team2_info,
                verbose=True)

        # Do the playback for the demo and update the scores on the running session menu.
        team1_scores, team2_scores, current_server, ball_bounce_metrics, game_is_finished = self.demo_handler.playback_demo()
        self.current_session_data = {}
        self.current_session_data = {
            "game_type": self.transferred_session_params["game_type"],
            "game_mode": self.transferred_session_params["game_mode"],
            "sets": self.transferred_session_params["sets"],
            "title": self.transferred_session_params["title"],
            "team1_name": team1_scores["name"],
            "team2_name": team2_scores["name"],
            "team1_score": {"sets": team1_scores["sets"], "points": team1_scores["points"]},
            "team2_score": {"sets": team2_scores["sets"], "points": team2_scores["points"]},
            "server": current_server
        }
        shared_running_session = self.current_session_data
        self.incoming_ball_bounce_metrics = ball_bounce_metrics
        Logger.info("RunningSession - issue_demo_playback: Scores got updated.")

        # Call the update functions.
        self.update_session_ball_bounce_metrics(self.incoming_ball_bounce_metrics)
        self.update_session_info(self.current_session_data)
        Logger.info("RunningSession - issue_demo_playback: Updated all the views on the running session.")

        #####################################
        # LINK THE GAME MAPPER FOR THE DEMO
        #####################################
        shared_running_session_game_mapper = self.demo_handler.get_mapper()

        # Finally check if the game was finished. If it did, stop the scheduler.
        if game_is_finished:
            global running_session_scheduler_functions
            global shared_running_session_scheduler_interval

            # Change the notification text to game finished.
            self.ids.running_session_game_notification.text = "GAME FINISHED"
            Clock.unschedule(running_session_scheduler_functions[0])
            Clock.unschedule(running_session_scheduler_functions[1])
            Logger.info("RunningSession - issue_demo_playback: Stopped the scheduler running the demo.")

            self.demo_handler = None
            shared_running_session_scheduler_interval = 5
            Logger.info("RunningSession - issue_demo_playback: The Demo gameplay was finished.")

            ##################################################
            # GAMEOVER POPUP COMES UP
            ##################################################
            self.gameover_popup_triggered()

    def ball_bounce_detector_play_prerecorded_video(self, *args):
        global shared_running_session_game_mapper
        global shared_running_session

        if not self.bounce_detector_initialized:
            global BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND
            global BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND
            global CAMERA_FEED_VIDEO_FILES
            global CAMERA_RESOLUTION
            global CONNECTED_CAMERAS
            global GUI_CONFIG_PARAMS
            global ball_bounce_proof_save_file

            ball_color = [
                tuple(BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND),
                tuple(BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND)]

            camera_feed_video_files = None
            if self.enable_live_cameras:
                Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Ball Bounce Detector is initialized with live cameras: %s." % CONNECTED_CAMERAS)
            else:
                camera_feed_video_files = CAMERA_FEED_VIDEO_FILES
                Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Ball Bounce Detector is initialized with video feed(s): %s." % camera_feed_video_files)

            enable_video_debug = False
            if "video_playback_debug" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["video_playback_debug"]:
                enable_video_debug = True

            # Initialize the game mapper
            self.ball_bounce_tracker = bounce_detector.BallBounceDetector(
                save_image=ball_bounce_proof_save_file,
                camera_devices=CONNECTED_CAMERAS,
                camera_resolution=CAMERA_RESOLUTION,
                ball_colors=ball_color,
                video_files=camera_feed_video_files,
                oob_inverval=GUI_CONFIG_PARAMS["score_tracker"]["oob_interval"],
                verbose=False,
                debug=enable_video_debug)
            
            # Initialie the game mapper!
            self.transferred_session_params = copy.deepcopy(shared_running_session)

            transfered_team1_info = {
                "name": self.transferred_session_params["team1_name"],
                "sets": self.transferred_session_params["team1_score"]["sets"],
                "points": self.transferred_session_params["team1_score"]["points"]}
            transfered_team2_info = {
                "name": self.transferred_session_params["team2_name"],
                "sets": self.transferred_session_params["team2_score"]["sets"],
                "points": self.transferred_session_params["team2_score"]["points"]}

            shared_running_session_game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
                starting_server_team=self.transferred_session_params["server"],
                num_of_sets=self.transferred_session_params["sets"],
                ruleset_type=self.transferred_session_params["game_mode"].lower(),
                team1_info=transfered_team1_info,
                team2_info=transfered_team2_info,
                verbose=True)

            # Call the update functions.
            self.update_session_ball_bounce_metrics(self.incoming_ball_bounce_metrics)
            self.update_session_info(self.current_session_data)
            Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Updated all the views with initialized data.")

            # Finally indicate that the mapper is initialized
            self.bounce_detector_initialized = True
            Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Game Mapper initialized with transfered settings.")

        # If the detector is initialized, analyze the feed and obtain the metrics.
        if self.bounce_detector_initialized:
            # Get the last action from the Coordinate 2D Mapper and pass to the Ball Bounce Detector.
            self.ball_bounce_tracker.set_action(shared_running_session_game_mapper.get_action())
            _, ball_was_bounced, self.incoming_ball_bounce_metrics = self.ball_bounce_tracker.analyze_top_view_camera()

            # If the ball bounces, and only when it bounces, obtain the metrics.
            if ball_was_bounced:
                # Send the metrics to the the game mapper and get back the latest info from the mapper.
                shared_running_session_game_mapper.play_game(x=self.incoming_ball_bounce_metrics["x"], y=self.incoming_ball_bounce_metrics["y"])

            # Get updates from the mapper.
            team1_scores, team2_scores = shared_running_session_game_mapper.get_scores()
            game_is_finished, _ = shared_running_session_game_mapper.get_game_state()
            current_server = shared_running_session_game_mapper.get_server()

            # Update the current session data with said updates.
            self.current_session_data = {}
            self.current_session_data = {
                "game_type": self.transferred_session_params["game_type"],
                "game_mode": self.transferred_session_params["game_mode"],
                "sets": self.transferred_session_params["sets"],
                "title": self.transferred_session_params["title"],
                "team1_name": team1_scores["name"],
                "team2_name": team2_scores["name"],
                "team1_score": {"sets": team1_scores["sets"], "points": team1_scores["points"]},
                "team2_score": {"sets": team2_scores["sets"], "points": team2_scores["points"]},
                "server": current_server
            }
            shared_running_session = self.current_session_data

            # Call the update functions.
            self.update_session_ball_bounce_metrics(self.incoming_ball_bounce_metrics)
            self.update_session_info(self.current_session_data)
            Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Updated all the views on the running session.")

            # Finally check if the game was finished. If it did, stop the scheduler.
            if game_is_finished:
                global running_session_scheduler_functions
                global shared_running_session_scheduler_interval

                # Change the notification text to game finished.
                self.ids.running_session_game_notification.text = "GAME FINISHED"
                Clock.unschedule(running_session_scheduler_functions[0])
                Clock.unschedule(running_session_scheduler_functions[1])
                Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Stopped the scheduler running the demo.")

                self.bounce_detector_initialized = False
                shared_running_session_scheduler_interval = (1/30)
                Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: The Demo gameplay was finished.")

                # destroy the game mapper object.
                self.bounce_detector_initialized = False
                shared_running_session_game_mapper = None

                # destroy all the cv2 objects.
                self.ball_bounce_tracker.terminate_cameras()
                Logger.info("RunningSession - ball_bounce_detector_play_prerecorded_video: Terminated CV2 camera objects.")

                # Remove the image running in the background.
                self.ids.running_session_ball_bounce_proof.source = "artifacts/current_session/no_image_to_display.png"
                self.ids.running_session_ball_bounce_proof.opacity = 0
                self.ids.running_session_ball_bounce_proof.reload()

                # Remove the ball bounce detector.
                self.ball_bounce_tracker = None

                ##################################################
                # GAMEOVER POPUP COMES UP
                ##################################################
                self.gameover_popup_triggered()

    def ball_bounce_detector_monitor_cameras(self, *args):
        Logger.info("RunningSession - ball_bounce_detector_monitor_cameras: Using Live Camera feed.")
        self.ball_bounce_detector_play_prerecorded_video(args)

    #####################################################################################
    # Update functions.
    #####################################################################################
    def update_session_info(self, current_session_data):
        Logger.info("RunningSession - update_session_info: Updating Session Data.")

        self.ids.running_session_title.text = current_session_data["title"]
        self.ids.running_session_game_type_info.text = current_session_data["game_type"]
        self.ids.running_session_game_mode_info.text = current_session_data["game_mode"]
        self.ids.running_session_sets_info.text = "%s SET MATCH" % str(current_session_data["sets"])
        self.ids.running_session_team1_name.text = current_session_data["team1_name"]
        self.ids.running_session_team2_name.text = current_session_data["team2_name"]

        # The following are all updated from the Score Tracker API
        self.ids.running_session_team1_set_score.text = str(current_session_data["team1_score"]["sets"])
        self.ids.running_session_team1_point_score.text = str(current_session_data["team1_score"]["points"])
        self.ids.running_session_team2_set_score.text = str(current_session_data["team2_score"]["sets"])
        self.ids.running_session_team2_point_score.text = str(current_session_data["team2_score"]["points"])

        if current_session_data["server"] == 1:
            # Team 1 color is orange.
            self.ids.running_session_team1_name.color = "#F58A42"
            self.ids.running_session_team1_set_score.color = "#F58A42"
            self.ids.running_session_team1_point_score.color = "#F58A42"

            # Team 2 score is white.
            self.ids.running_session_team2_name.color = "#FFFFFF"
            self.ids.running_session_team2_set_score.color = "#FFFFFF"
            self.ids.running_session_team2_point_score.color = "#FFFFFF"
        else:
            # Team 1 color is white.
            self.ids.running_session_team1_name.color = "#FFFFFF"
            self.ids.running_session_team1_set_score.color = "#FFFFFF"
            self.ids.running_session_team1_point_score.color = "#FFFFFF"

            # Team 2 score is orange.
            self.ids.running_session_team2_name.color = "#F58A42"
            self.ids.running_session_team2_set_score.color = "#F58A42"
            self.ids.running_session_team2_point_score.color = "#F58A42"

    def update_session_ball_bounce_metrics(self, incoming_ball_bounce_metrics):
        Logger.debug("RunningSession - update_session_info: Retrieved raw ball bounce metrics: %s" % incoming_ball_bounce_metrics)
        if not incoming_ball_bounce_metrics["ready"]:
            # Hide the image.
            current_time = time.time()
            time_since_last_ball_bounce = current_time - self.ball_bounce_proof_last_displayed_time

            # If the last bounce occurred over 2 seconds ago, then stop showing the image.
            if time_since_last_ball_bounce > 2:
                self.ids.running_session_ball_bounce_proof.source = "artifacts/current_session/no_image_to_display.png"
                self.ids.running_session_ball_bounce_proof.opacity = 0
                self.ids.running_session_ball_bounce_proof.reload()

                # Reset the game notifications
                self.ids.running_session_app_notification.text = "RECORDING..."

        else:
            Logger.info("RunningSession - update_session_info: Retrieved valid ball bounce metrics: %s" % incoming_ball_bounce_metrics)
            self.ball_bounce_proof_last_displayed_time = time.time()
            
            # Show an image
            self.ids.running_session_ball_bounce_proof.source = incoming_ball_bounce_metrics["bounce_proof"]
            self.ids.running_session_ball_bounce_proof.opacity = 1
            self.ids.running_session_ball_bounce_proof.reload()
            
            # Update the game notification with the bounce info.
            # TODO: The score awarded parameter is read directly from the Score Tracker API
            #self.ids.running_session_app_notification.text = "BOUNCE DETECTED!\n(X=%s, Y=%s)\nSCORE_AWARDED=%s" % (
            #    incoming_ball_bounce_metrics["x"],
            #    incoming_ball_bounce_metrics["y"],
            #    0
            #)
            self.ids.running_session_app_notification.text = "BOUNCE!\n(X=%s, Y=%s)" % (
                int(incoming_ball_bounce_metrics["x"]),
                int(incoming_ball_bounce_metrics["y"]))

            #####################################
            # AUTOSAVING
            #####################################
            if self.ids.running_session_game_type_info.text == "Normal":
                Logger.info("RunningSession - update_session_info: Auto-saving the data to the JSON file.")

                ###############################################
                # Obtain the latest timestamp.
                ###############################################
                current_time = datetime.datetime.now()
                current_timestamp = current_time.strftime(TIMESTAMP_FORMAT)
                Logger.info("RunningSession - update_session_info: Current time now is: %s" % current_timestamp)

                #####################################################################
                # AUTOSAVING FEATURE
                #
                # TODO: Instead of calling this all the time, only call on new serve.
                # Need to add new hook to API for this to happen.
                # Obtain the current date.
                #####################################################################
                global shared_running_session
                session_data_to_save = {
                    "date": current_timestamp,
                    "game_type": shared_running_session["game_type"],
                    "game_mode": shared_running_session["game_mode"],
                    "sets": shared_running_session["sets"],
                    "title": shared_running_session["title"],
                    "team1": shared_running_session["team1_name"],
                    "team2": shared_running_session["team2_name"],
                    "team1_sets": shared_running_session["team1_score"]["sets"], 
                    "team1_points": shared_running_session["team1_score"]["points"],
                    "team2_sets": shared_running_session["team2_score"]["sets"], 
                    "team2_points": shared_running_session["team2_score"]["points"],
                    "server": shared_running_session["server"]}
                Logger.debug("RunningSession - update_session_info: New data to write: %s" % session_data_to_save)
                
                # Overwrite previous autosave file.
                with open(autosave_file, "w") as json_writefile:
                    autosave_session_data_json = json.dumps(session_data_to_save, indent=14)
                    json_writefile.write(autosave_session_data_json)
                    Logger.debug("RunningSession - update_session_info: Successfully saved the running session to the Autosave JSON file: %s" % sessions_file)

    #####################################################################################
    # The following scheduler function detects keyfob presses.
    #####################################################################################
    def check_keyfob_remote_presses(self, *args):
        global shared_running_session
        global running_session_scheduler_functions
        global shared_running_session_game_mapper
        global GUI_ENABLE_KEYFOB_ACTION
        global GUI_KEYBOARD_BUTTON_PRESS
        global GUI_KEYFOB_REMOTE_KEY_PRESS
        global GUI_KEYBOARD_PRESS_COUNTER

        # On Raspberry Pi (ONLY), Check GPIO inputs and obtain Keyfob Remote presses.
        if REMOTE_TYPE == "four_button_remote":
            GUI_KEYFOB_REMOTE_KEY_PRESS = KeyfobStatus()
            GUI_ENABLE_KEYFOB_ACTION = True
        else:
            # Used to check if keyboard press was done (30fps)
            if GUI_KEYBOARD_PRESS_COUNTER > 1:
                GUI_KEYBOARD_BUTTON_PRESS = False
                GUI_KEYBOARD_PRESS_COUNTER = 0
            GUI_KEYBOARD_PRESS_COUNTER += 1

        # If the button was pressed, do some action.
        if GUI_KEYBOARD_BUTTON_PRESS or GUI_ENABLE_KEYFOB_ACTION:
            if GUI_KEYFOB_REMOTE_KEY_PRESS['A']:
                Logger.info("RunningSession - check_keyfob_remote_presses: Pressed A. Pause or Unpause")
                if not self.paused_flag:
                    ########################################
                    # STOP THE SCHEDULER, ENTER PAUSE MODE
                    ########################################

                    # Pause basically disables updating the score.
                    # Ball bounce API is still running in the background.
                    # Set the color of the text to red to indicate session was paused.
                    self.ids.running_session_team1_name.color = "#FF0000"
                    self.ids.running_session_team1_set_score.color = "#FF0000"
                    self.ids.running_session_team1_point_score.color = "#FF0000"
                    self.ids.running_session_team2_name.color = "#FF0000"
                    self.ids.running_session_team2_set_score.color = "#FF0000"
                    self.ids.running_session_team2_point_score.color = "#FF0000"

                    # Update game notification.
                    self.ids.running_session_game_notification.text = "PAUSED"
                    
                    # Update flags.
                    self.undo_flag = False
                    self.paused_flag = True

                    # Stop the schedulers.
                    Clock.unschedule(running_session_scheduler_functions[1])
                    Logger.debug("RunningSession - check_keyfob_remote_presses - Key A: Session was paused.")
                else:
                    ##############################
                    # UNDO THE LAST SCORE!
                    ##############################

                    if self.undo_flag:
                        Logger.info("RunningSession - check_keyfob_remote_presses - Key A: Undo is to be committed.")

                        # Update current_session_data from modified scores in the
                        # updated game mapper object.
                        team1_scores, team2_scores = shared_running_session_game_mapper.get_scores()
                        current_server = shared_running_session_game_mapper.get_server()
                        self.current_session_data = {}
                        self.current_session_data = {
                            "game_type": self.transferred_session_params["game_type"],
                            "game_mode": self.transferred_session_params["game_mode"],
                            "sets": self.transferred_session_params["sets"],
                            "title": self.transferred_session_params["title"],
                            "team1_name": team1_scores["name"],
                            "team2_name": team2_scores["name"],
                            "team1_score": {"sets": team1_scores["sets"], "points": team1_scores["points"]},
                            "team2_score": {"sets": team2_scores["sets"], "points": team2_scores["points"]},
                            "server": current_server
                        }
                        self.incoming_ball_bounce_metrics = {
                            "ready": False,
                            "x": 100,
                            "y": 100,
                            "timeout_exceeded": False,
                            "bounce_proof": "artifacts/current_session/ball_bounce_proof.png"}
                        shared_running_session = self.current_session_data

                        ###########################################################################
                        # For the demo, subtract the counter to "fix" reset if accidentally
                        # pressed point increase on Player 1 or 2.
                        ###########################################################################
                        if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
                            self.demo_handler.modify_counter(False)
                            Logger.debug("RunningSession - check_keyfob_remote_presses - Key A: Reset counter back by 1 for the Demo.")
                        else:
                            Logger.debug("RunningSession - check_keyfob_remote_presses - Key A: Ball Bounce Detector does nothing special.")

                        print("RunningSession - check_keyfob_remote_presses - Key A: Session data was updated with undoed data.")

                    # Rerun the update functions.
                    self.update_session_ball_bounce_metrics(self.incoming_ball_bounce_metrics)
                    self.update_session_info(self.current_session_data)
                    Logger.debug("RunningSession - check_keyfob_remote_presses - Key A: Updated session data for unpausing.")

                    # Update game notification.
                    self.ids.running_session_game_notification.text = "UNPAUSED"

                    # Update all the flags
                    self.undo_flag = False
                    self.paused_flag = False

                    # Resume the schedulers.
                    global shared_running_session_scheduler_interval
                    Clock.schedule_interval(running_session_scheduler_functions[1], shared_running_session_scheduler_interval)
                    Logger.info("RunningSession - check_keyfob_remote_presses - Key A: Session was unpaused.")

            if GUI_KEYFOB_REMOTE_KEY_PRESS['B']:
                Logger.info("RunningSession - check_keyfob_remote_presses: Pressed B.")
                if self.paused_flag:
                    self.undo_popup_triggered()
                    
            if GUI_KEYFOB_REMOTE_KEY_PRESS['C']:
                Logger.info("RunningSession - check_keyfob_remote_presses: Pressed C.")

                if self.paused_flag and self.undo_flag:
                    #############################################################
                    # This mapper is shared between demo and live mapper.
                    # Team 1 gets one point after the undo.
                    #############################################################
                    shared_running_session_game_mapper.modify_score(True, False)

                    # Change the color to blue to indicate that Team 1's point is undoed.
                    self.ids.running_session_team1_name.color = "#42F542"
                    self.ids.running_session_team1_set_score.color = "#42F542"
                    self.ids.running_session_team1_point_score.color = "#42F542"
                    self.ids.running_session_team2_name.color = "#FF0000"
                    self.ids.running_session_team2_set_score.color = "#FF0000"
                    self.ids.running_session_team2_point_score.color = "#FF0000"

                    ##################################################
                    # For the demo, retrieve the game mapper
                    # and then update the global mapper.
                    ##################################################
                    if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
                        self.demo_handler.set_mapper(shared_running_session_game_mapper)
                        shared_running_session_game_mapper = self.demo_handler.get_mapper()
                        Logger.info("RunningSession - check_keyfob_remote_presses - Key C: Demo Game Mapper was updated.")
                    else:
                        Logger.info("RunningSession - check_keyfob_remote_presses - Key C: Ball Bounce Detector Game Mapper was updated.")
                    
                    Logger.info("RunningSession - check_keyfob_remote_presses - Key C: as score is undoed, Team #1 will get one point.")

            if GUI_KEYFOB_REMOTE_KEY_PRESS['D']:
                Logger.info("RunningSession - check_keyfob_remote_presses: Pressed D.")
                if self.paused_flag and self.undo_flag:
                    #############################################################
                    # This mapper is shared between demo and live mapper.
                    # Team 2 gets one point after the undo.
                    #############################################################
                    shared_running_session_game_mapper.modify_score(False, True)

                    # Change the color to blue to indicate that Team 2's point is undoed.
                    self.ids.running_session_team1_name.color = "#FF0000"
                    self.ids.running_session_team1_set_score.color = "#FF0000"
                    self.ids.running_session_team1_point_score.color = "#FF0000"
                    self.ids.running_session_team2_name.color = "#42F542"
                    self.ids.running_session_team2_set_score.color = "#42F542"
                    self.ids.running_session_team2_point_score.color = "#42F542"

                    ##################################################
                    # For the demo, retrieve the game mapper
                    # and then update the global mapper.
                    ##################################################
                    if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
                        self.demo_handler.set_mapper(shared_running_session_game_mapper)
                        shared_running_session_game_mapper = self.demo_handler.get_mapper()
                        Logger.info("RunningSession - check_keyfob_remote_presses - Key D: Demo Game Mapper was updated.")
                    else:
                        Logger.info("RunningSession - check_keyfob_remote_presses - Key D: Ball Bounce Detector Game Mapper was updated.")

                    Logger.info("RunningSession - check_keyfob_remote_presses - Key D: Score was undoed and Team 2 got the point.")
        
                ##############################################################
                # SAVE THE RUNNING SESSION FOR PLAYING LATER.
                ##############################################################
                elif self.paused_flag and not self.undo_flag and not self.popup_is_running:
                    team1_scores, team2_scores = shared_running_session_game_mapper.get_scores()
                    current_server = shared_running_session_game_mapper.get_server()
                    self.current_session_data = {}
                    self.current_session_data = {
                        "game_type": self.transferred_session_params["game_type"],
                        "game_mode": self.transferred_session_params["game_mode"],
                        "sets": self.transferred_session_params["sets"],
                        "title": self.transferred_session_params["title"],
                        "team1_name": team1_scores["name"],
                        "team2_name": team2_scores["name"],
                        "team1_score": {"sets": team1_scores["sets"], "points": team1_scores["points"]},
                        "team2_score": {"sets": team2_scores["sets"], "points": team2_scores["points"]},
                        "server": current_server
                    }
                    self.save_session_popup_triggered()

    #####################################################################################
    # Popup actions.
    #####################################################################################
    def undo_popup_triggered(self, *args):
        if not self.popup_is_running:
            # Configure the popup contents
            self.undo_popup = RunningSessionUndoPopop(self)
            self.undo_popup.open()

            # Set the below flag to true to prevent opening up multiple popup windows
            # on the input monitor.
            self.popup_is_running = True

    def undo_popup_dismissed(self, *args):
        # Dismiss the popup, and reset the flag to false
        self.undo_popup.dismiss()

        # Set the flags accordingly.
        self.popup_is_running = False

    def undo_the_last_score(self):
        # Dismiss the popup window.
        self.undo_popup.dismiss() 

        # Set the flags accordingly.
        self.undo_flag = True
        self.popup_is_running = False

        # Modify the shared game mapper to undo the last score.
        global shared_running_session_game_mapper
        shared_running_session_game_mapper.modify_score(False, False)

        ##########################################################
        # For the demo, update the game mapper in the class.
        ##########################################################
        if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
            # Update the demo handler with the updated game mapper if exists.
            self.demo_handler.set_mapper(shared_running_session_game_mapper)
            shared_running_session_game_mapper = self.demo_handler.get_mapper()
            Logger.info("RunningSession - undo_the_last_score - Demo Game Mapper was updated.")
        else:
            Logger.info("RunningSession - undo_the_last_score - Ball Bounce Detector Game Mapper was updated.")
        Logger.info("RunningSession - undo_the_last_score: The previous point was undoed.")

    def save_session_popup_triggered(self, *args):
        # Only if the popup isn't running open the popup.
        if not self.popup_is_running:
            self.popup_is_running = True

            # Update the label with the contents we want to save to the JSON file.
            session_data = "Title: %s\nTeams: %s vs %s\nTeam1 Score (Set|Point): %s|%s\nTeam2 Score (Set|Point): %s|%s\nMode: %s, Sets: %s, Server: %s" % (
                self.current_session_data["title"],
                self.current_session_data["team1_name"],
                self.current_session_data["team2_name"],
                self.current_session_data["team1_score"]["sets"],
                self.current_session_data["team1_score"]["points"],
                self.current_session_data["team2_score"]["sets"],
                self.current_session_data["team2_score"]["points"],
                self.current_session_data["game_mode"],
                self.current_session_data["sets"],
                self.current_session_data["server"]
            )

            # Save the data to the global variable so that the data may be written to the JSON file.
            global shared_running_session
            shared_running_session = self.current_session_data

            # Configure the popup to save.
            self.save_session_popup = RunningSessionSavePopop(self)
            self.save_session_popup.ids.running_session_save_session_popup_label.text = session_data
            self.save_session_popup.open()

    def save_session_popup_dismissed(self, *args):
        self.save_session_popup.dismiss()
        self.popup_is_running = False

    def save_the_running_session(self):
        # Dismiss the popup window.
        self.save_session_popup.dismiss() 

        # Stop all scheduler functions.
        # NOTE: The scheduler for the game mapper and ball bounce detector are already paused.
        global running_session_scheduler_functions
        Clock.unschedule(running_session_scheduler_functions[0])
        Logger.info("RunningSession - save_the_running_session: Stopping the monitoring functions.")

        # Reset all the flags.
        self.paused_flag = False
        self.undo_flag = False
        self.popup_is_running = False
        Logger.info("RunningSession - save_the_running_session: Reset all the flags for Running session.")

        # Obtain today's date:
        current_time = datetime.datetime.now()
        current_timestamp = current_time.strftime(TIMESTAMP_FORMAT)
        Logger.info("RunningSession - save_the_running_session: Current time now is: %s" % current_timestamp)

        # Save all contents to the JSON file.
        global shared_running_session
        session_data_to_save = {
            "date": current_timestamp,
            "game_type": shared_running_session["game_type"],
            "game_mode": shared_running_session["game_mode"],
            "sets": shared_running_session["sets"],
            "title": shared_running_session["title"],
            "team1": shared_running_session["team1_name"],
            "team2": shared_running_session["team2_name"],
            "team1_sets": shared_running_session["team1_score"]["sets"], 
            "team1_points": shared_running_session["team1_score"]["points"],
            "team2_sets": shared_running_session["team2_score"]["sets"], 
            "team2_points": shared_running_session["team2_score"]["points"],
            "server": shared_running_session["server"]}
        Logger.info("RunningSession - save_the_running_session: New data to write: %s" % session_data_to_save)
        Logger.info("RunningSession - save_the_running_session: Combined current running session data into for the JSON file.")
        
        new_sessions_data = []
        if os.path.exists(sessions_file):
            with open(sessions_file, "r") as json_readfile:
                Logger.debug("RunningSession - save_the_running_session: Previous sessions data found. Combining.")

                # Read the previous data
                old_sessions_data = json.load(json_readfile)
                if len(old_sessions_data) > 6:
                    old_sessions_data.pop(0)

                # Append to data.
                old_sessions_data.append(session_data_to_save)
                new_sessions_data = old_sessions_data
                Logger.debug("RunningSession - save_the_running_session: Combined Data written to JSON file: %s" % new_sessions_data)
            
        else:
            Logger.debug("RunningSession - save_the_running_session: No sessions data found, saving new data for this running session...")
            new_sessions_data = session_data_to_save
        
        # Finally write session data to the JSON file.
        with open(sessions_file, "w") as json_writefile:
            new_sessions_data_json = json.dumps(new_sessions_data, indent=14)
            json_writefile.write(new_sessions_data_json)
            Logger.debug("RunningSession - save_the_running_session: Successfully saved the running session to the JSON file: %s" % sessions_file)

        # Reset all the flags back before exiting the menu.
        global shared_running_session_game_mapper
        if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
            self.demo_handler = None
        else:
            # destroy the game mapper object.
            self.bounce_detector_initialized = False
            shared_running_session_game_mapper = None

            # destroy all the cv2 objects.
            self.ball_bounce_tracker.terminate_cameras()
            Logger.info("RunningSession - save_the_running_session: Terminated CV2 camera objects.")

            # Remove the image running in the background.
            self.ids.running_session_ball_bounce_proof.source = "artifacts/current_session/no_image_to_display.png"
            self.ids.running_session_ball_bounce_proof.opacity = 0
            self.ids.running_session_ball_bounce_proof.reload()

            # Remove the ball bounce detector.
            self.ball_bounce_tracker = None
    
        shared_running_session_game_mapper = None

        # Return back to the main menu.
        self.manager.transition.direction = "right"
        self.manager.current = "main_menu"
        Logger.debug("RunningSession - save_the_running_session: Returning back to the Main Menu after saving the last session.")

    def gameover_popup_triggered(self, *args):
        # Configure and Launch the Gameover popup.
        self.gameover_popuup = RunningSessionGameOverPopop(self)
        self.gameover_popuup.open()

    def return_to_main_menu_on_gameover(self):
        # Dismiss the popup
        self.gameover_popuup.dismiss()

        # Return to the Main Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "main_menu"

    def start_newgame_on_gameover(self):
        # Dismiss the popup
        self.gameover_popuup.dismiss()
    
        # Transition to the Start New Session Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "start_new_session_settings_menu"

#################################################
#   Restore Sessions Menu
#################################################
class RestoreSessionsPopup(Popup):
    def __init__(self, functions, **kwargs):
        super(RestoreSessionsPopup, self).__init__(**kwargs)
        self.functions = functions

class RestoreSessionsMenu(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(RestoreSessionsMenu, self).__init__(**kwargs)

        if "enable_demo" in GUI_CONFIG_PARAMS["core"] and not GUI_CONFIG_PARAMS["core"]["enable_demo"]:
            generate_samples_button = self.ids.restore_sessions_generate_sample_button
            generate_samples_button.parent.remove_widget(generate_samples_button)
            print("Generate button is disabled, as the app is NOT in Demo mode.")
        else:
            print("Demo mode enabled. Enabling Generate Button.")

        ##################################################################################
        # TODO: Refresh button pressed when switching to Restore Session from anywhere?
        ##################################################################################
        global gui_initialize_menus_functions
        gui_initialize_menus_functions[1] = self.restore_sessions_refresh_button

        # Configure the formatted text for the label on the popup.
        self.popup_text = "Date: {date}\nTitle: {title}\nTeams: {team1_name} vs {team2_name}\nTeam1 Score (Set|Point): {team1_sets}|{team1_points}\nTeam2 Score (Set|Point): {team2_sets}|{team2_points}\nMode: {game_mode}, Sets: {sets}, Server: {server}"

    # Clicking this button takes you to back to the Main Menu screen.
    def restore_sessions_goback_button(self):
        # Switch to the Main Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "main_menu"

    # Clicking this button generates fixed samples.
    def restore_sessions_generate_sample_button(self):
        GenerateHistorySamples()

    # Clicking this button refreshes the scrollview with items
    # pulled directly from the json file.
    def restore_sessions_refresh_button(self, *args):
        # Clears all previous listings.
        listings = self.ids.restore_sessions_listings
        listings.clear_widgets()

        # Generate new listings based on JSON file entries.
        try:
            try:
                with open(autosave_file, "r") as autosave_f:
                    # Load the JSON entry for the autosave data.
                    autosave_session_data = json.load(autosave_f)
                    Logger.info("RestoreSessionsMenu - restore_sessions_refresh_button: Autosave data loaded successfully!")

                    # Generate the text for the button
                    autosave_button_text = "(AUTOSAVE) %s -> %s/%s Sets\n%s, %s/%s, 1=(%s/%s), 2=(%s/%s), Server: %s" % (
                        autosave_session_data["date"],
                        autosave_session_data["game_mode"],
                        autosave_session_data["sets"],
                        autosave_session_data["title"],
                        autosave_session_data["team1"],
                        autosave_session_data["team2"],
                        autosave_session_data["team1_sets"],
                        autosave_session_data["team1_points"],
                        autosave_session_data["team2_sets"],
                        autosave_session_data["team2_points"],
                        autosave_session_data["server"]) 

                    # Create the button object.
                    self.autosave_button = Button(
                        text=autosave_button_text,
                        font="Arial",
                        font_size="18sp",
                        background_color="#FFFB00",
                        size_hint=(1.0, 0.3))
                    
                    # Add a callback for the autosave.
                    self.autosave_button.bind(on_press=lambda *args: self.restore_sessions_autosave_button_click_action(autosave_session_data, *args))

                    # Add the button to the layout.
                    listings.add_widget(self.autosave_button)


            except Exception as e:
                Logger.warning("RestoreSessionsMenu - restore_sessions_refresh_button: No autosave file found.")

            # Load the saved sessions after the autosave.
            with open(sessions_file, "r") as s:
                # Load the JSON entries.
                sessions_data = json.load(s)
                Logger.info("RestoreSessionsMenu - restore_sessions_refresh_button: Session data loaded successfully!")

                # Create buttons based on each entry.
                self.session_list_buttons = []
                self.session_list_buttons_data = []
                #for session_index, loaded_session_data_entry in enumerate(sessions_data):
                for session_index, loaded_session_data_entry in enumerate(reversed(sessions_data)):
                    # Generate the text that will go into the button.
                    button_text = "%s -> %s/%s Sets\n%s, %s/%s, 1=(%s/%s), 2=(%s/%s), Server: %s" % (
                        loaded_session_data_entry["date"],
                        loaded_session_data_entry["game_mode"],
                        loaded_session_data_entry["sets"],
                        loaded_session_data_entry["title"],
                        loaded_session_data_entry["team1"],
                        loaded_session_data_entry["team2"],
                        loaded_session_data_entry["team1_sets"],
                        loaded_session_data_entry["team1_points"],
                        loaded_session_data_entry["team2_sets"],
                        loaded_session_data_entry["team2_points"],
                        loaded_session_data_entry["server"])

                    # Create the button object.
                    self.current_button = Button(
                        text=button_text,
                        font="Arial",
                        font_size="18sp",
                        background_color="#32A86D",
                        size_hint=(1.0, 0.3))

                    # Set callback for each button.
                    # NOTE: Lamda doesn't seem to pass the actual index to the
                    # click_action function and instead passes some button id instead.
                    # Therefore add an ID to the dictionary object with the session data
                    # to be referenced again later.
                    self.current_button.bind(on_press=lambda button_index=session_index: self.restore_sessions_list_button_click_action(button_index))
                    loaded_session_data_entry["id"] = self.current_button

                    # These list will be referenced in the popup to pass to
                    # the running sessions screen.
                    self.session_list_buttons_data.append(loaded_session_data_entry)

                    # Append the button to buttons list to use later.
                    self.session_list_buttons.append(self.current_button)

                    # Add the button to the layout.
                    listings.add_widget(self.current_button)

        except Exception as e:
            Logger.warning("RestoreSessionsMenu - restore_sessions_refresh_button: No Sessions file found. Nothing to refresh.")

    def restore_sessions_configure_data_bundle_and_open_popup(self, session_data):
        # Generate the data bundle used for the Running Sessions Menu.
        global shared_running_session
        shared_running_session = {
            "game_type": session_data["game_type"],
            "game_mode": session_data["game_mode"],
            "title": session_data["title"],
            "team1_name": session_data["team1"],
            "team2_name": session_data["team2"],
            "sets": session_data["sets"],
            "team1_score": {"points": session_data["team1_points"], "sets": session_data["team1_sets"]},
            "team2_score": {"points": session_data["team2_points"], "sets": session_data["team2_sets"]},
            "server": session_data["server"]
        }

        # Modify the label field for the popup.
        popup_label_text = self.popup_text.format(
            date=session_data["date"],
            title=session_data["title"],
            team1_name=session_data["team1"],
            team2_name=session_data["team2"],
            team1_sets=session_data["team1_sets"],
            team1_points=session_data["team1_points"],
            team2_sets=session_data["team2_sets"],
            team2_points=session_data["team2_points"],
            game_mode=session_data["game_mode"],
            sets=session_data["sets"],
            server=session_data["server"]
        )

        # Configure the popup and then show it.
        self.restore_session_button_popup = RestoreSessionsPopup(self)
        self.restore_session_button_popup.ids.restore_session_popup_label.text = popup_label_text
        self.restore_session_button_popup.open()

    def restore_sessions_autosave_button_click_action(self, *args):
        # Load the session data for the autosave
        self.restore_sessions_data = args[0]
        self.restore_sessions_configure_data_bundle_and_open_popup(self.restore_sessions_data)
    
    def restore_sessions_get_session_data_for_button_id(self, button_id):
        for i in range(len(self.session_list_buttons_data)):
            if self.session_list_buttons_data[i]["id"] == button_id:
                return self.session_list_buttons_data[i]

    def restore_sessions_list_button_click_action(self, button):
        # Obtain the sessions data from the given button object (id)
        self.restore_sessions_data = self.restore_sessions_get_session_data_for_button_id(button)
        self.restore_sessions_configure_data_bundle_and_open_popup(self.restore_sessions_data)

    def restore_the_saved_session(self, *args):
        # Dismiss the popup window.
        self.restore_session_button_popup.dismiss()

        #############################################
        # For Demoing only
        #############################################
        global running_session_scheduler_functions
        global shared_running_session_scheduler_interval
        if "video_demo" not in GUI_CONFIG_PARAMS and "demo" in GUI_CONFIG_PARAMS and GUI_CONFIG_PARAMS["core"]["enable_demo"]:
            shared_running_session_scheduler_interval = 5
            Clock.schedule_once(running_session_scheduler_functions[1])
            #Logger.info("RestoreSessionsMenu - running_session_menu: Initialize the views on Running session for the Demo.")
        else:
            shared_running_session_scheduler_interval = float(1/30)
            Logger.info("RestoreSessionsMenu - running_session_menu: Ball Bounce Detector not implemented yet!")

        # Run the scheduler functions.
        Clock.schedule_interval(running_session_scheduler_functions[0], 1/60)
        Clock.schedule_interval(running_session_scheduler_functions[1], shared_running_session_scheduler_interval)
        Logger.info("RestoreSessionsMenu - running_session_menu: Scheduling game mapper and ball bounce monitoring.")

        # Print the bundle that we are passing to Running session.
        global shared_running_session
        Logger.debug("RestoreSessionsMenu - running_session_menu: Bundle to be passed: %s" % shared_running_session)

        # Transition to the Running Sessions Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "running_session_menu"
        Logger.info("RestoreSessionsMenu: Change to Running Session Menu.")


#################################################
#   Debug Menu
#################################################
class DebugMenuClearHistoryPopup(Popup):
    def __init__(self, functions, **kwargs):
        super(DebugMenuClearHistoryPopup, self).__init__(**kwargs)
        self.functions = functions

class DebugMenu(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(DebugMenu, self).__init__(**kwargs)

    # Pressing the Main Menu button returns to the previous menu.
    def debug_menu_goback_button(self):
        # Switch to the Main Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "main_menu"

    ######################################################
    # Ball Bounce Detection Configuration settings
    ######################################################
    def configure_ball_bounce_detection_settings(self):
        # Load any previously saved settings from the YAML file on boot.
        # NOTE: Assume background + ball color + table color settings are all present in the YAML file.
        if "table_tennis" in GUI_CONFIG_PARAMS and "ball_color_lower" in GUI_CONFIG_PARAMS["table_tennis"] and "ball_color_higher" in GUI_CONFIG_PARAMS["table_tennis"] and "table" in GUI_CONFIG_PARAMS["table_tennis"]:
            # Ball Color Lower Bound
            global BALL_BOUNCE_DETECTION_PARAM_IDS
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_slider_r.value = GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"][0]
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_label_r.text = str(GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"][0])
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_slider_g.value = GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"][1]
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_label_g.text = str(GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"][1])
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_slider_b.value = GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"][2]
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_label_b.text = str(GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"][2])

            # Modify the preview color.
            global BALL_BOUNCE_DETECTION_DEBUG_CANVAS
            global DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND
            BALL_BOUNCE_DETECTION_DEBUG_CANVAS.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)
            DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND = InstructionGroup()

            DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
                Color((BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_slider_r.value/255),
                    (BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_slider_g.value/255),
                    (BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_lower_bound_slider_b.value/255))
            )
            DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
                Ellipse(
                    pos=(self.width * 0.2, self.height * 0.685),
                    size=(self.width * 0.07, self.width * 0.07)
                )
            )
            BALL_BOUNCE_DETECTION_DEBUG_CANVAS.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)

            # Ball Color Higher Bound
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_slider_r.value = GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"][0]
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_label_r.text = str(GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"][0])
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_slider_g.value = GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"][1]
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_label_g.text = str(GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"][1])
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_slider_b.value = GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"][2]
            BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_label_b.text = str(GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"][2])
    
            # Modify the preview color.
            global DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND
            BALL_BOUNCE_DETECTION_DEBUG_CANVAS.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)
            DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND = InstructionGroup()

            DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
                Color((BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_slider_r.value/255),
                    (BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_slider_g.value/255),
                    (BALL_BOUNCE_DETECTION_PARAM_IDS.ball_color_higher_bound_slider_b.value/255))
            )
            DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
                Ellipse(
                    pos=(self.width * 0.2, self.height * 0.465),
                    size=(self.width * 0.07, self.width * 0.07)
                )
            )
            BALL_BOUNCE_DETECTION_DEBUG_CANVAS.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)

            # Table Color
            BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_slider_r.value = GUI_CONFIG_PARAMS["table_tennis"]["table"][0]
            BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_label_r.text = str(GUI_CONFIG_PARAMS["table_tennis"]["table"][0])
            BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_slider_g.value = GUI_CONFIG_PARAMS["table_tennis"]["table"][1]
            BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_label_g.text = str(GUI_CONFIG_PARAMS["table_tennis"]["table"][1])
            BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_slider_b.value = GUI_CONFIG_PARAMS["table_tennis"]["table"][2]
            BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_label_b.text = str(GUI_CONFIG_PARAMS["table_tennis"]["table"][2])

            # Modify the preview color.
            global DEBUG_COLOR_INDICATOR_TABLE
            BALL_BOUNCE_DETECTION_DEBUG_CANVAS.remove(DEBUG_COLOR_INDICATOR_TABLE)
            DEBUG_COLOR_INDICATOR_TABLE = InstructionGroup()

            DEBUG_COLOR_INDICATOR_TABLE.add(
                Color((BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_slider_r.value/255),
                    (BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_slider_g.value/255),
                    (BALL_BOUNCE_DETECTION_PARAM_IDS.table_color_slider_b.value/255))
            )
            DEBUG_COLOR_INDICATOR_TABLE.add(
                Ellipse(
                    pos=(self.width * 0.2, self.height * 0.265),
                    size=(self.width * 0.07, self.width * 0.07)
                )
            )
            BALL_BOUNCE_DETECTION_DEBUG_CANVAS.add(DEBUG_COLOR_INDICATOR_TABLE)

    def debug_menu_bb_filter_button(self):
        # Initialize the Ball Bounce settings from the YAML file.
        self.configure_ball_bounce_detection_settings()

        # Switch to the Ball Bounce Settings Menu
        self.manager.transition.direction = "left"
        self.manager.current = "ball_bounce_settings_menu"

    # Clicking this button takes you to the RF Remote test screen
    def debug_menu_remote_button(self):
        # Start the scheduler.
        global DEBUG_REMOTE_SCHEDULER_FUNCTION
        Clock.schedule_interval(DEBUG_REMOTE_SCHEDULER_FUNCTION, 1/60)
        print("Started Debug Remote Menu Scheduler function...")

        # Switch to the RF Remote Menu
        self.manager.transition.direction = "left"
        self.manager.current = "debug_remote_menu"

    # Clicking this button takes you to the Camera Feed test screen.
    def debug_menu_camera_feed_button(self):
        # Switch to the Camera Settings Menu
        self.manager.transition.direction = "left"
        self.manager.current = "debug_camera_menu"

    # Clicking this button takes you to the Score Tracker API test screen
    def debug_menu_score_tracker_api_button(self):
        # Switch to the Score Tracker API Debug Menu
        self.manager.transition.direction = "left"
        self.manager.current = "debug_score_tracker_api"

    # Clicking this button opens a dialog box for clearing the saved history.
    def debug_clear_history_button(self):
        # Configure and open the popup.
        self.popup_clear_history = DebugMenuClearHistoryPopup(self)
        self.popup_clear_history.open()

    def clear_saved_sessions_popup_button(self):
        ClearHistory(clear_autosave=False)
        self.popup_clear_history.dismiss()

    def clear_all_popup_button(self):
        ClearHistory(clear_autosave=True)
        self.popup_clear_history.dismiss()


#################################################
#   Ball Bounce Algorithm Settings Menu
#################################################
class BallBounceFilterSettings(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(BallBounceFilterSettings, self).__init__(**kwargs)

        # Initialize the canvas color indicator variables.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND
        global DEBUG_COLOR_INDICATOR_TABLE
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND
        global BALL_BOUNCE_DETECTION_DEBUG_CANVAS
        BALL_BOUNCE_DETECTION_DEBUG_CANVAS = self.canvas
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND = InstructionGroup()
        DEBUG_COLOR_INDICATOR_TABLE = InstructionGroup()
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND = InstructionGroup()
        BALL_BOUNCE_DETECTION_DEBUG_CANVAS.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)
        BALL_BOUNCE_DETECTION_DEBUG_CANVAS.add(DEBUG_COLOR_INDICATOR_TABLE)
        BALL_BOUNCE_DETECTION_DEBUG_CANVAS.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)

        # Used for configuring the parameters in the layout from the Debug Menu.
        global BALL_BOUNCE_DETECTION_PARAM_IDS
        BALL_BOUNCE_DETECTION_PARAM_IDS = self.ids
        Logger.debug("BallBounceFilterSettings: Initializing Ball Bounce Detection parameters...")

    # Pressing the Main Menu button returns to the previous menu.
    def debug_bb_filter_goback_button(self):
        # Switch to the Debug Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "debug_menu"

    # Clicking this button saves the values back to the config YAML file.
    def debug_bb_filter_save_settings(self):
        Logger.info("BallBounceFilterSettings - debug_bb_filter_save_settings: New Ball Bounce Ball Color Lower Bound: %s" % BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND)
        Logger.info("BallBounceFilterSettings - debug_bb_filter_save_settings: New Ball Bounce Ball Color Higher Bound: %s" % BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND)
        Logger.info("BallBounceFilterSettings - debug_bb_filter_save_settings: New Ball Bounce Table Color: %s" % BALL_BOUNCE_DETECTION_TABLE_COLOR)
        
        # TODO: Update Config YAML file.
        with open(config_yaml_file, "w") as yaml_f:
            # Modify the ball bounce entries in the YAML file.
            GUI_CONFIG_PARAMS["table_tennis"]["ball_color_lower"] = BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND
            GUI_CONFIG_PARAMS["table_tennis"]["ball_color_higher"] = BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND
            GUI_CONFIG_PARAMS["table_tennis"]["table"] = BALL_BOUNCE_DETECTION_TABLE_COLOR

            # Store the contents back into the YAML file and sort it.
            yaml.dump(GUI_CONFIG_PARAMS, yaml_f, sort_keys=True)
            Logger.debug("BallBounceFilterSettings - debug_bb_filter_save_settings: Updated the config yaml file with ball bounce algorithm entries.")

        # TODO: Update the Algorithm? Setup class?
        Logger.debug("BallBounceFilterSettings - debug_bb_filter_save_settings: Configured Ball Bounce Filter algorithm.")

        # Switch to the Main Menu.
        self.manager.transition.direction = "left"
        self.manager.current = "main_menu"

    def slider_ball_color_lower_r(self, *args):
        value = int(args[1])
        self.ids.ball_color_lower_bound_label_r.text = str(value)
        global BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND
        BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND[0] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND
        self.canvas.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND = InstructionGroup()

        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
            Color((self.ids.ball_color_lower_bound_slider_r.value/255),
                  (self.ids.ball_color_lower_bound_slider_g.value/255),
                  (self.ids.ball_color_lower_bound_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.685),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)

    def slider_ball_color_lower_g(self, *args):
        value = int(args[1])
        self.ids.ball_color_lower_bound_label_g.text = str(value)
        global BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND
        BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND[1] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND
        self.canvas.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND = InstructionGroup()

        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
            Color((self.ids.ball_color_lower_bound_slider_r.value/255),
                  (self.ids.ball_color_lower_bound_slider_g.value/255),
                  (self.ids.ball_color_lower_bound_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.685),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)

    def slider_ball_color_lower_b(self, *args):
        value = int(args[1])
        self.ids.ball_color_lower_bound_label_b.text = str(value)
        global BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND
        BALL_BOUNCE_DETECTION_BALL_COLOR_LOWER_BOUND[2] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND
        self.canvas.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND = InstructionGroup()

        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
            Color((self.ids.ball_color_lower_bound_slider_r.value/255),
                  (self.ids.ball_color_lower_bound_slider_g.value/255),
                  (self.ids.ball_color_lower_bound_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.685),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_LOWER_BOUND)

    def slider_ball_color_higher_r(self, *args):
        value = int(args[1])
        self.ids.ball_color_higher_bound_label_r.text = str(value)
        global BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND
        BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND[0] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND
        self.canvas.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND = InstructionGroup()

        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
            Color((self.ids.ball_color_higher_bound_slider_r.value/255),
                  (self.ids.ball_color_higher_bound_slider_g.value/255),
                  (self.ids.ball_color_higher_bound_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.465),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)

    def slider_ball_color_higher_g(self, *args):
        value = int(args[1])
        self.ids.ball_color_higher_bound_label_g.text = str(value)
        global BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND
        BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND[1] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND
        self.canvas.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND = InstructionGroup()

        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
            Color((self.ids.ball_color_higher_bound_slider_r.value/255),
                  (self.ids.ball_color_higher_bound_slider_g.value/255),
                  (self.ids.ball_color_higher_bound_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.465),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)

    def slider_ball_color_higher_b(self, *args):
        value = int(args[1])
        self.ids.ball_color_higher_bound_label_b.text = str(value)
        global BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND
        BALL_BOUNCE_DETECTION_BALL_COLOR_HIGHER_BOUND[2] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND
        self.canvas.remove(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND = InstructionGroup()

        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
            Color((self.ids.ball_color_higher_bound_slider_r.value/255),
                  (self.ids.ball_color_higher_bound_slider_g.value/255),
                  (self.ids.ball_color_higher_bound_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.465),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_BALL_COLOR_HIGHER_BOUND)

    def slider_table_color_r(self, *args):
        value = int(args[1])
        self.ids.table_color_label_r.text = str(value)
        global BALL_BOUNCE_DETECTION_TABLE_COLOR
        BALL_BOUNCE_DETECTION_TABLE_COLOR[0] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_TABLE
        self.canvas.remove(DEBUG_COLOR_INDICATOR_TABLE)
        DEBUG_COLOR_INDICATOR_TABLE = InstructionGroup()

        DEBUG_COLOR_INDICATOR_TABLE.add(
            Color((self.ids.table_color_slider_r.value/255),
                  (self.ids.table_color_slider_g.value/255),
                  (self.ids.table_color_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_TABLE.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.265),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_TABLE)

    def slider_table_color_g(self, *args):
        value = int(args[1])
        self.ids.table_color_label_g.text = str(value)
        global BALL_BOUNCE_DETECTION_TABLE_COLOR
        BALL_BOUNCE_DETECTION_TABLE_COLOR[1] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_TABLE
        self.canvas.remove(DEBUG_COLOR_INDICATOR_TABLE)
        DEBUG_COLOR_INDICATOR_TABLE = InstructionGroup()

        DEBUG_COLOR_INDICATOR_TABLE.add(
            Color((self.ids.table_color_slider_r.value/255),
                  (self.ids.table_color_slider_g.value/255),
                  (self.ids.table_color_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_TABLE.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.265),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_TABLE)

    def slider_table_color_b(self, *args):
        value = int(args[1])
        self.ids.table_color_label_b.text = str(value)
        global BALL_BOUNCE_DETECTION_TABLE_COLOR
        BALL_BOUNCE_DETECTION_TABLE_COLOR[2] = value

        # Modify the preview color.
        global DEBUG_COLOR_INDICATOR_TABLE
        self.canvas.remove(DEBUG_COLOR_INDICATOR_TABLE)
        DEBUG_COLOR_INDICATOR_TABLE = InstructionGroup()

        DEBUG_COLOR_INDICATOR_TABLE.add(
            Color((self.ids.table_color_slider_r.value/255),
                  (self.ids.table_color_slider_g.value/255),
                  (self.ids.table_color_slider_b.value/255))
        )
        DEBUG_COLOR_INDICATOR_TABLE.add(
            Ellipse(
                pos=(self.width * 0.2, self.height * 0.265),
                size=(self.width * 0.07, self.width * 0.07)
            )
        )
        self.canvas.add(DEBUG_COLOR_INDICATOR_TABLE)


#################################################
#   RF Remote Settings
#################################################
class DebugRemoteMenu(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(DebugRemoteMenu, self).__init__(**kwargs)

        # Schedule the input press checker function.
        global DEBUG_REMOTE_SCHEDULER_FUNCTION
        DEBUG_REMOTE_SCHEDULER_FUNCTION = self.check_keyfob_remote_presses

    #####################################################################################
    # The following function is run only when the Keyfob is connected, on the
    # Raspberry Pi.
    #####################################################################################
    def check_keyfob_remote_presses(self, *args):
        global GUI_ENABLE_KEYFOB_ACTION
        global GUI_KEYBOARD_BUTTON_PRESS
        global GUI_KEYFOB_REMOTE_KEY_PRESS
        global GUI_KEYBOARD_PRESS_COUNTER

        # On Raspberry Pi (ONLY), Check GPIO inputs and obtain Keyfob Remote presses.
        if REMOTE_TYPE == "four_button_remote":
            GUI_KEYFOB_REMOTE_KEY_PRESS = KeyfobStatus()
            GUI_ENABLE_KEYFOB_ACTION = True
        else:
            # Used to check if keyboard press was done.
            if GUI_KEYBOARD_PRESS_COUNTER > 60:
                GUI_KEYBOARD_BUTTON_PRESS = False
                GUI_KEYBOARD_PRESS_COUNTER = 0
            GUI_KEYBOARD_PRESS_COUNTER += 1
           
        # By default, choose the no press image
        self.ids.image.source = KEYFOB_IMAGES_DIRECTORY + "/" + "keyfob_no_press.png"

        # 3. If the key was pressed, update the corresponding labels.
        if GUI_KEYBOARD_BUTTON_PRESS or GUI_ENABLE_KEYFOB_ACTION:
            if GUI_KEYFOB_REMOTE_KEY_PRESS['A']:
                self.ids.button_press_a.text = "A: Was Pressed!"
                self.ids.button_press_a.color = "#6CD926"

                # Change to image showing Button A was pressed (GREEN)
                self.ids.image.source = KEYFOB_IMAGES_DIRECTORY + "/" + "keyfob_press_a.png"

                # Action
                self.ids.logic_textbox.text = KEYFOB_ACTIONS[0]
            else:
                self.ids.button_press_a.text = "A: Not Pressed!"
                self.ids.button_press_a.color = "#FF0006"

            if GUI_KEYFOB_REMOTE_KEY_PRESS['B']:
                self.ids.button_press_b.text = "B: Was Pressed!"
                self.ids.button_press_b.color = "#6CD926"

                # Change to image showing Button B was pressed (GREEN)
                self.ids.image.source = KEYFOB_IMAGES_DIRECTORY + "/" + "keyfob_press_b.png"
                self.ids.logic_textbox.text = KEYFOB_ACTIONS[1]
            else:
                self.ids.button_press_b.text = "B: Not Pressed!"
                self.ids.button_press_b.color = "#FF0006"

            if GUI_KEYFOB_REMOTE_KEY_PRESS['C']:
                self.ids.button_press_c.text = "C: Was Pressed!"
                self.ids.button_press_c.color = "#6CD926"

                # Change to image showing Button C was pressed (GREEN)
                self.ids.image.source = KEYFOB_IMAGES_DIRECTORY + "/" + "keyfob_press_c.png"
                self.ids.logic_textbox.text = KEYFOB_ACTIONS[2]
            else:
                self.ids.button_press_c.text = "C: Not Pressed!"
                self.ids.button_press_c.color = "#FF0006"

            if GUI_KEYFOB_REMOTE_KEY_PRESS['D']:
                self.ids.button_press_d.text = "D: Was Pressed!"
                self.ids.button_press_d.color = "#6CD926"

                # Change to image showing Button D was pressed (GREEN)
                self.ids.image.source = KEYFOB_IMAGES_DIRECTORY + "/" + "keyfob_press_d.png"
                self.ids.logic_textbox.text = KEYFOB_ACTIONS[3]
            else:
                self.ids.button_press_d.text = "D: Not Pressed!"
                self.ids.button_press_d.color = "#FF0006"

            if not GUI_KEYFOB_REMOTE_KEY_PRESS['A'] and GUI_KEYFOB_REMOTE_KEY_PRESS['B'] and GUI_KEYFOB_REMOTE_KEY_PRESS['C'] and GUI_KEYFOB_REMOTE_KEY_PRESS['D']:
                self.ids.logic_textbox.text = ""
        else:
            self.ids.button_press_a.text = "A: Not Pressed!"
            self.ids.button_press_b.text = "B: Not Pressed!"
            self.ids.button_press_c.text = "C: Not Pressed!"
            self.ids.button_press_d.text = "D: Not Pressed!"
            self.ids.button_press_a.color = "#FF0006"
            self.ids.button_press_b.color = "#FF0006"
            self.ids.button_press_c.color = "#FF0006"
            self.ids.button_press_d.color = "#FF0006"
            self.ids.logic_textbox.text = ""

        # Reload the image.
        self.ids.image.reload()

    def debug_remote_goback_button(self):
        # Stop the schedule running in the background.
        Clock.unschedule(self.check_keyfob_remote_presses)
        Logger.debug("DebugRemoteMenu - debug_remote_goback_button: Stopped Debug Remote Menu Scheduler function.")

        # Switch to the Debug Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "debug_menu"


#################################################
#   Camera Settings
#################################################
class DebugCameraMenu(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(DebugCameraMenu, self).__init__(**kwargs)

        # Initialize the Drop down values.
        self.camera_device_ids = ["None selected"]

        if "cameras" in GUI_CONFIG_PARAMS:
            self.camera_device_ids = []
            self.camera_device_ids.extend(GUI_CONFIG_PARAMS["cameras"])

            global CONNECTED_CAMERAS
            if len(GUI_CONFIG_PARAMS["cameras"]) == 1:
                self.ids.debug_camera_primary_device_dropdown.values = self.camera_device_ids
                self.ids.debug_camera_secondary_device_dropdown.values = ["None selected"]

                # Update the dropdowns and the global variable to be used by the Ball Bounce Detector.
                self.ids.debug_camera_primary_device_dropdown.text = self.camera_device_ids[0]
                CONNECTED_CAMERAS["primary"] = self.debug_camera_primary_device_dropdown.text
                Logger.info("DebugCameraMenu: Primary Camera device is now: %s." % CONNECTED_CAMERAS["primary"])
            elif len(GUI_CONFIG_PARAMS["cameras"]) == 2:
                self.ids.debug_camera_primary_device_dropdown.values = self.camera_device_ids
                self.ids.debug_camera_secondary_device_dropdown.values = self.camera_device_ids

                # Update the dropdowns and the global variable to be used by the Ball Bounce Detector.
                self.ids.debug_camera_primary_device_dropdown.text = self.camera_device_ids[0]
                self.ids.debug_camera_secondary_device_dropdown.text = self.camera_device_ids[1]
                CONNECTED_CAMERAS["primary"] = self.ids.debug_camera_primary_device_dropdown.text
                CONNECTED_CAMERAS["secondary"] = self.ids.debug_camera_secondary_device_dropdown.text
                Logger.info("DebugCameraMenu: Primary Camera device is now: %s." % CONNECTED_CAMERAS["primary"])
                Logger.info("DebugCameraMenu: Secondary Camera device is now: %s." % CONNECTED_CAMERAS["secondary"])
            else:
                self.camera_device_ids = ["None selected"]
                self.ids.debug_camera_primary_device_dropdown.values = self.camera_device_ids
                self.ids.debug_camera_secondary_device_dropdown.values = self.camera_device_ids

        # Initialize the camera stream images to non-connected ones.
        self.ids.debug_camera_primary_preview_image.source = CAMERA_IMAGE_LOCATION + "camera_turned_off.png"
        self.ids.debug_camera_secondary_preview_image.source = CAMERA_IMAGE_LOCATION + "camera_turned_off.png"
        self.ids.debug_camera_primary_preview_image.reload()
        self.ids.debug_camera_secondary_preview_image.reload()

    # Pressing the Main Menu button returns to the previous menu.
    def debug_camera_goback_button(self):
        # Stop all camera streams.
        self.debug_camera_primary_stop_button()
        self.debug_camera_secondary_stop_button()

        # Switch to the Debug Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "debug_menu"

    # Choosing the drop down selection for the primary usb camera dev id
    # will use the device number for the feed.
    def debug_camera_primary_device_selected(self, value):
        if value != "None selected":
            global CONNECTED_CAMERAS
            CONNECTED_CAMERAS["primary"] = value
            Logger.info("DebugCameraMenu - debug_camera_primary_device_selected: Primary Camera device is now: %s" % CONNECTED_CAMERAS["primary"])
        else:
            Logger.info("DebugCameraMenu - debug_camera_primary_device_selected: [Primary Camera] No valid device id was selected!")

    # Choosing the drop down selection for the secondary usb camera dev id
    # will use the device number for the feed.
    def debug_camera_secondary_device_selected(self, value):
        if value != "None selected":
            global CONNECTED_CAMERAS
            CONNECTED_CAMERAS["secondary"] = value
            Logger.info("DebugCameraMenu - debug_camera_secondary_device_selected: Secondary Camera device is now: %s" % CONNECTED_CAMERAS["secondary"])
        else:
            Logger.info("DebugCameraMenu - debug_camera_secondary_device_selected: [Secondary Camera] No valid device id was selected!")

    # Clicking this button will Start Primary Camera Feed
    def debug_camera_primary_start_button(self):
        global CAMERA_STREAMING
        if CONNECTED_CAMERAS["primary"] is not None and not CAMERA_STREAMING['primary']:
            # Attach primary camera to CV2
            connect_to_cameras(CONNECTED_CAMERAS)

            # Configure a schedule to read out the camera stream at 30 fps.
            Clock.schedule_interval(self.update_camera_stream_primary, 1/30)

            # Set the streaming flag to true to ensure you don't schedule clock twice.
            CAMERA_STREAMING["primary"] = True

            Logger.info("DebugCameraMenu - debug_camera_primary_start_button: Primary camera started (device = %s)." % CONNECTED_CAMERAS["primary"])
        else:
            if not CAMERA_STREAMING['primary']:
                Logger.info("DebugCameraMenu - debug_camera_primary_start_button: Cannot start camera.")

    # Clicking this button will Stop Primary Camera Feed
    def debug_camera_primary_stop_button(self):
        global ATTACHED_CAMERAS
        global CAMERAS_READY
        global CAMERA_STREAMING
        if CAMERAS_READY['primary']:
            # Stop the currently running scheduler.
            Clock.unschedule(self.update_camera_stream_primary)

            cv2.destroyAllWindows()
            ATTACHED_CAMERAS['primary'].release()
            # ATTACHED_CAMERAS['primary'] = None
            CAMERAS_READY['primary'] = False
            CAMERA_STREAMING["primary"] = False
            Logger.info("DebugCameraMenu - debug_camera_primary_stop_button: Primary camera was stopped.")

        # Change the background image for the secondary camera back to not-connected one.
        self.ids.debug_camera_primary_preview_image.source = CAMERA_IMAGE_LOCATION + "camera_turned_off.png"
        self.ids.debug_camera_primary_preview_image.reload()

    # Clicking this button will Start Secondary Camera Feed
    def debug_camera_secondary_start_button(self):
        if CONNECTED_CAMERAS["secondary"] is not None and not CAMERA_STREAMING['secondary']:
            # Attach secondary camera to CV2
            connect_to_cameras(CONNECTED_CAMERAS)

            # Configure a schedule to read out the camera stream at 30 fps.
            Clock.schedule_interval(self.update_camera_stream_secondary, 1/30)

            # Set the streaming flag to true to ensure you don't schedule clock twice.
            CAMERA_STREAMING["secondary"] = True

            Logger.info("DebugCameraMenu - debug_camera_secondary_start_button: Secondary camera started (device = %s)." % CONNECTED_CAMERAS["secondary"])
        else:
            if not CAMERA_STREAMING['secondary']:
                print("Cannot start camera.")

    # Clicking this button will Stop Secondary Camera Feed
    def debug_camera_secondary_stop_button(self):
        global ATTACHED_CAMERAS
        global CAMERAS_READY
        global CAMERA_STREAMING
        if CAMERAS_READY['secondary']:
            # Stop the currently running scheduler.
            Clock.unschedule(self.update_camera_stream_secondary)

            cv2.destroyAllWindows()
            ATTACHED_CAMERAS['secondary'].release()
            # ATTACHED_CAMERAS['secondary'] = None
            CAMERAS_READY['secondary'] = False
            CAMERA_STREAMING["secondary"] = False
            Logger.info("DebugCameraMenu - debug_camera_secondary_stop_button: Secondary camera was stopped.")

        # Change the background image for the secondary camera back to not-connected one.
        self.ids.debug_camera_secondary_preview_image.source = CAMERA_IMAGE_LOCATION + "camera_turned_off.png"
        self.ids.debug_camera_secondary_preview_image.reload()

    # Clicking this button will refresh the drop down lists values.
    def debug_camera_refresh_button(self):
        self.camera_device_ids = find_camera_device_ids()
        print(self.ids.debug_camera_primary_device_dropdown.values)
        self.ids.debug_camera_primary_device_dropdown.values = self.camera_device_ids
        self.ids.debug_camera_secondary_device_dropdown.values = self.camera_device_ids

    # NOTE: These two update functions simply refresh the images in with the camera stream.
    def update_camera_stream_primary(self, *args):
        camera_buffer_location = CAMERA_IMAGE_LOCATION + "camera_feed_primary.png"

        # Download the latest image from the attached camera.
        # TODO: Instead of saving, can we load directly from memory?
        result1, image1 = ATTACHED_CAMERAS["primary"].read()
        cv2.imwrite(camera_buffer_location, image1)

        # Reload the image for the primary camera.
        self.ids.debug_camera_primary_preview_image.source = camera_buffer_location
        self.ids.debug_camera_primary_preview_image.reload()

    def update_camera_stream_secondary(self, *args):
        camera_buffer_location = CAMERA_IMAGE_LOCATION + "camera_feed_secondary.png"

        # Download the latest image from the attached camera.
        # TODO: Instead of saving, can we load directly from memory?
        result2, image2 = ATTACHED_CAMERAS["secondary"].read()
        cv2.imwrite(camera_buffer_location, image2)

        # Reload the image for the secondary camera.
        self.ids.debug_camera_secondary_preview_image.source = camera_buffer_location
        self.ids.debug_camera_secondary_preview_image.reload()


#################################################
#   Score Tracker API Debug Menu
#################################################
class DebugScoreTracker(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(DebugScoreTracker, self).__init__(**kwargs)
        
        # Store the game state we will use to configure the coordinate mapper on boot.
        self.current_mapper_data = {"game_mode": "Singles",
                                    "sets": 3,
                                    "team1_name": "TEAM 1",
                                    "team2_name": "TEAM 2",
                                    "team1_score": {"sets": 0, "points": 0},
                                    "team2_score": {"sets": 0, "points": 0},
                                    "server": 2}

        # Initialize all views for the debug menu.
        self.update_debug_score_tracker_api_views(self.current_mapper_data) 

        # Configure flags.
        self.game_started = False
        self.undo_point = False
        self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"

        # Remove all previous ball bounce coordinate indicators
        self.ball_bounce_indicator = InstructionGroup()
        self.ball_bounce_indicator_active = False

    #####################################################################################
    # Update functions
    #####################################################################################
    def update_debug_score_tracker_api_views(self, mapper_data):
        self.ids.debug_score_tracker_api_sets.text = str(mapper_data["sets"])
        self.ids.debug_score_tracker_api_game_mode.text = mapper_data["game_mode"]
        self.ids.debug_score_tracker_api_team1_name.text = mapper_data["team1_name"]
        self.ids.debug_score_tracker_api_team2_name.text = mapper_data["team2_name"]
        self.ids.debug_score_tracker_api_team1_sets.text = str(mapper_data["team1_score"]["sets"])
        self.ids.debug_score_tracker_api_team1_points.text = str(mapper_data["team1_score"]["points"])
        self.ids.debug_score_tracker_api_team2_sets.text = str(mapper_data["team2_score"]["sets"])
        self.ids.debug_score_tracker_api_team2_points.text = str(mapper_data["team2_score"]["points"])

        if mapper_data["server"] == 1:
            self.ids.debug_score_tracker_api_starting_team_label.text = "Server=Team1"

            # Set the colors to yellow for Server.
            self.ids.debug_score_tracker_api_team1_name.color = "#FFFF00"
            self.ids.debug_score_tracker_api_team1_sets.color = "#FFFF00"
            self.ids.debug_score_tracker_api_team1_points.color = "#FFFF00"
            self.ids.debug_score_tracker_api_team2_name.color = "#FFFFFF"
            self.ids.debug_score_tracker_api_team2_sets.color = "#FFFFFF"
            self.ids.debug_score_tracker_api_team2_points.color = "#FFFFFF"
        else:
            self.ids.debug_score_tracker_api_starting_team_label.text = "Server=Team2"
            self.ids.debug_score_tracker_api_team1_name.color = "#FFFFFF"
            self.ids.debug_score_tracker_api_team1_sets.color = "#FFFFFF"
            self.ids.debug_score_tracker_api_team1_points.color = "#FFFFFF"
            self.ids.debug_score_tracker_api_team2_name.color = "#FFFF00"
            self.ids.debug_score_tracker_api_team2_sets.color = "#FFFF00"
            self.ids.debug_score_tracker_api_team2_points.color = "#FFFF00"

    #####################################################################################
    # All Button functions.
    #####################################################################################
    def debug_score_tracker_api_back_button(self, *args):
        # Reset flags.
        self.game_started = False

        # Switch to the Debug Menu
        self.manager.transition.direction = "right"
        self.manager.current = "debug_menu"

    def debug_score_tracker_api_newgame_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        # Force the game to start with 0|0 for both sides.
        self.current_mapper_data["team1_score"]["sets"] = 0
        self.current_mapper_data["team1_score"]["points"] = 0
        self.current_mapper_data["team2_score"]["sets"] = 0
        self.current_mapper_data["team2_score"]["points"] = 0

        # TODO: Should this be added to self for use later?
        team1_info = {"name": self.current_mapper_data["team1_name"],
                      "sets": self.current_mapper_data["team1_score"]["sets"],
                      "points": self.current_mapper_data["team1_score"]["points"]}
        team2_info = {"name": self.current_mapper_data["team2_name"],
                      "sets": self.current_mapper_data["team2_score"]["sets"],
                      "points": self.current_mapper_data["team2_score"]["points"]}

        self.game_mapper = coordinate_2d_mapper.table_tennis_coordinate_mapper(
            starting_server_team=self.current_mapper_data["server"],
            num_of_sets=self.current_mapper_data["sets"],
            ruleset_type=self.current_mapper_data["game_mode"].lower(),
            team1_info=team1_info,
            team2_info=team2_info,
            verbose=True
        )

        # Update the mapper data object from values directly from the coordinate mapper.
        self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
        self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
        self.current_server = self.game_mapper.get_server()
        self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
        self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
        self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
        self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
        self.current_mapper_data["server"]=self.current_server

        # Update the views.
        self.update_debug_score_tracker_api_views(self.current_mapper_data)    

        # Update all the flags.
        self.game_started = True
        self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
        self.undo_point = False
        Logger.debug("DebugScoreTracker - debug_score_tracker_api_newgame_button: New Game started!")

    def debug_score_tracker_api_starting_server_button(self, *args):
        # TODO: Need a button to terminate the game.
        if not self.game_started:
            self.current_mapper_data["server"] = coordinate_2d_mapper.randomly_choose_starting_server_team()
            self.update_debug_score_tracker_api_views(self.current_mapper_data)
            print("DebugScoreTracker - debug_score_tracker_api_starting_server_button: Starting Server is now: TEAM %s" % self.current_mapper_data["server"])

    # TODO: Connect the undo functions in next update.
    def debug_score_tracker_api_undo_point_button(self, *args):
        # If the flag was set to false before, only once, redo the last point.
        if not self.undo_point and self.game_started:
            # Undo the last point in the API.
            self.game_mapper.modify_score(False, False)
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Set the color of the button to red and set the flag to True.
            self.ids.debug_score_tracker_api_undo_point.background_color = "#D9324E"
            self.undo_point = True

    def debug_score_tracker_api_manual_team1_button(self, *args):
        if self.undo_point:
            # Manually change the last score to reward Team 1
            self.game_mapper.modify_score(True, False)
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Change the color of the button back to green and set flag to False.
            self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
            self.undo_point = False

    def debug_score_tracker_api_manual_team2_button(self, *args):
        if self.undo_point:
            # Manually change the last score to reward Team 1
            self.game_mapper.modify_score(False, True)
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Change the color of the button back to green and set flag to False.
            self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
            self.undo_point = False

    def debug_score_tracker_api_q1_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False
    
            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.5, self.height * 0.7),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on Quadrant 1
            random_value = random.randint(-30, -20)
            self.game_mapper.play_game(random_value, 50)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            # Check if the game finished. If it does, change status and set game_started flag to false.
            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False

            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_q2_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False

            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.5, self.height * 0.45),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on Quadrant 2
            random_value = random.randint(20, 30)
            self.game_mapper.play_game(random_value, 50)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            # Check if the game finished. If it does, change status and set game_started flag to false.
            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False
            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_q3_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False

            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.2, self.height * 0.7),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on Quadrant 3
            random_value = random.randint(-30, -20)
            self.game_mapper.play_game(random_value, -50)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            # Check if the game finished. If it does, change status and set game_started flag to false.
            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False
            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_q4_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False

            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.2, self.height * 0.45),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on Quadrant 4
            random_value = random.randint(20, 30)
            self.game_mapper.play_game(random_value, -50)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False
            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_out1_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False

            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.66, self.height * 0.57),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on OUT (top)
            random_value = random.randint(0, 10)
            self.game_mapper.play_game(random_value, 110)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False
            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_out2_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False

            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.04, self.height * 0.57),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on OUT (Bottom)
            random_value = random.randint(0, 10)
            self.game_mapper.play_game(random_value, -110)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False
            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_net_button(self, *args):
        # Remove all previous indicators.
        if self.ball_bounce_indicator_active:
            self.canvas.remove(self.ball_bounce_indicator)
            self.ball_bounce_indicator = InstructionGroup()

        if self.game_started:
            # If the point was undoed, and one of the quadrants was selected, reset the undo flag.
            if self.undo_point:
                # Change the color of the button back to green and set flag to False.
                self.ids.debug_score_tracker_api_undo_point.background_color = "#32D98E"
                self.undo_point = False

            # Draw a circle on the layout.
            self.ball_bounce_indicator.add(
                Color(1.0, 0.0, 0.0)
            )
            self.ball_bounce_indicator.add(
                Ellipse(
                    pos=(self.width * 0.35, self.height * 0.57),
                    size=(self.width * 0.02, self.width * 0.02)
                )
            )
            self.canvas.add(self.ball_bounce_indicator)
            self.ball_bounce_indicator_active = True

            # Bounce on Let
            random_value = random.randint(0, 10)
            self.game_mapper.play_game(random_value, 0)

            # Update the mapper data object from values directly from the coordinate mapper.
            self.team1_scores, self.team2_scores = self.game_mapper.get_scores()
            self.game_is_finished, self.team_that_won = self.game_mapper.get_game_state()
            self.current_server = self.game_mapper.get_server()

            self.current_mapper_data["team1_score"]["sets"] = self.team1_scores["sets"]
            self.current_mapper_data["team1_score"]["points"] = self.team1_scores["points"]
            self.current_mapper_data["team2_score"]["sets"] = self.team2_scores["sets"]
            self.current_mapper_data["team2_score"]["points"] = self.team2_scores["points"]
            self.current_mapper_data["server"]=self.current_server

            # Update the views.
            self.update_debug_score_tracker_api_views(self.current_mapper_data)

            # Update the notification.
            if self.game_is_finished:
                self.ids.debug_score_tracker_api_status.text = "GAME_FINISHED"
                self.game_started = False
            else:
                self.ids.debug_score_tracker_api_status.text = "BOUNCED"

    def debug_score_tracker_api_set_selected(self, value):
        self.current_mapper_data["sets"] = int(value)
        print("DebugScoreTracker - debug_score_tracker_api_set_selected: Sets selected: %s" % self.current_mapper_data["sets"])

    def debug_score_tracker_api_game_mode_selected(self, value):
        self.current_mapper_data["game_mode"] = value
        print("DebugScoreTracker - debug_score_tracker_api_set_selected: Game Mode selected: %s" % self.current_mapper_data["game_mode"])


#################################################
#   About Menu
#################################################
class AboutMenu(Screen):
    def __init__(self, **kwargs):
        # Re-initialize this Kivy Menu to bind keyboard buttons.
        super(AboutMenu, self).__init__(**kwargs)

        # Update the version # based on the YAML file.
        self.ids.about_menu_version_number.text = "Version: " + GUI_CONFIG_PARAMS["version"]

    # Clicking this button goes back to the main menu.
    def about_menu_goback_button(self):
        # Switch to the Main Menu.
        self.manager.transition.direction = "right"
        self.manager.current = "main_menu"

    # Clicking this button opens up a webpage to our git page.
    # Can also display the link in a prompt.
    # NOTE: Replace with actual git address?
    def about_menu_git_link(self):
        # Open up the web browser link.
        git_link = GUI_CONFIG_PARAMS["about"]["git"]
        webbrowser.open(git_link)

    # Clicking this button pulls the latest build from git,
    # runs a shell script which then closes the app,
    # moves the files to the same working directory
    # and reloads the application.
    # NOTE: Add animation for "Updating..." until app closes.
    def about_menu_update_app(self):
        current_floatlayout = self.ids.about_menu_floatlayout
        updater_image = Image(
            source="artifacts/menus/about/updating.gif",
            opacity=1,
            size_hint=(0.8, 0.8),
            pos=(self.width * 0.12, self.height * 0.22),
            anim_delay = 0.05,
            mipmap = True)
        current_floatlayout.add_widget(updater_image)
        
        # Disable all the buttons.
        self.ids.about_menu_goback_button.disabled = True
        self.ids.about_menu_git_link.disabled = True
        self.ids.about_menu_update_app.disabled = True

        # Start running the updater.
        root_directory = "/".join(os.path.realpath(__file__).split("/")[:-1])
        updater_command = "python3 %s/perform_update.py" % root_directory

        if REMOTE_TYPE == "keyboard":
            # On Local machines you can use gnome-terminal to launch local commands.
            foreground_updater_command = "gnome-terminal -- bash -c '%s; exec bash'" % updater_command
        else:
            # Raspberry Pi can use lxterminal to launch foreground commands.
            foreground_updater_command = "lxterminal -e '%s'" % updater_command
        os.system(foreground_updater_command)


class GUI(App):
    ######################################################
    # GUI Builder
    ######################################################
    def build(self):
        self.title = "Score Tracker GUI"

        # Try to initialize this object
        self.new_sessions_settings_popup_window = None

        score_tracker_gui = ScreenManager()
        score_tracker_gui.add_widget(MainMenu(name="main_menu"))
        score_tracker_gui.add_widget(NewSessionSettings(name="start_new_session_settings_menu"))
        score_tracker_gui.add_widget(RunningSession(name="running_session_menu"))
        score_tracker_gui.add_widget(RestoreSessionsMenu(name="restore_sessions_menu"))
        score_tracker_gui.add_widget(DebugMenu(name="debug_menu"))
        score_tracker_gui.add_widget(BallBounceFilterSettings(name="ball_bounce_settings_menu"))
        score_tracker_gui.add_widget(DebugRemoteMenu(name="debug_remote_menu"))
        score_tracker_gui.add_widget(DebugCameraMenu(name="debug_camera_menu"))
        score_tracker_gui.add_widget(DebugScoreTracker(name="debug_score_tracker_api"))
        score_tracker_gui.add_widget(AboutMenu(name="about_menu"))

        return score_tracker_gui


if __name__ == '__main__':
    running_gui = GUI()
    running_gui.run()
