# Table Tennis Score Tracker

The Score Tracker GUI is an application written for linux-based systems, particulary the Raspbian OS in order to be able to run on the Raspberry Pi with an LCD display connected, touch-screen enabled and attached to an RF receiver. This GUI uses OpenCV in order to mask out the ball in the camera feed, as well as generate table boundaries to perform the ball bounce detection. Currently, ball bounce timeouts do not work yet, in which if a ball bounce isn't detected in say 10-15 seconds, a bounce is automatically assumed to considered "out". Currently three modes exist for running the tracker: Standard Demo, Video Demo and Live Camera feed. enabling these 3 modes is possible via the YAML file.


## Getting started

Before running the GUI on your linux system or on the raspberry pi, make sure to run the script:
```
bash ./score_tracker_dependencies.sh
```
Comment out the following lines when running locally to avoid launching the GUI on every boot.
```
line 36  sudo touch /etc/xdg/autostart/score_tracker_gui.desktop
line 37  sudo echo "[Desktop Entry]" > /etc/xdg/autostart/score_tracker_gui.desktop
line 38  sudo echo "Name=Score_Tracker_GUI" >> /etc/xdg/autostart/score_tracker_gui.desktop
line 39  sudo echo "Exec=/bin/bash /score_tracker/run_score_tracker.sh"
```

Once the dependencies are run, you can run the following python script to launch the GUI.
```
python3 ./score_tracker_gui.py
```

By default, the resolution is set to 1024 x 600 to match the resolution of the Raspberry Pi display.
If you change this in the YAML file, some of the GUI menus will not be scaled correctly.


## Installation on Raspberry Pi

Asusming that you have already install Raspberry Pi OS on your Raspberry Pi (https://www.raspberrypi.com/software/),

Simply run the dependencies script below. This should also automatically generate the entries to launch the GUI on boot.
```
bash ./score_tracker_dependencies.sh
```

Create a new directory in root on the Pi and give correct read/write permissions.
```
sudo mkdir /score_tracker
sudo chmod -R 777 /score_tracker
```

Copy all the contents of "sport-score-tracker" into that folder.

Replace "gui_config.yaml" with "gui_config_pi.yaml"
```
cd /score_tracker
mv common/gui_config_pi.yaml gui_config.yaml
```

Finally, run the score tracker gui with the script:
```
cd /score_tracker
./run_score_tracker.sh
```

## Running the GUI in full screen mode.

Edit the gui_config YAML file in "common" folder and modify the "fullscreen" line to true.
```
core:
  display:
    x: 1024
    y: 600
  enable_demo: true
  font: Arial
  fullscreen: true
```

To return to windowed mode, change the value in the YAML file for "fullscreen" to false.
```
core:
  display:
    x: 1024
    y: 600
  enable_demo: true
  font: Arial
  fullscreen: false
```


## Change OOB (Out of Bounce) trigger-on-rally-bounce time.
During a rally, ball bounces are tracked with the Ball Bounce Detector. However, since the detector only tracks ball bounces, it cannot detect when a ball has gone out of bounds. The out of bounds condition is triggered when a ball had last bounced on any player's side and no new bounces have occurred after some time (in seconds). When this time is exceeded (oob_interval), an OOB coordinate is automatically passed to the coordinate 2D mapper and the respective team is awarded the point. At this point, the ball is in service and only until the next ball bounce occurs during a rally, will the OOB condition get triggered again.

To modify the interval for OOB to get triggered, edit the gui_config YAML file in "common" folder, parameter "oob_interval"
```
score_tracker:
  default_mode: Singles
  default_sets: 3
  default_team1: Team 1
  default_team2: Team 2
  name: table_tennis
  oob_interval: 15
```


## Use Live Video feed for the Score Tracker Ball Bounces

Edit the "gui_config" YAML file in "common" directory with the following:
```
core:
  display:
    x: 1024
    y: 600
  enable_demo: false
  font: Arial
  fullscreen: false
```
Basically, setting "enable_demo" to false, automatically disables all other demos.


## Use the Standard Demo for the Score Tracker Ball Bounces

Edit the "gui_config" YAML file in "common" directory with the following:
```
core:
  display:
    x: 1024
    y: 600
  enable_demo: true
  font: Arial
  fullscreen: false
demo: demo1

```
Make sure that the lines "video_demo" and "video_playback_debug" are REMOVED.


## Use the Top-view Video Demo for the Score Tracker Ball Bounces

Edit the "gui_config" YAML file in "common" directory with the following:
```
core:
  display:
    x: 1024
    y: 600
  enable_demo: true
  font: Arial
  fullscreen: false
demo: demo1
video_demo: 1
video_playback_debug: false

```
If you want to checkout the output from the camera or video while the bounce detector is in effect, set "video_playback_debug" flag to true in the YAML file:
```
core:
  display:
    x: 1024
    y: 600
  enable_demo: true
  font: Arial
  fullscreen: false
demo: demo1
video_demo: 1
video_playback_debug: true
```


## Updating the GUI

To update the GUI from git, edit the gui_config YAML file in "common" folder for which branch to pull from:
```
about:
  credits:
  - Partner A
  - Partner B
  - Partner C
  - Partner D
  git: https://gitlab.com/hisaamhashim/sport-score-tracker
  git_branch: hisaam-branch
```

If you want to pull from head, simply remove "git_branch" from the same YAML file:
```
about:
  credits:
  - Partner A
  - Partner B
  - Partner C
  - Partner D
  git: https://gitlab.com/hisaamhashim/sport-score-tracker
```

Then, run the following script.
```
cd /score_tracker
python3 ./perform_update.py
```

## Credits

Special thanks to Professor Preetpal Kang from San Jose State University for letting us work on such an interesting and challenging project.

Our team members who contributed greatly to this project are:
* Nathan Luc (GUI)
* Jesse Pham (Ball Bounce Detector)
* Huy Nguyen (Ball Bounce Detector)
* Hisaam Hashim (Score Tracker API, GUI and merging)


## Future Improvements

The following are some features that can be added to improve ball bounce accuracy:
* GUI window in Debug->Ball Bounce to reconfigure the overlay table coordinates for calibration using % of the camera resolution.
* Update the GUI using FloatLayout to make the GUI scalable on multiple resolutions.
* Update the Ball Bounce Detection Video Width and Height calculations to scale the resolution down from 1080p to 720p to improve detection performance.

