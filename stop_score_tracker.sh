#!/bin/bash
NUMBER_OF_APPLICATIONS=$(ps -aef | grep "score_tracker_gui.py" | awk '{print $2}' | wc -l)
APPLICATION_ID=$(ps -aef | grep "score_tracker_gui.py" | awk '{print $2}' | head --lines=1)

# Terminate the app.
echo "$(ps -aef | grep "score_tracker_gui.py")"
echo "# score tracker applications: $NUMBER_OF_APPLICATIONS"

if [ "$NUMBER_OF_APPLICATIONS" -ge "2" ]
then
	echo "Killing the score tracker application ($APPLICATION_ID)"
	sudo kill -9 $APPLICATION_ID
else
	echo "No score tracker applications are currently running."
fi
